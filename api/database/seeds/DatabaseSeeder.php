<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(BreedsTableSeeder::class);
        $this->call(QuestionTypesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
    }
}
