<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            [
                'department_id' => 2,
                'options' => '["Whole body", "Particular area"]',
                'question_type_id' => 3,
                'title' => 'Hair loss',
            ],
            [
                'department_id' => 2,
                'options' => '["Long period", "Sudden"]',
                'question_type_id' => 3,
                'title' => 'Itching',
            ],
            [
                'department_id' => 2,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Rashes',
            ],
            [
                'department_id' => 2,
                'options' => '["Whole body", "Localized"]',
                'question_type_id' => 3,
                'title' => 'Dandruff',
            ],
            [
                'department_id' => 2,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Pustules',
            ],
            [
                'department_id' => 2,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Foul smell',
            ],
            [
                'department_id' => 2,
                'options' => '["Tick", "Fleas", "Louse"]',
                'question_type_id' => 3,
                'title' => 'Ecto parasites',
            ],
            [
                'department_id' => 2,
                'options' => '["Face", "Cheek", "Limbs", "Fore limb", "Hind limb", "Around eyes", "Abdomen", "Inside Eyes", "Tail", "Neck", "Ears", "Between paws"]',
                'question_type_id' => 3,
                'title' => 'Sites where lesion notices',
            ],
            [
                'department_id' => 2,
                'options' => '["Sudden onset", "More than one week", "Intermittent occurence", "Seasonal"]',
                'question_type_id' => 3,
                'title' => 'When did you notice lesion and the frequency of occurance',
            ],
            [
                'department_id' => 2,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Have you consulted a veterinarian?',
            ],
            [
                'department_id' => 2,
                'options' => NULL,
                'question_type_id' => 1,
                'title' => 'Time since last occurrence',
            ],
            [
                'department_id' => 2,
                'options' => NULL,
                'question_type_id' => 1,
                'title' => 'Food given',
            ],
            [
                'department_id' => 2,
                'options' => NULL,
                'question_type_id' => 2,
                'title' => 'Any other comments',
            ],
            [
                'department_id' => 1,
                'options' => '["Whole body", "Particular area"]',
                'question_type_id' => 3,
                'title' => 'Hair loss',
            ],
            [
                'department_id' => 1,
                'options' => '["Long period", "Sudden"]',
                'question_type_id' => 3,
                'title' => 'Itching',
            ],
            [
                'department_id' => 1,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Rashes',
            ],
            [
                'department_id' => 1,
                'options' => '["Whole body", "Localized"]',
                'question_type_id' => 3,
                'title' => 'Dandruff',
            ],
            [
                'department_id' => 1,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Pustules',
            ],
            [
                'department_id' => 1,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Foul smell',
            ],
            [
                'department_id' => 1,
                'options' => '["Tick", "Fleas", "Louse"]',
                'question_type_id' => 3,
                'title' => 'Ecto parasites',
            ],
            [
                'department_id' => 1,
                'options' => '["Face", "Cheeck", "Limbs", "Fore limb", "Hind limb", "Around eyes", "Abdomen", "Inside Eyes", "Tail", "Neck", "Ears", "Between paws"]',
                'question_type_id' => 3,
                'title' => 'Sites where lesion notices',
            ],
            [
                'department_id' => 1,
                'options' => '["Sudden onset", "More than one week", "Intermittent occurence", "Seasonal"]',
                'question_type_id' => 3,
                'title' => 'When did you notice lesion and the frequency of occurance',
            ],
            [
                'department_id' => 1,
                'options' => '["Yes", "No"]',
                'question_type_id' => 3,
                'title' => 'Have you consulted a veterinarian?',
            ],
            [
                'department_id' => 1,
                'options' => NULL,
                'question_type_id' => 1,
                'title' => 'Time since last occurrence',
            ],
            [
                'department_id' => 1,
                'options' => NULL,
                'question_type_id' => 1,
                'title' => 'Food given',
            ],
            [
                'department_id' => 1,
                'options' => NULL,
                'question_type_id' => 2,
                'title' => 'Any other comments',
            ],
        ]);
    }
}