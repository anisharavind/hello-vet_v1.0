<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create user
        $user = User::create([
            'name'  => 'Dr. Sunil Kumar',
            'phone' => '9847268690',
        ]);

        // Create roles
        Role::create(['name' => 'super-admin']);
        Role::create(['name' => 'admin']);

        // Assign role
        $user->assignRole(['super-admin', 'admin']);
    }
}
