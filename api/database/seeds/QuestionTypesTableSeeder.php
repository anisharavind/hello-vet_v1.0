<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_types')->insert([
            [
                'name' => 'Text',
                'value' => 'text',
            ],
            [
                'name' => 'Text Area',
                'value' => 'textarea',
            ],
            [
                'name' => 'Select',
                'value' => 'select',
            ],
        ]);
    }
}