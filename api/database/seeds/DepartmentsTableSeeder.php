<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            [
                'created_at' => NULL,
                'deleted_at' => NULL,
                'disabled_at' => NULL,
                'fee' => '100.00',
                'id' => 1,
                'image' => NULL,
                'name' => 'Dermatology',
                'updated_at' => NULL,
            ],
            [
                'created_at' => NULL,
                'deleted_at' => NULL,
                'disabled_at' => NULL,
                'fee' => '50.00',
                'id' => 2,
                'image' => NULL,
                'name' => 'General Consultation',
                'updated_at' => NULL,
            ],
        ]);
    }
}