<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BreedsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('breeds')->insert([
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Affenpinscher',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Afghan Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Aidi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Airedale Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Akbash',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Akita',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alano Español',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alapaha Blue Blood Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alaskan husky',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alaskan Klee Kai',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alaskan Malamute',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alopekis',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alpine Dachsbracke',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Bully',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Cocker Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American English Coonhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Eskimo Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Foxhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Hairless Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Pit Bull Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Staffordshire Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'American Water Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Anatolian Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Andalusian Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Anglo-Français de Petite Vénerie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Appenzeller Sennenhund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ariegeois',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Armant',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Armenian Gampr dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Artois Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Australian Cattle Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Australian Kelpie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Australian Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Australian Stumpy Tail Cattle Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Australian Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Austrian Black and Tan Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Austrian Pinscher[29]',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Azawakh',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bakharwal dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Banjara Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Barbado da Terceira',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Barbet',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basenji',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basque Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basset Artésien Normand',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basset Bleu de Gascogne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basset Fauve de Bretagne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Basset Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bavarian Mountain Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Beagle',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Beagle-Harrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Belgian Shepherd Dog: Groenendael',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Belgian Shepherd Dog: Laekenois',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Belgian Shepherd Dog: Malinois',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Belgian Shepherd Dog: Tervuren',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bearded Collie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Beauceron',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bedlington Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bergamasco Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Berger Picard',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bernese Mountain Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bichon Frisé',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Billy',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Black and Tan Coonhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Black Norwegian Elkhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Black Russian Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Black Mouth Cur',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bloodhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Blue Lacy',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Blue Picardy Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bluetick Coonhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Boerboel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bohemian Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bolognese',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Border Collie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Border Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Borzoi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bosnian Coarse-haired Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Boston Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bouvier des Ardennes',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bouvier des Flandres',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Boxer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Boykin Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bracco Italiano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque d\'Auvergne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque de l\'Ariège',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque du Bourbonnais',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque Francais',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque Saint-Germain',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Briard',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Briquet Griffon Vendéen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Brittany',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Broholmer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bruno Jura Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Brussels Griffon',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bucovina Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bull Arab',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bull Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bullmastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bully Kutta',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Burgos Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cairn Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Campeiro Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Canaan Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Canadian Eskimo Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cane Corso',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cantabrian Water Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cão da Serra de Aires',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cão de Castro Laboreiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cão de Gado Transmontano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cão Fila de São Miguel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cardigan Welsh Corgi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Carolina Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Carpathian Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Catahoula Leopard Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Catalan Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Caucasian Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cavalier King Charles Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Central Asian Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cesky Fousek',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cesky Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chesapeake Bay Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chien Français Blanc et Noir',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chien Français Blanc et Orange',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chien Français Tricolore',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chihuahua',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chilean Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chinese Chongqing Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chinese Crested Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chinook',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chippiparai',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chow Chow',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cimarrón Uruguayo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cirneco dell\'Etna',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Clumber Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Combai',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Colombian fino hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Coton de Tulear',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cretan Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Croatian Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Curly-Coated Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cursinu',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Czechoslovakian Wolfdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dachshund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dalmatian',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dandie Dinmont Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Danish-Swedish Farmdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dingo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Doberman Pinscher',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dogo Argentino',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dogo Guatemalteco',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dogue Brasileiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dogue de Bordeaux',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Drentse Patrijshond',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Drever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dunker',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dutch Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dutch Smoushond',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'East Siberian Laika',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'East European Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Cocker Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Foxhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Setter',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Springer Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
            'name' => 'English Toy Terrier (Black & Tan)',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Entlebucher Mountain Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Estonian Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Estrela Mountain Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Eurasier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Field Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Fila Brasileiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Finnish Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Finnish Lapphund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Finnish Spitz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Flat-Coated Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'French Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'French Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Galgo Español',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Galician Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Garafian Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Gascon Saintongeois',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Georgian Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Longhaired Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Pinscher',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Roughhaired Pointer[',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Shorthaired Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Spitz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'German Wirehaired Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Giant Schnauzer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Glen of Imaal Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Golden Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Gończy Polski',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Gordon Setter',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Gran Mastín de Borínquen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Anglo-Français Blanc et Noir',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Anglo-Français Blanc et Orange',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Anglo-Français Tricolore',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Basset Griffon Vendéen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Bleu de Gascogne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Griffon Vendéen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Great Dane',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Great Pyrenees',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Greater Swiss Mountain Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Greek Harehound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Greek Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Greenland Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Greyhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Griffon Bleu de Gascogne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Griffon Fauve de Bretagne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Griffon Nivernais',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Gull Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hamiltonstövare',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hanover Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Harrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Havanese',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Himalayan Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hokkaido',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hortaya borzaya',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hovawart',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Huntaway',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hygen Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ibizan Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Icelandic Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Indian pariah dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Indian Spitz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Irish Red and White Setter',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Irish Setter',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Irish Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Irish Water Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Irish Wolfhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Istrian Coarse-haired Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Istrian Shorthaired Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Italian Greyhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Jack Russell Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Jagdterrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Japanese Chin',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Japanese Spitz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Japanese Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Jindo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Jonangi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kai Ken',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kangal Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kanni',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Karakachan dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Karelian Bear Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Karst Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Keeshond',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kerry Beagle',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kerry Blue Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'King Charles Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'King Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kintamani',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kishu',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kokoni',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Komondor',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kooikerhondje',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Koolie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Koyun dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kromfohrländer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kuchi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kumaon Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kunming wolfdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kuvasz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Labrador Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lagotto Romagnolo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lakeland Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lancashire Heeler',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Landseer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lapponian Herder',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lapponian Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Large Münsterländer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Leonberger',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lhasa Apso',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lithuanian Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Löwchen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Lupo Italiano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mackenzie River husky',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Magyar agár',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mahratta Greyhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Maltese',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Manchester Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Maremma Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'McNab dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature American Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature Bull Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature Fox Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature Pinscher',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature Schnauzer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Miniature Shar Pei',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Molossus of Epirus',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Montenegrin Mountain Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mountain Cur',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mucuchies',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mudhol Hound]',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Mudi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Neapolitan Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'New Guinea singing dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'New Zealand Heading Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Newfoundland',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norfolk Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norrbottenspets',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Northern Inuit Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norwegian Buhund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norwegian Elkhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norwegian Lundehund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norwich Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Nova Scotia Duck Tolling Retriever',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old Croatian Sighthound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old Danish Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old English Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old English Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Olde English Bulldogge',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Original Fila Brasileiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Otterhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pachon Navarro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Paisley Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Papillon',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Parson Russell Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pastore della Lessinia e del Lagorai',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Patagonian Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Patterdale Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pekingese',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pembroke Welsh Corgi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Perro de Pastor Mallorquin',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Perro de Presa Canario',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Perro de Presa Mallorquin',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Peruvian Inca Orchid',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Petit Basset Griffon Vendéen',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Petit Bleu de Gascogne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Phalène',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pharaoh Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Phu Quoc Ridgeback',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Picardy Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Plummer Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Plott Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Podenco Canario',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Poitevin',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Polish Greyhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Polish Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Polish Lowland Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Polish Tatra Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pomeranian',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pont-Audemer Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Poodle',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Porcelaine',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Portuguese Podengo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Portuguese Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Portuguese Water Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Posavac Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pražský Krysařík',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pudelpointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pug',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Puli',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pumi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pungsan dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pyrenean Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Pyrenean Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rafeiro do Alentejo',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rajapalayam',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rampur Greyhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rastreador Brasileiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rat Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ratonero Bodeguero Andaluz',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ratonero Mallorquin',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ratonero Murciano de Huerta',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Ratonero Valenciano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Redbone Coonhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rhodesian Ridgeback',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Romanian Mioritic Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Romanian Raven Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rottweiler',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Rough Collie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Russian Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Russian Toy',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Russo-European Laika',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Russell Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saarloos Wolfdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sabueso Español',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saint Bernard',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saint Hubert Jura Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saint-Usuge Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saluki',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Samoyed',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sapsali',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sarabi dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Šarplaninac',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Schapendoes',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Schillerstövare',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Schipperke',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Schweizer Laufhund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Schweizerischer Niederlaufhund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Scotch Collie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Scottish Deerhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Scottish Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sealyham Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Segugio Italiano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Seppala Siberian Sleddog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Serbian Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Serbian Tricolour Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Serrano Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shar Pei',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shetland Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shiba Inu',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shih Tzu',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shikoku',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Shiloh Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Siberian Husky',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Silken Windhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Silky Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sinhala Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Skye Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sloughi',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Slovakian Wirehaired Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Slovenský cuvac',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Slovenský kopov',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Smalandstövare',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Small Greek domestic dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Small Münsterländer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Smooth Collie',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Smooth Fox Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Soft-Coated Wheaten Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'South Russian Ovcharka',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Spanish Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Spanish Water Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Spinone Italiano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sporting Lucas Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Stabyhoun',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Staffordshire Bull Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Standard Schnauzer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Stephens Cur',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Styrian Coarse-haired Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sussex Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Swedish Elkhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Swedish Lapphund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Swedish Vallhund',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tahltan Bear Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Taigan',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Taiwan Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tamaskan Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Teddy Roosevelt Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Telomian',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tenterfield Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Terrier Brasileiro',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Thai Bangkaew Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Thai Ridgeback',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tibetan Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tibetan Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tibetan Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tornjak',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tosa',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Toy Fox Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Toy Manchester Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Transylvanian Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Treeing Cur',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Treeing Tennessee Brindle',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Treeing Walker Coonhound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Trigg Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tyrolean Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Villano de Las Encartaciones',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Villanuco de Las Encartaciones',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Vizsla',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Volpino Italiano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Weimaraner',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Welsh Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Welsh Springer Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Welsh Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'West Highland White Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'West Siberian Laika',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Westphalian Dachsbracke',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Wetterhoun',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Whippet',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'White Shepherd',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'White Swiss Shepherd Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Wire Fox Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Wirehaired Pointing Griffon',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Wirehaired Vizsla',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Xiasi Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Xoloitzcuintli',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Yakutian Laika',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Yorkshire Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alaunt',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alpine Mastiff',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Alpine Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Argentine Polar Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Black and Tan Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Blue Paul Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Braque Dupuy',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bull and terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Bullenbeisser',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chien-gris',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Chiribaya Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cordoba Fighting Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cumberland Sheepdog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Cur',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dalbo dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Dogo Cubano',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English Water Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'English White Terrier',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Fila da Terceira',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Grand Fauve de Bretagne',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Halls Heeler',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hare Indian Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Hawaiian Poi Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Kurī',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Marquesan Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Molossus',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Moscow Water Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Norfolk Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'North Country Beagle',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old English Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Old Spanish Pointer',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Russian Tracker',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Saint John\'s water dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Sakhalin Husky',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Salish Wool Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Southern Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tahitian Dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Talbot Hound',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Toy Bulldog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Toy Trawler Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Turnspit dog',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Tweed Water Spaniel',
            ],
            [
                'category_id' => 1,
                'image' => NULL,
                'name' => 'Welsh Hillman',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Abyssinian',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'American Bobtail',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'American Curl',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'American Shorthair',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'American Wirehair',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Balinese',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Bengal',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Birman',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Bombay',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Burmese',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Chartreux',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Cornish Rex',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Cymric',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Devon Rex',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Egyptian Mau',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Exotic Shorthair',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Havana Brown',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Himalayan',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Japanese Bobtail',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Javanese',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Korat',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Maine Coon',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Manx',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Munchkin',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Nebelung',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Norwegian Forest Cat',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Ocicat',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Oriental',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Persian',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Ragdoll',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Russian Blue',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Scottish Fold',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Selkirk Rex',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Siamese',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Siberian',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Singapura',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Snowshoe',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Somali',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Sphynx',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Tonkinese',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Turkish Angora',
            ],
            [
                'category_id' => 2,
                'image' => NULL,
                'name' => 'Turkish Van',
            ],
        ]);
    }
}