<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'id' => 1,
                'image' => NULL,
                'name' => 'Dog',
            ],
            [
                'id' => 2,
                'image' => NULL,
                'name' => 'Cat',
            ],
        ]);
    }
}