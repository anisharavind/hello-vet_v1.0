<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
              ->constrained()
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->foreignId('category_id');
            $table->foreignId('breed_id');
            $table->string('name', 50);
            $table->unsignedTinyInteger('sex')->comment('0 - Male, 1 - Female');
            $table->date('dob')->nullable();
            $table->date('age')->nullable();
            $table->string('image')->nullable();
            $table->date('subscription_ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
