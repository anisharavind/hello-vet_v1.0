<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
              ->constrained()
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->foreignId('pet_id')
              ->constrained()
              ->onUpdate('cascade')
              ->onDelete('cascade');
            $table->foreignId('department_id');
            $table->foreignId('doctor_id')->nullable();
            $table->unsignedDecimal('fee');
            $table->dateTime('date');
            $table->string('token');
            $table->string('order_id')->nullable();
            $table->string('payment_id')->nullable();
            $table->string('prescription')->nullable();
            $table->string('cancellation_reason')->nullable();
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('cancelled_at')->nullable();
            $table->timestamps();
            $table->unique(['department_id', 'date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
