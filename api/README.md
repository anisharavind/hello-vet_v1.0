# Installation

### Prerequisites
  - PHP >= 7.3
  - [Larvel Server Requirements](https://laravel.com/docs/7.x#server-requirements)
  - MySQL 8
  - Composer

### Update env variables
Copy `.env.example` to `.env`
```sh
$ cp .env.example .env
```
Update env variables
```sh
APP_URL=

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

### Install composer dependencies
```sh
$ composer install
```

### Create application key
```sh
$ php artisan key:generate
```

### Migrate & Seed DB
```sh
$ php artisan migrate --seed
```

### Start server
```sh
$ php artisan serve
```
