<table>
  <thead>
  <tr>
    @if (in_array('date', $columns))
    <th>Date</th>
    @endif
    @if (in_array('department', $columns))
    <th>Department</th>
    @endif
    @if (in_array('fee', $columns))
    <th>Fee</th>
    @endif
    @if (in_array('name', $columns))
    <th>Pet name</th>
    @endif
    @if (in_array('type', $columns))
    <th>Type of Pet</th>
    @endif
    @if (in_array('breed', $columns))
    <th>Breed</th>
    @endif
    @if (in_array('sex', $columns))
    <th>Sex</th>
    @endif
    @if (in_array('owner', $columns))
    <th>Owner name</th>
    @endif
    @if (in_array('email', $columns))
    <th>Email</th>
    @endif
    @if (in_array('phone', $columns))
    <th>Phone</th>
    @endif
    @if (in_array('diagnosis', $columns))
    <th>Diagnosis</th>
    @endif
    @if (in_array('prescription', $columns))
    <th>Prescription</th>
    @endif
  </tr>
  </thead>
  <tbody>
    @foreach($appointments as $appointment)
    <tr>
      @if (in_array('date', $columns))
      <td>{{ $appointment->date }}</td>
      @endif
      @if (in_array('department', $columns))
      <td>{{ $appointment->department->name }}</td>
      @endif
      @if (in_array('fee', $columns))
      <td>{{ $appointment->fee }}</td>
      @endif
      @if (in_array('name', $columns))
      <td>{{ $appointment->pet->name }}</td>
      @endif
      @if (in_array('type', $columns))
      <td>{{ $appointment->pet->category->name }}</td>
      @endif
      @if (in_array('breed', $columns))
      <td>{{ $appointment->pet->breed->name }}</td>
      @endif
      @if (in_array('sex', $columns))
      <td>{{ $appointment->pet->sex == 0 ? 'Male' : 'Female' }}</td>
      @endif
      @if (in_array('owner', $columns))
      <td>{{ $appointment->user->name }}</td>
      @endif
      @if (in_array('email', $columns))
      <td>{{ $appointment->user->email }}</td>
      @endif
      @if (in_array('phone', $columns))
      <td>{{ $appointment->user->phone }}</td>
      @endif
      @if (in_array('diagnosis', $columns))
      <td>{{ optional($appointment->diagnosis)->content }}</td>
      @endif
      @if (in_array('prescription', $columns))
      <td>
        @foreach($appointment->prescriptions as $prescription)
        <div>{{ $prescription->item }}</div>
        @endforeach
      </td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>