<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}}</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 22px;
            }

            .razorpay-payment-button {
                background: #7D39C4;
                border: none;
                border-radius: 48px;
                color: #fff;
                font-size: 18px;
                height: 48px;
                padding: 0 32px;
                outline: none;
            }
        </style>
    </head>
    <body>
        <div class="flex-center full-height">
            <div class="content">
                <div class="title">
                    <h2>₹ {{$appointment->fee}}</h2>
                    <form action="{{request()->fullUrl()}}" method="POST">
                        <script
                            src="https://checkout.razorpay.com/v1/checkout.js"
                            data-key="{{config('app.payment_key')}}"
                            data-amount="{{$appointment->fee * 100}}"
                            data-currency="INR"
                            data-order_id="{{$appointment->order_id}}"
                            data-description="Credits towards consultation"
                            data-prefill.name="{{$appointment->user->name}}"
                            data-prefill.email="{{$appointment->user->email}}"
                            data-prefill.contact="{{$appointment->user->phone}}"
                            data-buttontext="Make the payment"
                        ></script>
                        <input type="hidden" name="hidden">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
