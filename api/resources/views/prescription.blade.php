<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        @font-face {
            font-family: 'Circular Std Book';
            src: url('{{public_path('fonts/CircularStd-Book.ttf')}}') format('truetype');
            font-weight: 500;
            font-style: normal;
        }

        body,
        html {
            background-color: #fff;
            font-family: 'Circular Std Book';
            margin: 0;
        }

        p {
            margin: 0;
        }

        .wrapper {
            font-size: 14px;
            line-height: 1.5;
            color: rgba(0, 0, 0, 0.9);
            width: 700px;
            height: 1056px;
            padding: 47px;
            padding-bottom: 0;
            position: relative;
        }

        .hospital {
            font-size: 13px;
            text-align: right;
            padding-bottom: 30px;
        }

        .owner {
            margin-top: 15px;
        }
        .subtitle {
            margin-bottom: 15px;
        }

        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 99;
            padding-left: 47px;
            padding-right: 47px;
        }

        .instructions {
            width: 640px;
            height: 50px;
            font-size: 12px;
            padding: 15px 30px;
        }

        table {
            width: 100%;
            margin-top: 10px;
        }

        table, td {
            padding: 0;
        }

        .table,
        th {
            border: 1px solid rgba(196, 196, 196, 0.5);
            border-collapse: collapse;
        }

        .table td {
            padding: 10px 15px;
            border-left: 1px solid rgba(196, 196, 196, 0.5);
            border-right: 1px solid rgba(196, 196, 196, 0.5);
            border-collapse: collapse;
        }

        .table th {
            font-size: 14px;
            text-align: left;
            padding: 15px;
        }

        .col-1 {
            width: 40%;
        }

        .col-2 {
            width: 15%;
        }

        .col-3 {
            width: 45%;
        }

        .label {
            margin-right: 10px;
        }

        .font-bold {
            font-weight: bold;
            line-height: 2.5;
            letter-spacing: 1.5px;
            color: #000;
        }

        .font-normal {
            font-weight: normal;
        }

        .vertical-top {
            vertical-align: top;
        }

        .h-100 {
            height: 100%;
        }
        .text-left {
          text-align: left;
        }
        .text-center {
          text-align: center;
        }
        .text-right {
          text-align: right;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <div class="header">
            <div class="pet">
                <table>
                    <tr>
                        <td>
                            <img src="{{public_path('logo.png')}}" width="128" height="128" />
                        </td>
                        <td class="hospital">
                            <p>Dr. Sunil Kumar - B.V.Sc &amp; AH, PG</p>
                            <p>Sr. Surgeon and Pet Practitioner</p>
                            <p>Felican Pet Hospital - Multi Speciality</p>
                            <p>Sharipady, Eroor, Tripunithura, Cochin</p>
                            <p>Reg No: 1018</p>
                            <p>Phone: 0478 2776661, 99619333999</p>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="content">
            <table class="table">
                <thead>
                    <tr>
                        <th class="font-normal col-1">ITEM</th>
                        <th class="font-normal col-2">USAGE</th>
                        <th class="font-normal col-3">DIRECTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($appointment->prescriptions as $prescription)
                    <tr class="vertical-top">
                        <td>{{$prescription['item']}}</td>
                        <td>{{$prescription['usage']}}</td>
                        <td>{{$prescription['directions']}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="footer">
            <div class="instructions">
                <p>*This consultation done is a preliminary one only and may vary with final diagnosis. The advice and prescription is in good faith and its professional best and shall not be held liable for any legal proceedings as per the terms agreed with online consultation.</p>
            </div>
        </div>
    </div>
</body>

</html>