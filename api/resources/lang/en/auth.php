<?php

return [
    'failed'       => 'These credentials do not match our records.',
    'throttle'     => 'Too many login attempts. Please try again in :seconds seconds.',
    'invalid_code' => 'Invalid verification code.',
    'code_expired' => 'Verification code expired.',
    'wait'         => 'Please wait before retrying.'
];
