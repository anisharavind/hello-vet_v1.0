<?php

use App\Models\Appointment;
use Illuminate\Support\Facades\Route;

Route::namespace('Api')->group(function () {
    // Auth routes
    Route::namespace('Auth')->group(function () {
        Route::post('login', 'LoginController@login');
        Route::post('verify', 'VerificationController@verify');
        Route::post('verify/resend', 'VerificationController@resend');

        Route::middleware(['auth:sanctum'])->group(function () {
            Route::post('logout', 'LoginController@logout');
        });
    });

    // Main routes
    Route::middleware(['auth:sanctum'])->group(function () {
        // User
        Route::get('user', 'UserController@show');
        Route::post('user', 'UserController@update');
        Route::post('user/image', 'UserController@upload');
        Route::post('user/fcm', 'UserController@fcm');
        Route::post('user/pkd', 'UserController@pkd');
        Route::delete('user', 'UserController@destroy');

        // Pets
        Route::apiResource('pets', 'PetController');
        Route::get('categories', 'CategoryController@index');
        Route::get('categories/{id}/breeds', 'CategoryController@breeds');

        // Booking
        Route::get('timeslots', 'TimeSlotController@index');
        Route::get('departments', 'DepartmentController@index');
        Route::get('departments/{id}/questions', 'DepartmentController@questions');

        // Appointments
        Route::get('appointments/upcoming', 'AppointmentController@upcoming');
        Route::post('appointments/{appointment}/payment', 'AppointmentController@payment');
        Route::post('appointments/{appointment}/cancel', 'AppointmentController@cancel');
        Route::apiResource('appointments', 'AppointmentController');

        // Doctor
        Route::namespace('Doctor')->prefix('doctor')->middleware(['role:admin'])->group(function () {
            Route::get('users/admins', 'UserController@admins');
            Route::apiResource('users', 'UserController');

            Route::get('dashboard', 'DashboardController@index');
            Route::get('reports/filters', 'ReportController@filters');
            Route::get('reports', 'ReportController@index');
            Route::get('calendar', 'CalendarController@index');
            Route::apiResource('time-slots', 'TimeSlotController');

            // Appointments
            Route::post('appointments/{appointment}/diagnosis', 'AppointmentController@diagnosis');
            Route::post('appointments/{appointment}/prescription', 'AppointmentController@prescription');
            Route::get('appointments/{appointment}', 'AppointmentController@show');

            // Pet history
            Route::get('pets/{id}/history', 'PetController@history');

            Route::apiResource('departments', 'DepartmentController');
            Route::apiResource('doctors', 'DoctorController');
            Route::apiResource('questions', 'QuestionController');
            Route::apiResource('refunds', 'AppointmentRefundController');
            Route::apiResource('medicines', 'MedicineController');
        });
    });
});
