<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('payments/{appointment}', 'PaymentController@show');
Route::post('payments/{appointment}', 'PaymentController@update');
Route::get('success', 'PaymentController@success')->name('success');
Route::get('reports/download', 'Api\Doctor\ReportController@download');

Route::get('/{any}', 'HomeController@index')->where('any', '.*');
