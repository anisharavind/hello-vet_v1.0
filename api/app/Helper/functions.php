<?php

use Illuminate\Validation\ValidationException;

/**
 * Filter phone no.
 *
 * @param  string  $phone
 * @return string
 */
function filterPhone($phone)
{
    $phone = preg_replace("/^\+91/", '', $phone);
    $phone = preg_replace("/^0/", '', $phone);

    return $phone;
}

/**
 * Generate verification code.
 *
 * @return  string
 */
function OtpCode()
{
    return sprintf('%06d', mt_rand(0, 999999));
}

/**
 * Throw validation exception.
 *
 * @param  array  $errors
 * @return void
 *
 * @throws ValidationException
 */
function validationError($errors)
{
    throw ValidationException::withMessages($errors);
}

/**
 * Check resource ownership.
 *
 * @param  int  $userId
 * @param  int  $resourceUserId
 * @return void
 *
 * @throws mixed
 */
function checkOwnership($userId, $resourceUserId)
{
    return abort_unless($userId === $resourceUserId, 403);
}
