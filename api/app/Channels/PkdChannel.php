<?php

namespace App\Channels;

use Exception;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Http;

class PkdChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        try {
            $token = $notifiable->routeNotificationFor('Pkd');

            if (!$token) {
                return;
            }

            $channel = $notification->toPkd($notifiable);

            $this->triggerVoipCall($token, $channel->content);
        } catch (Exception $e) {
            info($e);
        }
    }

    public function triggerVoipCall($deviceId, $channel)
    {
        $url = "https://onesignal.com/api/v1/notifications";
        $data = [
            'app_id' => config('app.notification_app_id'),
            'contents' => array(
                "en" => 'Call from Felican',
            ),

            'apns_push_type_override' => "voip",
            'include_ios_tokens' => array($deviceId),
            'data' => array('channel' => $channel),
        ];

        try {
            $response = Http::post($url, $data);

            info("Response $response");

        } catch (\Throwable $th) {
            info("Failed to trigger voice call $th");
        }
    }
}
