<?php

namespace App\Channels\Messages;

class AppointmentMessage
{
    public $content;
  
    /**
     * Set message content.
     *
     * @param  string  $content
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}
