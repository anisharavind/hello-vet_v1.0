<?php

namespace App\Channels\Messages;

class FcmMessage
{
    public $content;
  
    /**
     * Set message content.
     *
     * @param  string  $content
     * @return $this
     */
    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}
