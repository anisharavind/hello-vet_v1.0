<?php

namespace App\Channels;

use Illuminate\Support\Facades\Http;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSms($notifiable);
        $url = "http://sms.mars.net.in/api/v1/sms/send?";
        $query = http_build_query([
            'key'     => config('app.sms_key'),
            'type'    => 1,
            'to'      => $notifiable->routeNotificationFor('Sms'),
            'sender'  => config('app.sms_sender'),
            'message' => $message->content,
            'flash'   => 0,
        ]);

        try {
            $response = Http::get($url.$query);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}