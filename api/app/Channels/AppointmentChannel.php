<?php

namespace App\Channels;

use Exception;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;

class AppointmentChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        try {
            $token = $notifiable->routeNotificationFor('Fcm');

            if (!$token) return;

            $message = $notification->toFcm($notifiable);

            $notification = [
                'title' => "New Appointment",
            ];
            $data = [
                'screen' => 'Appointment',
                'param'  => $message->content
            ];
            $message = CloudMessage::fromArray([
                'token'        => $token,
                'notification' => $notification,
                'data'         => $data
            ]);

            FirebaseMessaging::send($message);
        } catch (Exception $e) {
            info($e);
        }
    }
}