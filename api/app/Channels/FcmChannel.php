<?php

namespace App\Channels;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\AndroidConfig;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;

class FcmChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        try {
            $token = $notifiable->routeNotificationFor('Fcm');

            if (!$token) return;

            $channel = $notification->toFcm($notifiable);

            $notification = [
                'title' => "New Video Call",
                'body'  => "You have a new video call from hello felican"
            ];
            $data = [
                'phone'   => '9847268690',
                'uid'     => Str::uuid(),
                'channel' => $channel->content,
                'type'    => 'call',
                'screen'  => 'Video'
            ];
            $message = CloudMessage::fromArray([
                'token'        => $token,
                'notification' => $notification,
                'data'         => $data
            ]);
            $config = AndroidConfig::fromArray([
                'ttl'      => '0s',
                'priority' => 'high'
            ]);

            FirebaseMessaging::send($message->withAndroidConfig($config));
        } catch (Exception $e) {
            info($e);
        }
    }
}