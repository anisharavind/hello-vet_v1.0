<?php

namespace App\Channels;

use Exception;
use Illuminate\Notifications\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Laravel\Firebase\Facades\FirebaseMessaging;

class PrescriptionChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        try {
            $token = $notifiable->routeNotificationFor('Fcm');

            if (!$token) return;

            $message = $notification->toFcm($notifiable);

            $notification = [
                'title' => "Download prescription",
                'body'  => "Your prescription is ready to download"
            ];
            $data = [
                'screen' => 'Appointment',
                'param'  => $message->content
            ];
            $message = CloudMessage::fromArray([
                'token'        => $token,
                'notification' => $notification,
                'data'         => $data
            ]);

            FirebaseMessaging::send($message);
        } catch (Exception $e) {
            info($e);
        }
    }
}