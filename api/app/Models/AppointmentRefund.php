<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentRefund extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Appointment.
     */
    public function appointment()
    {
        return $this->belongsTo('App\Models\Appointment');
    }
}
