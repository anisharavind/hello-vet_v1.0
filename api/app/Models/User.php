<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'name', 'email', 'image',
    ];

    /**
     * Route notifications for the Sms channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSms($notification)
    {
        return $this->phone;
    }

    /**
     * Route notifications for the Sms channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForFcm($notification)
    {
        return $this->fcm_token;
    }

    /**
     * Route notifications for the Pkd channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForPkd($notification)
    {
        return $this->pkd_token;
    }

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['roles'];

    /**
     * Verification code.
     */
    public function verification()
    {
        return $this->hasOne('App\Models\Verification');
    }

    /**
     * Pets.
     */
    public function pets()
    {
        return $this->hasMany('App\Models\Pet');
    }

    /**
     * Appointments.
     */
    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');
    }
}
