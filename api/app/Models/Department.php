<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Questions.
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    /**
     * Appointments.
     */
    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');
    }
}
