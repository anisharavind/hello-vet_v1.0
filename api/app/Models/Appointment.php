<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Appointment between dates.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $start
     * @param  string  $end
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDateBetween($query, $start, $end)
    {
        if (!$start || !$end) {
            return $query;
        }

        return $query->whereDate('date', '>=', Carbon::parse($start))
            ->whereDate('date', '<=', Carbon::parse($end));
    }

    /**
     * User.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Pet.
     */
    public function pet()
    {
        return $this->belongsTo('App\Models\Pet');
    }

    /**
     * Department.
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department');
    }

    /**
     * Doctor.
     */
    public function doctor()
    {
        return $this->belongsTo('App\Models\Doctor');
    }

    /**
     * Doctor.
     */
    public function questions()
    {
        return $this->hasMany('App\Models\AppointmentQuestion');
    }

    /**
     * Presicriptions.
     */
    public function prescriptions()
    {
        return $this->hasMany('App\Models\AppointmentPrescription');
    }

    /**
     * Diagnosis.
     */
    public function diagnosis()
    {
        return $this->hasOne('App\Models\AppointmentDiagnosis');
    }

    /**
     * Refund.
     */
    public function refund()
    {
        return $this->hasOne('App\Models\AppointmentRefund');
    }
}
