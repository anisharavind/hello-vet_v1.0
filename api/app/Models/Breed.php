<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Pets.
     */
    public function pets()
    {
        return $this->hasMany('App\Models\Pet');
    }
}
