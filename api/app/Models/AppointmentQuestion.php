<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentQuestion extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Question.
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }
}
