<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 12;

    /**
     * Category.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Breed.
     */
    public function breed()
    {
        return $this->belongsTo('App\Models\Breed');
    }

    /**
     * Appointments.
     */
    public function appointments()
    {
        return $this->hasMany('App\Models\Appointment');
    }
}
