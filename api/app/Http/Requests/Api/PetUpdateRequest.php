<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PetUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => ['required', 'integer', 'exists:categories,id'],
            'breed_id'    => ['required', 'integer', 'exists:breeds,id'],
            'name'        => ['required', 'string', 'max:50'],
            'sex'         => ['required', 'integer'],
            'dob'         => ['nullable', 'date_format:Y-m-d', 'required_without:age'],
            'age'         => ['required', 'date_format:Y-m-d', 'required_without:dob'],
            'image'       => ['nullable', 'image', 'mimetypes:image/jpeg,image/png', 'max:10240'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'category_id' => 'category',
            'breed_id'    => 'breed',
        ];
    }
}
