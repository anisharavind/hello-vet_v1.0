<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class AppointmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pet_id'                  => ['required', 'integer', 'exists:pets,id'],
            'department_id'           => ['required', 'integer', 'exists:departments,id'],
            'doctor_id'               => ['nullable', 'integer', 'exists:doctors,id'],
            'date'                    => ['required', 'date'],
            'questions'               => ['required', 'array'],
            'questions.*.question_id' => ['required', 'integer', 'exists:questions,id'],
            'questions.*.answer'      => ['nullable'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'pet_id'        => 'pet',
            'department_id' => 'department',
            'doctor_id'     => 'doctor',
        ];
    }
}
