<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class QuestionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department_id'    => ['required', 'integer', 'exists:departments,id'],
            'question_type_id' => ['required', 'integer', 'exists:question_types,id'],
            'title'            => ['required', 'string'],
            'options'          => ['nullable', 'array'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'department_id'    => 'department',
            'question_type_id' => 'question type',
        ];
    }
}
