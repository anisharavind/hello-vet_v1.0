<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class PetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->id,
            'name'                 => $this->name,
            'sex'                  => $this->sex,
            'dob'                  => $this->dob,
            'age'                  => $this->age,
            'image'                => $this->image ? Storage::url($this->image) : $this->image,
            'subscription_ends_at' => $this->subscription_ends_at,
            'created_at'           => $this->created_at,
            'updated_at'           => $this->updated_at,
            'user'                 => new UserResource($this->whenLoaded('user')),
            'category'             => new CategoryResource($this->whenLoaded('category')),
            'breed'                => new BreedResource($this->whenLoaded('breed')),
            'appointments'         => AppointmentResource::collection($this->whenLoaded('appointments'))
        ];
    }
}
