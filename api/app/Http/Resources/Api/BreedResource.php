<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class BreedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'image'    => $this->image ? Storage::url($this->image) : $this->image,
            'category' => new CategoryResource($this->whenLoaded('category')),
            'pets'     => PetResource::collection($this->whenLoaded('pets'))
        ];
    }
}
