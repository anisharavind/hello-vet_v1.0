<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'answer'      => $this->answer,
            'file'        => $this->file ? Storage::url($this->file) : $this->file,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'appointment' => new AppointmentResource($this->whenLoaded('appointment')),
            'question'    => new QuestionResource($this->whenLoaded('question'))
        ];
    }
}
