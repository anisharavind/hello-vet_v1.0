<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'phone'       => $this->phone,
            'email'       => $this->email,
            'image'       => $this->image ? Storage::url($this->image) : $this->image,
            'created_at'  => $this->created_at->toIso8601ZuluString('milliseconds'),
            'updated_at'  => $this->updated_at->toIso8601ZuluString('milliseconds'),
            'pets'        => PetResource::collection($this->whenLoaded('pets')),
            'roles'       => $this->roles()->pluck('name')
        ];
    }
}
