<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name,
            'image'  => $this->image ? Storage::url($this->image) : $this->image,
            'breeds' => BreedResource::collection($this->whenLoaded('breeds')),
            'pets'   => PetResource::collection($this->whenLoaded('pets'))
        ];
    }
}
