<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'fee'       => $this->fee,
            'image'     => $this->image ? Storage::url($this->image) : $this->image,
            'questions' => QuestionResource::collection($this->whenLoaded('questions'))
        ];
    }
}
