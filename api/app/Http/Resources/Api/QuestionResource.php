<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'question_type_id' => $this->question_type_id,
            'title'            => $this->title,
            'options'          => $this->options,
            'type'             => new QuestionTypeResource($this->whenLoaded('type')),
        ];
    }
}
