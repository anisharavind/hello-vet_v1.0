<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'date'          => $this->date,
            'fee'           => $this->fee,
            'token'         => $this->token,
            'order_id'      => $this->order_id,
            'prescription'  => $this->prescription ? Storage::url($this->prescription) : $this->prescription,
            'paid_at'       => $this->paid_at,
            'cancelled_at'  => $this->cancelled_at,
            'user'          => new UserResource($this->whenLoaded('user')),
            'pet'           => new PetResource($this->whenLoaded('pet')),
            'department'    => new DepartmentResource($this->whenLoaded('department')),
            'doctor'        => new DoctorResource($this->whenLoaded('doctor')),
            'diagnosis'     => new AppointmentDiagnosisResource($this->whenLoaded('diagnosis')),
            'questions'     => AppointmentQuestionResource::collection($this->whenLoaded('questions')),
            'prescriptions' => AppointmentPrescriptionResource::collection($this->whenLoaded('prescriptions')),
        ];
    }
}
