<?php

namespace App\Http\Resources\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'department_id' => $this->department_id,
            'name'          => $this->name,
            'image'         => $this->image ? Storage::url($this->image) : $this->image,
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
            'department'    => new DepartmentResource($this->whenLoaded('department'))
        ];
    }
}
