<?php

namespace App\Http\Controllers;


use App\Models\Pet;
use Razorpay\Api\Api;
use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AppointmentUpdateRequest;

class PaymentController extends Controller
{
    /**
     * Show paymemt form for appointment.
     *
     * @return View
     */
    public function show(Appointment $appointment)
    {
        // Get token query
        $token = request()->query('token');

        // Check token
        abort_if(is_null($token) || is_null($appointment->token), 403);
        abort_unless($token === $appointment->token, 403);

        // Load relations
        $appointment->load(['user']);

        return view('payment', ['appointment' => $appointment]);
    }

    /**
     * Update payment details in appointment.
     *
     * @param  AppointmentUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        // Get input
        $input = $request->all();

        // Check token
        abort_if(is_null($input['token']) || is_null($appointment->token), 403);
        abort_unless($input['token'] === $appointment->token, 403);

        // Check payment signature
        $api = new Api(config('app.payment_key'), config('app.payment_secret'));
        $api->utility->verifyPaymentSignature([
            'razorpay_payment_id' => $input['razorpay_payment_id'],
            'razorpay_order_id'   => $input['razorpay_order_id'],
            'razorpay_signature'  => $input['razorpay_signature']
        ]);

        // Update appointment
        $appointment->payment_id = $input['razorpay_payment_id'];
        $appointment->paid_at    = now();
        $appointment->save();

        // Update pet subscription
        $pet = Pet::findOrFail($appointment->pet_id);
        $pet->subscription_ends_at = now()->addDays(15);
        $pet->save();

        return redirect()->route('success');
    }

    /**
     * Show success.
     *
     * @return \Illuminate\Http\Response
     */
    public function success()
    {
        //
    }
}
