<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Show admin view.
     *
     * @return View
     */
    public function index()
    {
        return view('welcome');
    }
}
