<?php

namespace App\Http\Controllers\Api;

use App\Models\TimeSlot;
use App\Models\Appointment;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\TimeSlotResource;

class TimeSlotController extends Controller
{
    /**
     * List timeslots.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get timeslots
        $timeslots = TimeSlot::addSelect([
            'count' => Appointment::selectRaw('count(*) as count')
                ->whereColumn('date', 'time_slots.date')
                ->limit(1)
        ])
            ->where('date', '>', now())
            ->having('count', 0)
            ->get();

        return TimeSlotResource::collection($timeslots);
    }
}
