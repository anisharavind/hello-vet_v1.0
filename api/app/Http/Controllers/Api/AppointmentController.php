<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Pet;
use App\Models\User;
use Razorpay\Api\Api;
use App\Models\TimeSlot;
use App\Models\Department;
use App\Models\Appointment;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\AppointmentResource;
use App\Http\Requests\Api\AppointmentCreateRequest;
use App\Http\Requests\Api\AppointmentUpdateRequest;
use App\Http\Requests\Api\AppointmentCancelRequest;
use App\Http\Requests\Api\AppointmentPaymentRequest;
use App\Notifications\Appointment as AppointmentNotification;

class AppointmentController extends Controller
{
    /**
     * List appointments history.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get user
        $user = auth()->user();

        // Get appointments
        $appointments = Appointment::with(['pet'])
            ->where('user_id', $user->id)
            ->where('date', '<=', now())
            ->whereNull('cancelled_at')
            ->latest('date')
            ->paginate(10);

        return AppointmentResource::collection($appointments);
    }

    /**
     * List upcoming appointments.
     *
     * @return \Illuminate\Http\Response
     */
    public function upcoming()
    {
        // Get user
        $user = auth()->user();

        // Get appointments
        $appointments = Appointment::with(['pet'])
            ->where('user_id', $user->id)
            ->where('date', '>', now())
            ->whereNull('cancelled_at')
            ->oldest('date')
            ->paginate(10);

        return AppointmentResource::collection($appointments);
    }

    /**
     * Create appointment.
     *
     * @param  AppointmentCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AppointmentCreateRequest $request)
    {
        // Db transaction
        $result = DB::transaction(function () use ($request) {
            // Get input
            $input = $request->validated();

            // Get user
            $user = auth()->user();

            $pet = Pet::findOrFail($input['pet_id']);

            // Check ownership
            checkOwnership($user->id, $pet->user_id);

            // Check time slot availability
            $exists = Appointment::where('date', $input['date'])
                ->where('department_id', $input['department_id'])
                ->exists();

            abort_if($exists, 422, 'Time slot is not available, try another one');

            // Get & lock timeslot
            $timeslot = TimeSlot::where('date', $input['date'])
                ->lockForUpdate()
                ->firstOrFail();

            // Get & lock deparment
            $department = Department::lockForUpdate()
                ->findOrFail($input['department_id']);

            // Get doctor
            $doctor = $request->filled('doctor_id') ? $input['doctor_id'] : null;

            // Create token
            $token = Hash::make(Str::uuid());

            // Create appointment
            $appointment                = new Appointment();
            $appointment->user_id       = $user->id;
            $appointment->pet_id        = $pet->id;
            $appointment->department_id = $department->id;
            $appointment->doctor_id     = $doctor;
            $appointment->fee           = $department->fee;
            $appointment->date          = $timeslot->date;
            $appointment->token         = $token;

            // Check pet subscription
            $date = $pet->subscription_ends_at ? Carbon::parse($pet->subscription_ends_at) : null;

            if ($date && $date->isAfter(now())) {
                $appointment->order_id   = 'subscription';
                $appointment->payment_id = null;
                $appointment->fee        = 0;
                $appointment->paid_at    = now();
            } else {
                // Create order in payment gateway
                $api   = new Api(config('app.payment_key'), config('app.payment_secret'));
                $order = $api->order->create([
                    'amount'          => $department->fee * 100,
                    'currency'        => 'INR',
                    'payment_capture' =>  '1'
                ]);
                $appointment->order_id = $order['id'];
            }


            $appointment->save();

            $questions = collect($input['questions'])->map(function ($item, $key) use (
                $request,
                $appointment
            ) {
                if ($request->hasFile('questions.'.$key.'.answer')) {
                    // Upload image
                    $extension = $request->file('questions.'.$key.'.answer')
                        ->extension();
                    $item['file'] = Storage::putFileAs(
                        'appointments/'.$appointment->id,
                        $request->file('questions.'.$key.'.answer'),
                        sha1($item['question_id']).'.'.$extension
                    ).'?v='.time();
                    $item['answer'] = null;
                }

                return $item;
            });

            // Create appointment questions
            $appointment->questions()->createMany($questions);

            // Get admin users
            $users = User::role(['super-admin', 'admin'])->get();

            // Send notification
            foreach ($users as $user) {
                $user->notify(new AppointmentNotification($appointment->id));
            }

            return $appointment;
        });

        return new AppointmentResource($result);
    }

    /**
     * Show appointment.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $appointment->user_id);

        // Load relations
        $appointment->load(['pet', 'user'])
            ->where('user_id', $user->id)
            ->get();

        return new AppointmentResource($appointment);
    }

    /**
     * Update appointment.
     *
     * @param  AppointmentUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(AppointmentUpdateRequest $request, Appointment $appointment)
    {
        // Db transaction
        $result = DB::transaction(function () use ($appointment, $request) {
            // Get input
            $input = $request->validated();

            // Get user
            $user = auth()->user();

            // Check ownership
            checkOwnership($user->id, $appointment->user_id);

            $date = $input['date'];
            $old  = Carbon::parse($appointment->date);

            abort_if($old->isBefore(now()), 422, "You can't edit past appointments");

            if ($appointment->date != $date) {
                // Check time slot availability
                $exists = Appointment::where('date', $date)
                    ->where('department_id', $appointment->department_id)
                    ->exists();

                abort_if($exists, 422, 'Time slot is not available, try another one');

                // Get & lock timeslot
                $timeslot = TimeSlot::where('date', $date)
                    ->lockForUpdate()
                    ->firstOrFail();

                $date = $timeslot->date;
            }

            // Update appointment
            $appointment->date = $date;
            $appointment->save();

            // Delete existing questions
            // $appointment->questions()->delete();

            // Create appointment questions
            // $appointment->questions()->createMany($input['questions']);

            return $appointment;
        });


        return new AppointmentResource($result);
    }

    /**
     * Update appointment (old one required for rn razor pay).
     *
     * @param  AppointmentPaymentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function payment(AppointmentPaymentRequest $request, Appointment $appointment)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $appointment->user_id);

        // Update appointment
        $appointment->payement_id = $input['payment_id'];
        $appointment->save();

        return new AppointmentResource($appointment);
    }

    /**
     * Cancel appointment.
     *
     * @param  AppointmentCancelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function cancel(AppointmentCancelRequest $request, Appointment $appointment)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $appointment->user_id);

        DB::transaction(function () use ($appointment, $input) {
            // Cancel appointment
            $appointment->cancelled_at        = now();
            $appointment->cancellation_reason = $input['reason'];
            $appointment->save();

            if ($appointment->payment_id) {
                // Cancel subscription
                $appointment->pet->subscription_ends_at = null;
                $appointment->pet->save();

                // Create refund request
                $appointment->refund()->create();
            }
        });

        return response()->noContent();
    }
}
