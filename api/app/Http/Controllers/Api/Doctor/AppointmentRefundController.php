<?php

namespace App\Http\Controllers\Api\Doctor;

use Razorpay\Api\Api;
use App\Models\AppointmentRefund;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\AppointmentRefundResource;

class AppointmentRefundController extends Controller
{
    /**
     * List refunds.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get refunds
        $refunds = AppointmentRefund::with(['appointment' => function ($query) {
            $query->with('pet.category')
                ->where('fee', '>', 0);
        }])
            ->whereNull('refund_id')
            ->whereNull('status')
            ->get();

        return AppointmentRefundResource::collection($refunds);
    }

    /**
     * Update refund.
     *
     * @param  AppointmentUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(AppointmentRefund $refund)
    {
        abort_if(
            is_null($refund->appointment->payment_id),
            422,
            'Unable to process right now'
        );

        try {
            $api = new Api(config('app.payment_key'), config('app.payment_secret'));
            $ref = $api->refund->create(
                ['payment_id' => $refund->appointment->payment_id]
            );

            // Update refund
            $refund->refund_id = $ref['id'];
            $refund->status = 1;
            $refund->save();
        } catch (\Throwable $th) {
            abort(500, $th->getMessage());
        }

        return response()->noContent();
    }
}
