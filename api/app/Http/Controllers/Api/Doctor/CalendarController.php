<?php

namespace App\Http\Controllers\Api\Doctor;

use Carbon\Carbon;
use App\Models\Appointment;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    /**
     * Appointment calendar.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get query string
        $view = request()->query('view');
        $date = request()->query('date');

        // Get dates 
        $dates = $this->getDates($view, $date);

        // Get appointments
        $appointments = $this->getAppointments($view, $dates);

        return response()->json(['appointments' => $appointments]);
    }

    /**
     * Get dates.
     *
     * @param  string  $view
     * @param  string  $date
     * @return array
     */
    public function getDates($view, $date)
    {
        $parsed = $date ? Carbon::parse($date) : now();

        switch ($view) {
            case 'month':
                return [
                    $parsed->copy()->startOfMonth(),
                    $parsed->copy()->endOfMonth()
                ];
            
            default:
                return [$parsed];
        }
    }

    /**
     * Get appointments.
     *
     * @param  string  $view
     * @param  array  $dates
     * @return mixed
     */
    public function getAppointments($view, $dates)
    {
        switch ($view) {
            case 'month':
                return Appointment::selectRaw('date(date) as day, count(*) as count')
                    ->whereDate('date', '>=', $dates[0])
                    ->whereDate('date', '<=', $dates[1])
                    ->whereNotNull('paid_at')
                    ->whereNull('cancelled_at')
                    ->groupBy('day')
                    ->oldest('day')
                    ->get();

            default:
                return Appointment::with(['pet.category'])
                    ->whereDate('date', $dates[0])
                    ->whereNotNull('paid_at')
                    ->whereNull('cancelled_at')
                    ->oldest('date')
                    ->get();
        }
    }
}
