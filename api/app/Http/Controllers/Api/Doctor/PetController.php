<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Pet;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\PetResource;

class PetController extends Controller
{
    /**
     * Show pet appointment history.
     *
     * @return \Illuminate\Http\Response
     */
    public function history($id)
    {
        // Get pet history
        $pet = Pet::with([
            'appointments' => function ($q) {
                $q->with([
                    'questions.question.type',
                    'prescriptions',
                    'diagnosis'
                ])
                    ->whereNull('cancelled_at')
                    ->orderBy('date', 'DESC');
            },
            'category'
        ])
            ->where('id', $id)
            ->first();

        return new PetResource($pet);
    }
}
