<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\User;
use Illuminate\Support\Str;
use App\Notifications\CallUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\UserResource;
use App\Http\Requests\Api\UserCreateRequest;

class UserController extends Controller
{
    /**
     * List user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get user
        $user = auth()->user();
        
        // Get users
        $users = User::whereNotNull('fcm_token')
            ->where('id', '!=', $user->id)
            ->get();

        return UserResource::collection($users);
    }

    /**
     * Call user.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $channel = Str::uuid();
        $user->notify(new CallUser($channel));

        return response()->json(['channel' => $channel]);
    }

    /**
     * List admin users.
     *
     * @return \Illuminate\Http\Response
     */
    public function admins()
    {
        // Get users
        $users = User::role('admin')
            ->get();

        return UserResource::collection($users);
    }

    /**
     * Create admin user.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Create user
        $user = User::create($input);

        // Assign role
        $user->assignRole('admin');

        return new UserResource($user);
    }

    /**
     * Delete admin user.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        abort_if(
            $user->hasRole('super-admin'),
            422,
            'You cant delete super admin'
        );

        abort_if(
            $user->id === auth()->user()->id,
            422,
            'You cant delete your profile'
        );

        // Delete user
        $user->delete();

        return response()->noContent();
    }
}
