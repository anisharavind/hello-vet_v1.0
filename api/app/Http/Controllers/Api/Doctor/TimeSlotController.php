<?php

namespace App\Http\Controllers\Api\Doctor;

use Carbon\Carbon;
use App\Models\TimeSlot;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\TimeSlotResource;
use App\Http\Requests\Api\TimeSlotCreateRequest;

class TimeSlotController extends Controller
{
    /**
     * List time slots.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get date
        $date = request()->filled('date')
            ? Carbon::parse(request()->query('date'))
            : now();
        
        // Get time slots
        $timeslots = TimeSlot::whereDate('date', $date)->get();
        
        return TimeSlotResource::collection($timeslots);
    }

    /**
     * Add time slots.
     *
     * @param  TimeSlotCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimeSlotCreateRequest $request)
    {
        // Db transaction
        DB::transaction(function () use ($request) {
            // Get input
            $input = $request->validated();

            // Get date
            $date = Carbon::parse($input['date']);

            // Get time slots
            $timeslots = collect($input['time_slots'])->map(function ($item) {
                return ['date' => $item];
            });

            // Delete all time slots
            TimeSlot::whereDate('date', $date)->delete();

            // Update time slots
            TimeSlot::insert($timeslots->toArray());
        });

        return response()->noContent(201);
    }
}
