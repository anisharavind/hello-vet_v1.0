<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Doctor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\DoctorResource;
use App\Http\Requests\Api\DoctorCreateRequest;
use App\Http\Requests\Api\DoctorUpdateRequest;

class DoctorController extends Controller
{
    /**
     * List doctors.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get doctors
        $doctors = Doctor::with('department')->get();

        return DoctorResource::collection($doctors);
    }

    /**
     * Create doctor.
     *
     * @param  DoctorCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DoctorCreateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Create doctor
        $doctor = Doctor::create($input);

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $doctor->image = Storage::putFileAs(
                'doctor/'.$doctor->id,
                $request->file('image'),
                sha1($doctor->id).'.'.$extension
            );
            $doctor->save();
        }

        return new DoctorResource($doctor);
    }

    /**
     * Show doctor.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        return new DoctorResource($doctor);
    }

    /**
     * Update doctor.
     *
     * @param  DoctorUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(DoctorUpdateRequest $request, Doctor $doctor)
    {
        // Get input
        $input = $request->validated();

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $input['image'] = Storage::putFileAs(
                'doctor/'.$doctor->id,
                $request->file('image'),
                sha1($doctor->id).'.'.$extension
            );
        }

        // Update doctor
        $doctor->update($input);

        return new DoctorResource($doctor);
    }

    /**
     * Delete doctor.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor)
    {
        // Get image
        $image = $doctor->image;

        // Delete doctor
        $doctor->delete();

        if (!is_null($image)) {
            // Delete image
            Storage::delete($image);
        }

        return response()->noContent();
    }
}
