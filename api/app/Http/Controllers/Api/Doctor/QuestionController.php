<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Question;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\QuestionResource;
use App\Http\Requests\Api\QuestionCreateRequest;

class QuestionController extends Controller
{
    /**
     * Create question.
     *
     * @param  QuestionCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionCreateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Create question
        $question = Question::create($input);

        return new QuestionResource($question);
    }

    /**
     * Delete question.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        // Delete question
        $question->delete();

        return response()->noContent();
    }
}
