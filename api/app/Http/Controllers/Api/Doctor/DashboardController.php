<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Appointment;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Dashboard stats.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get appointment & revenue
        $query = DB::table('appointments')
            ->selectRaw('count(*) as count')
            ->selectRaw('sum(fee) as revenue')
            ->whereNotNull('paid_at')
            ->whereNull('cancelled_at');

        // For the day
        $day = (clone $query)->whereDate('date', now())
            ->first();

        // For the previous day
        $previous = (clone $query)->whereDate('date', now()->subDay())
            ->first();

        // All time
        $all = (clone $query)->first();

        // Get upcoming appointment
        $next = Appointment::with('pet.category')
            ->where('date', '>', now())
            ->whereNotNull('paid_at')
            ->whereNull('cancelled_at')
            ->first();

        return response()->json([
            'appointments' => [
                'day'      => $day,
                'previous' => $previous,
                'all'      => $all,
                'next'     => $next
            ]
        ]);
    }
}
