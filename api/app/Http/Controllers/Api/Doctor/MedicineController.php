<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Medicine;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\MedicineResource;

class MedicineController extends Controller
{
    /**
     * List medicine.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Create Medicine
        $medicines = Medicine::all();

        return MedicineResource::collection($medicines);
    }
}
