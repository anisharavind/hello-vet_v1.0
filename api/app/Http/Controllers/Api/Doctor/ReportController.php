<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Breed;
use App\Models\Category;
use App\Models\Department;
use App\Models\Appointment;
use App\Exports\AppointmentsExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Resources\Api\AppointmentResource;

class ReportController extends Controller
{
    /**
     * Reports.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = $this->getData()
            ->paginate()
            ->appends(request()->query());

        return AppointmentResource::collection($appointments);
    }

    /**
     * Report download.
     *
     * @return \Illuminate\Http\Response
     */
    public function download()
    {
        $data   = $this->getData();
        $export = new AppointmentsExport($data->get(), request()->query('columns'));
    
        return Excel::download($export, 'reports.xlsx');
    }

    /**
     * Get filtered data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData()
    {
        $appointments = QueryBuilder::for(Appointment::class)
            ->allowedFilters([
                'date',
                AllowedFilter::exact('department_id'),
                AllowedFilter::exact('pet.category_id'),
                AllowedFilter::exact('pet.breed_id'),
                AllowedFilter::scope('date_between'),
                AllowedFilter::callback('search', function (Builder $query, $value) {
                    $query->whereHas('user', function (Builder $query) use ($value) {
                        $query->where('name', 'like', '%'.$value.'%');
                    })
                        ->orWhereHas('user', function (Builder $query) use ($value) {
                            $query->where('phone', 'like', '%'.$value.'%');
                        })
                        ->orWhereHas('pet', function (Builder $query) use ($value) {
                            $query->where('name', 'like', '%'.$value.'%');
                        });
                }),
                AllowedFilter::callback('columns', function (Builder $query, $value) {
                    //
                })
            ])
            ->with([
                'department',
                'pet.category',
                'pet.breed',
                'user',
                'prescriptions',
                'diagnosis'
            ])
            ->orderBy('date', 'desc');

        return $appointments;
    }

    /**
     * Reports filter options.
     *
     * @return \Illuminate\Http\Response
     */
    public function filters()
    {
        // Get departments
        $departments = Department::has('appointments')
            ->select(['id', 'name'])
            ->get();

        // Get categories
        $categories = Category::has('pets')
            ->select(['id', 'name'])
            ->get();

        // Get breeds
        $breeds = Breed::has('pets')
            ->select(['id', 'name'])
            ->get();

        return response()->json(['filters' => [
            'departments' => $departments,
            'categories'  => $categories,
            'breeds'      => $breeds,
        ]]);
    }
}
