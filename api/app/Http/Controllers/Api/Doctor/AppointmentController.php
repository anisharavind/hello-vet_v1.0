<?php

namespace App\Http\Controllers\Api\Doctor;

use PDF;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;
use App\Notifications\Prescription;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\AppointmentResource;
use App\Http\Requests\Api\DiagnosisCreateRequest;
use App\Http\Requests\Api\PrescriptionCreateRequest;

class AppointmentController extends Controller
{
    /**
     * Show appointment.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        // Load relations
        $appointment->load([
            'user',
            'pet.category',
            'questions.question.type',
            'prescriptions',
            'diagnosis'
        ]);

        return new AppointmentResource($appointment);
    }

    /**
     * Add prescription.
     *
     * @return \Illuminate\Http\Response
     */
    public function prescription(PrescriptionCreateRequest $request, Appointment $appointment)
    {
        // Get input
        $input = $request->validated();

        // Load relations
        $appointment->load(['user', 'pet.category']);

        // Db transaction
        DB::transaction(function () use ($input, $appointment) {
            // Add prescriptions
            $appointment->prescriptions()
                ->createMany($input['prescriptions']);

            // Create pdf
            $pdf = PDF::loadView('prescription', compact('appointment'));

            $path = 'appointments/'.sha1($appointment->id).'.pdf';

            // Upload pdf
            Storage::put($path, $pdf->output());

            // Update appointment
            $appointment->prescription = $path;
            $appointment->save();

            // Notify user
            $appointment->user->notify(new Prescription($appointment->id));
        });

        return response()->noContent(201);
    }

    /**
     * Add diagnosis.
     *
     * @return \Illuminate\Http\Response
     */
    public function diagnosis(DiagnosisCreateRequest $request, Appointment $appointment)
    {
        // Get input
        $input = $request->validated();

        // Create diagnosis
        $appointment->diagnosis()
            ->updateOrCreate([], ['content' => $input['content']]);

        return response()->noContent(201);
    }
}
