<?php

namespace App\Http\Controllers\Api\Doctor;

use App\Models\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\DepartmentResource;
use App\Http\Requests\Api\DepartmentCreateRequest;
use App\Http\Requests\Api\DepartmentUpdateRequest;

class DepartmentController extends Controller
{
    /**
     * List departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get departments
        $departments = Department::with(['questions'])->get();

        return DepartmentResource::collection($departments);
    }

    /**
     * Create department.
     *
     * @param  DepartmentCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentCreateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Create department
        $department = Department::create($input);

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $department->image = Storage::putFileAs(
                'department/'.$department->id,
                $request->file('image'),
                sha1($department->id).'.'.$extension
            );
            $department->save();
        }

        return new DepartmentResource($department);
    }

    /**
     * Show department.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        return new DepartmentResource($department);
    }

    /**
     * Update department.
     *
     * @param  DepartmentUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentUpdateRequest $request, Department $department)
    {
        // Get input
        $input = $request->validated();

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $input['image'] = Storage::putFileAs(
                'department/'.$department->id,
                $request->file('image'),
                sha1($department->id).'.'.$extension
            );
        }

        // Update department
        $department->update($input);

        return new DepartmentResource($department);
    }

    /**
     * Delete department.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        // Get image
        $image = $department->image;

        // Delete department
        $department->delete();

        if (!is_null($image)) {
            // Delete image
            Storage::delete($image);
        }

        return response()->noContent();
    }
}
