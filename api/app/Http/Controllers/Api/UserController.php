<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\UserResource;
use App\Http\Requests\Api\UserFCMRequest;
use App\Http\Requests\Api\UserPKDRequest;
use App\Http\Requests\Api\UserUpdateRequest;
use App\Http\Requests\Api\UserUploadRequest;

class UserController extends Controller
{
    /**
     * Show user.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Get user
        $user = auth()->user();

        return new UserResource($user);
    }

    /**
     * Update user.
     *
     * @param  UserUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Update user
        $user->update($input);

        return new UserResource($user);
    }

    /**
     * Upload image.
     *
     * @param  UserUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function upload(UserUploadRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Get current image
        $image = $user->image;

        // Upload image
        $input['image'] = Storage::putFile(
            'user/'.$user->id,
            $request->file('image')
        );

        // Update user
        $user->update($input);

        if (!is_null($image)) {
            Storage::delete($image);
        }

        return new UserResource($user);
    }

    /**
     * Save fcm token.
     *
     * @param  UserUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function fcm(UserFCMRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Check for changes
        if ($user->fcm_token === $input['fcm_token']) {
            return response()->noContent();
        }

        // Reset all other user tokens
        User::where('fcm_token', $input['fcm_token'])
            ->update(['fcm_token' => null]);

        // Update user
        $user->fcm_token = $input['fcm_token'];

        if ($input['android']) {
            $user->pkd_token = null;
        }

        $user->save();

        return response()->noContent();
    }

    /**
     * Save pkd token.
     *
     * @param  UserPKDRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function pkd(UserPKDRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Check for changes
        if ($user->pkd_token === $input['pkd_token']) {
            return response()->noContent();
        }

        // Reset all other user tokens
        User::where('pkd_token', $input['pkd_token'])
            ->update(['pkd_token' => null]);

        // Update user
        $user->pkd_token = $input['pkd_token'];
        $user->save();

        return response()->noContent();
    }

    /**
     * Delete user.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        // Get user
        $user = auth()->user();

        // Get user id
        $id = $user->id;

        // Delte user
        $user->delete();

        // Delete user directory
        Storage::deleteDirectory('user/'.$id);

        return response()->noContent();
    }
}
