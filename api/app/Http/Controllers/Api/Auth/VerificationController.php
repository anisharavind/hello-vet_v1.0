<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Notifications\VerifyOtp;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\UserResource;
use App\Http\Requests\Api\Auth\ResendRequest;
use App\Http\Requests\Api\Auth\VerificationRequest;

class VerificationController extends Controller
{
    /**
     * Login user.
     *
     * @param  VerificationRequest  $request
     * @return UserResource
     */
    public function verify(VerificationRequest $request)
    {
        // Filter inputs
        $input = $request->validated();
        $phone = filterPhone($input['phone']);

        // Get user
        $user = User::with(['verification'])
            ->where('phone', $phone)
            ->firstOrFail();

        // Check verification code
        if (is_null($user->verification)) {
            return validationError(['code' => [trans('auth.invalid_code')]]);
        }

        // Check verification code
        if ((int) $user->verification->code !== (int) $input['code']) {
            return validationError(['code' => [trans('auth.invalid_code')]]);
        }

        // Check verification code expiry (10 min)
        if ($user->verification->updated_at->addMinutes(10)->isPast()) {
            return validationError(['code' => [trans('auth.code_expired')]]);
        }

        // Delelte verification code
        $user->verification()->delete();

        // Create access token
        $token = $user->createToken(config('app.name'))->plainTextToken;

        // Set home screen
        $appointment = $user->appointments()->first();
        $home = is_null($appointment) ? 'booking' : 'appointments';

        return (new UserResource($user))
            ->additional(['meta' => ['token' => $token, 'home' => $home]]);
    }

    /**
     * Resend verification email.
     *
     * @param  ResendRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(ResendRequest $request)
    {
        // Filter inputs
        $input = $request->validated();
        $phone = filterPhone($input['phone']);

        // Get user
        $user = User::with('verification')
            ->where('phone', $phone)
            ->firstOrFail();

        // Wait for 1 min before sending again
        if (
            ! is_null($user->verification) &&
            $user->verification->updated_at->addMinutes(1)->isFuture()
        ) {
            return validationError(['code' => [trans('auth.wait')]]);
        }

        // Create/update verification code
        $code = OtpCode();
        $verification = $user->verification()->updateOrCreate([], ['code' => $code]);

        // Send notification
        $user->notify(new VerifyOtp($verification));

        return  response()->noContent();
    }
}