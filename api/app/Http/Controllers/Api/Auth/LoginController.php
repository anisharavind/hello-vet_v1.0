<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Notifications\VerifyOtp;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\UserResource;
use App\Http\Requests\Api\Auth\LoginRequest;

class LoginController extends Controller
{
    /**
     * Login user.
     *
     * @param  LoginRequest  $request
     * @return UserResource
     */
    public function login(LoginRequest $request)
    {
        // Filter inputs
        $input = $request->validated();
        $phone = filterPhone($input['phone']);

        // Demo user login
        if (!is_null(config('app.demo_number')) && $phone === config('app.demo_number')) {
            return $this->demoUser($phone);
        }

        // Get/create user
        $user = User::firstOrCreate(['phone' => $phone]);

        // Wait for 1 min before sending again
        if (
            ! is_null($user->verification) &&
            $user->verification->updated_at->addMinutes(1)->isFuture()
        ) {
            return validationError(['phone' => [trans('auth.wait')]]);
        }

        // Create/update verification code
        $code = OtpCode();
        $verification = $user->verification()->updateOrCreate([], ['code' => $code]);

        // Send notification
        $user->notify(new VerifyOtp($verification));

        return  response()->noContent();
    }

    /**
     * Logout user.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        // Get user
        $user = auth()->user();

        // Revoke tokens
        $user->tokens()->delete();

        return response()->noContent();
    }

    /**
     * Auth for demo user
     *
     * @return string
     */
    public function demoUser($phone)
    {
        // Get or create demo user
        $user = User::firstOrCreate(
            ['phone' => $phone],
            ['name'  => 'DEMO USER']
        );

        // Update or create verification code
        $user->verification()->updateOrCreate([], ['code' => config('app.demo_code')]);

        return response()->noContent();
    }
}