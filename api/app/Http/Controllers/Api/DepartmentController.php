<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Models\Pet;
use App\Models\Department;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class DepartmentController extends Controller
{
    /**
     * List departments.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Get departments
        $departments = Department::with('questions.type')->get();

        // Get pet
        $pet = Pet::find(request()->query('pet'));
        $subscription = false;
        
        if ($pet) {
            // Check subscription
            $date = $pet->subscription_ends_at ? Carbon::parse($pet->subscription_ends_at) : null;
            $subscription = $date && $date->isAfter(now());
        }

        $departments = $departments->map(function ($dep) use ($subscription) {
            return [
                'id'        => $dep['id'],
                'name'      => $dep['name'],
                'fee'       => $subscription ? 0 : $dep['fee'],
                'image'     => $dep['image'] ? Storage::url($dep['image']) : $dep['image'],
                'questions' => $dep['questions'],
            ];
        });

        return response()->json(['data' => $departments]);
    }
}
