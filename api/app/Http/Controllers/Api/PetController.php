<?php

namespace App\Http\Controllers\Api;

use App\Models\Pet;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\Api\PetResource;
use App\Http\Requests\Api\PetCreateRequest;
use App\Http\Requests\Api\PetUpdateRequest;

class PetController extends Controller
{
    /**
     * List pets.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get user
        $user = auth()->user();

        // Get pets
        $pets = Pet::with(['category', 'breed'])
            ->where('user_id', $user->id)
            ->get();

        return PetResource::collection($pets);
    }

    /**
     * Create pet.
     *
     * @param  PetCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PetCreateRequest $request)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        if ($request->filled('dob') && $request->filled('age')) {
            $input['age'] = null;
        }

        // Create pet
        $pet = $user->pets()->create($input);

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $pet->image = Storage::putFileAs(
                'user/'.$user->id.'/pets',
                $request->file('image'),
                sha1($pet->id).'.'.$extension
            );
            $pet->save();
        }

        return new PetResource($pet);
    }

    /**
     * Show pet.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Pet $pet)
    {
        // Load relations
        $pet->load(['category', 'breed']);

        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $pet->user_id);

        return new PetResource($pet);
    }

    /**
     * Update pet.
     *
     * @param  PetUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(PetUpdateRequest $request, Pet $pet)
    {
        // Get input
        $input = $request->validated();

        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $pet->user_id);

        if ($request->hasFile('image')) {
            // Upload image
            $extension = $request->file('image')->extension();
            $input['image'] = Storage::putFileAs(
                'user/'.$user->id.'/pets',
                $request->file('image'),
                sha1($pet->id).'.'.$extension
            ).'?v='.time();
        }

        // Update pet
        $pet->update($input);

        return new PetResource($pet);
    }

    /**
     * Delete pet.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pet $pet)
    {
        // Get user
        $user = auth()->user();

        // Check ownership
        checkOwnership($user->id, $pet->user_id);

        // Get image
        $image = $pet->image;

        // Delte pet
        $pet->delete();

        if (!is_null($image)) {
            // Delete image
            Storage::delete($image);
        }

        return response()->noContent();
    }
}
