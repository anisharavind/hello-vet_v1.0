<?php

namespace App\Http\Controllers\Api;

use App\Models\Breed;
use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\BreedResource;
use App\Http\Resources\Api\CategoryResource;

class CategoryController extends Controller
{
    /**
     * List categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get categories
        $categories = Category::all();

        return CategoryResource::collection($categories);
    }

    /**
     * List category breeds.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function breeds($id)
    {
        // Get breeds
        $breeds = Breed::where('category_id', $id)->get();

        return BreedResource::collection($breeds);
    }
}
