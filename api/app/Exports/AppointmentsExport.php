<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AppointmentsExport implements FromView
{
    protected $appointments;
    protected $columns;

    public function __construct($appointments, $columns)
    {
        $this->appointments = $appointments;
        $this->columns = explode(',', $columns);
    }

    public function view(): View
    {
        return view('reports', [
            'appointments' => $this->appointments,
            'columns'      => $this->columns
        ]);
    }
}
