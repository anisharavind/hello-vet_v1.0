<?php

namespace App\Notifications;

use App\Channels\FcmChannel;
use App\Channels\PkdChannel;
use Illuminate\Bus\Queueable;
use App\Channels\Messages\FcmMessage;
use App\Channels\Messages\PkdMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class CallUser extends Notification implements ShouldQueue
{
    use Queueable;

    public $channel;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($channel)
    {
        $this->channel = $channel;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $notifiable->pkd_token ? [PkdChannel::class] : [FcmChannel::class];
    }

    /**
     * Get the fcm representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return FcmMessage
     */
    public function toFcm($notifiable)
    {
        return (new FcmMessage)->content($this->channel);
    }

    /**
     * Get the pkd representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return PkdMessage
     */
    public function toPkd($notifiable)
    {
        return (new PkdMessage)->content($this->channel);
    }
}
