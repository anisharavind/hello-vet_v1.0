<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use App\Models\Verification;
use Illuminate\Bus\Queueable;
use App\Channels\Messages\SmsMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyOtp extends Notification implements ShouldQueue
{
    use Queueable;

    public $verification;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Verification $verification)
    {
        $this->verification = $verification;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SmsMessage
     */
    public function toSms($notifiable)
    {
        $app  = config('app.name');
        $hash = config('app.android_hash');

        return (new SmsMessage)
            ->content("<#> Your {$app} verification OTP code is {$this->verification->code}. Code valid for 10 minutes. Please DO NOT share this OTP with anyone.\n{$hash}");
    }
}
