<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\PrescriptionChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Channels\Messages\PrescriptionMessage;

class Prescription extends Notification implements ShouldQueue
{
    use Queueable;

    public $appointment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PrescriptionChannel::class];
    }

    /**
     * Get the fcm representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return PrescriptionMessage
     */
    public function toFcm($notifiable)
    {
        return (new PrescriptionMessage)->content($this->appointment);
    }
}
