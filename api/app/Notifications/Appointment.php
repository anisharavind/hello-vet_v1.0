<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use App\Channels\AppointmentChannel;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Channels\Messages\AppointmentMessage;

class Appointment extends Notification implements ShouldQueue
{
    use Queueable;

    public $appointment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [AppointmentChannel::class];
    }

    /**
     * Get the fcm representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return AppointmentMessage
     */
    public function toFcm($notifiable)
    {
        return (new AppointmentMessage)->content($this->appointment);
    }
}
