# Installation

### Prerequisites
  - Node >= 10
  - Yarn
  - Watchman
  - Cocoapods
  - [Android Environment Setup](https://reactnative.dev/docs/environment-setup)
  - [iOS Environment Setup](https://reactnative.dev/docs/environment-setup)

### Install JS Dependencies
```sh
$ yarn
```

### Install iOS dependencies
```sh
$ cd ios
$ pod install
```

### Run app on Android emulator
```sh
$ yarn android
```

### Run app on iOS emulator
```sh
$ yarn ios
```