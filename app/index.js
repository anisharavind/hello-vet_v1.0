import 'react-native-gesture-handler'
import get from 'lodash/get'
import { AppRegistry } from 'react-native'
import RNVoipCall from 'react-native-voip-call'
import { enableScreens } from 'react-native-screens'
import messaging from '@react-native-firebase/messaging'
import LaunchApplication from 'react-native-bring-foreground'

import App from './app/App'
import { store } from './app/store'
import { name as appName } from './app.json'
import { setVideo } from './app/store/notifications'

// eslint-disable-next-line no-console
// console.disableYellowBox = true

enableScreens()

messaging().setBackgroundMessageHandler(async (message) => {
  const type = get(message, 'data.type')

  if (type === 'call') {
    const name = 'HelloFelican'
    const uid = get(message, 'data.uid') || null
    const phone = get(message, 'data.phone') || null
    const channel = get(message, 'data.channel') || null

    store.dispatch(setVideo({ uid, phone, channel }))

    const options = {
      callerId: uid,
      ios: {
        phoneNumber: phone,
        name,
        hasVideo: true,
      },
      android: {
        ringtuneSound: true,
        ringtune: 'ringtune',
        duration: 30000,
        vibration: true,
        channel_name: 'Video Call',
        notificationId: 1123,
        notificationTitle: 'Incomming Video Call',
        notificationBody: `${name} is Calling...`,
        answerActionTitle: 'Answer',
        declineActionTitle: 'Decline',
      },
    }

    try {
      LaunchApplication.open('net.in.mars.felican')
      await RNVoipCall.displayIncomingCall(options)
    } catch (error) {
      //
    }
  }
})

AppRegistry.registerComponent(appName, () => App)
