module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  env: {
    development: {
      plugins: [
        ['import', { libraryName: '@ant-design/react-native' }],
        [
          'module-resolver',
          {
            root: ['./app'],
            alias: {
              app: './app',
            },
          },
        ],
      ],
    },
  },
}
