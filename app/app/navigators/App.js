import get from 'lodash/get'
import { Platform } from 'react-native'
import React, { useEffect } from 'react'
import RNBootSplash from 'react-native-bootsplash'
import { RNVoipPushKit } from 'react-native-voip-call'
import { useSelector, useDispatch } from 'react-redux'
import messaging from '@react-native-firebase/messaging'
import { createStackNavigator } from '@react-navigation/stack'

import request from '../libs/request'
import Users from '../screens/Doctor/Users'
import { navigate } from '../libs/navigation'
import { setRoute, setVideo } from '../store/notifications'
import {
  Pet,
  Home,
  Pets,
  Video,
  Booking,
  Profile,
  Payment,
  Success,
  Personal,
  Settings,
  PetProfile,
  Appointment,
  BookingDate,
  BookingForm,
  ProfileInfo,
  Subscription,
  AppointmentEdit,
  AppointmentCancel,
} from '../screens/App'

const Stack = createStackNavigator()

const isIos = Platform.OS === 'ios'

const AppNavigator = ({ showProfile }) => {
  const dispatch = useDispatch()
  const { route } = useSelector((state) => state.notifications)

  const uploadFcm = async (token) => {
    try {
      const android = isIos ? 0 : 1
      await request.post('user/fcm', { fcm_token: token, android })
    } catch (error) {
      //
    }
  }

  const uploadPkd = async (token) => {
    try {
      await request.post('user/pkd', { pkd_token: token })
    } catch (error) {
      //
    }
  }

  useEffect(() => {
    const init = async () => {
      try {
        if (isIos) {
          RNVoipPushKit.requestPermissions()

          RNVoipPushKit.getPushKitDeviceToken((res) =>
            uploadPkd(res.deviceToken)
          )

          RNVoipPushKit.RemotePushKitNotificationReceived((notification) => {
            const uid = get(notification, 'data.uid') || null
            const phone = get(notification, 'data.phone') || null
            const channel = get(notification, 'data.channel') || null

            dispatch(setVideo({ uid, phone, channel }))
          })
        }

        const token = await messaging().getToken()
        await uploadFcm(token)
      } catch (error) {
        //
      }
    }

    init()
    RNBootSplash.hide({ duration: 250 })

    return messaging().onTokenRefresh(uploadFcm)
  }, [])

  useEffect(() => {
    if (route) {
      dispatch(setRoute(null))
      navigate(route.name, route.params)
    }
  }, [route])

  return (
    <Stack.Navigator headerMode="none">
      {showProfile && <Stack.Screen name="Personal" component={Personal} />}
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Booking" component={Booking} />
      <Stack.Screen name="BookingDate" component={BookingDate} />
      <Stack.Screen name="BookingForm" component={BookingForm} />
      <Stack.Screen name="Payment" component={Payment} />
      <Stack.Screen name="Settings" component={Settings} />
      <Stack.Screen name="Appointment" component={Appointment} />
      <Stack.Screen name="AppointmentEdit" component={AppointmentEdit} />
      <Stack.Screen name="AppointmentCancel" component={AppointmentCancel} />
      <Stack.Screen name="ProfileInfo" component={ProfileInfo} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="Pets" component={Pets} />
      <Stack.Screen name="PetProfile" component={PetProfile} />
      <Stack.Screen name="Pet" component={Pet} />
      <Stack.Screen name="Success" component={Success} />
      <Stack.Screen name="Subscription" component={Subscription} />
      <Stack.Screen name="Video" component={Video} />
      <Stack.Screen name="Users" component={Users} />
    </Stack.Navigator>
  )
}

export default AppNavigator
