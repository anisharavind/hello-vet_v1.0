import { Platform } from 'react-native'
import React, { useEffect } from 'react'
import RNBootSplash from 'react-native-bootsplash'
import { RNVoipPushKit } from 'react-native-voip-call'
import messaging from '@react-native-firebase/messaging'
import { createDrawerNavigator } from '@react-navigation/drawer'

import { colors } from '../config'
import request from '../libs/request'
import { textStyles } from '../components/Text'
import { Drawer as DrawerContent } from '../components'
import {
  Video,
  History,
  Calendar,
  Dashboard,
  Diagnosis,
  Timeslots,
  Appointment,
  Appointments,
  Prescription,
} from '../screens/Doctor'

const Drawer = createDrawerNavigator()
const options = {
  activeTintColor: colors.primary,
  inactiveTintColor: colors.text,
  activeBackgroundColor: colors.transparent,
  labelStyle: { ...textStyles.h4, fontWeight: '400' },
}

const isIos = Platform.OS === 'ios'

const DoctorNavigator = () => {
  const uploadFcm = async (token) => {
    try {
      const android = isIos ? 0 : 1
      await request.post('user/fcm', { fcm_token: token, android })
    } catch (error) {
      //
    }
  }

  const uploadPkd = async (token) => {
    try {
      await request.post('user/pkd', { pkd_token: token })
    } catch (error) {
      //
    }
  }

  useEffect(() => {
    const init = async () => {
      try {
        if (isIos) {
          RNVoipPushKit.requestPermissions()

          RNVoipPushKit.getPushKitDeviceToken((res) =>
            uploadPkd(res.deviceToken)
          )
        }

        const token = await messaging().getToken()
        await uploadFcm(token)
      } catch (error) {
        //
      }
    }

    init()
    RNBootSplash.hide({ duration: 250 })

    return messaging().onTokenRefresh(uploadFcm)
  }, [])

  return (
    <Drawer.Navigator
      drawerPosition="right"
      drawerContentOptions={options}
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="Dashboard" component={Dashboard} />
      <Drawer.Screen name="Calendar" component={Calendar} />
      <Drawer.Screen name="Timeslots" component={Timeslots} />
      <Drawer.Screen name="Appointments" component={Appointments} />
      <Drawer.Screen name="Appointment" component={Appointment} />
      <Drawer.Screen name="Prescription" component={Prescription} />
      <Drawer.Screen name="Diagnosis" component={Diagnosis} />
      <Drawer.Screen name="History" component={History} />
      <Drawer.Screen name="Video" component={Video} />
    </Drawer.Navigator>
  )
}

export default DoctorNavigator
