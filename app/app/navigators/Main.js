import React, { useState, useEffect } from 'react'
import get from 'lodash/get'
import RNVoipCall from 'react-native-voip-call'
import { Toast } from 'react-native-root-toaster'
import { useSelector, useDispatch } from 'react-redux'
import messaging from '@react-native-firebase/messaging'

import { navigate } from '../libs/navigation'
import { setRoute, setVideo } from '../store/notifications'

import AppNavigator from './App'
import AuthNavigator from './Auth'
import DoctorNavigator from './Doctor'

const endCallNotification = () => {
  RNVoipCall.endAllCalls()
  RNVoipCall.stopRingtune()
}

const MainNavigator = () => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(true)

  const { user, token } = useSelector((state) => state.user)

  const getRoute = (message) => {
    let params
    const id = get(message, 'data.param') || null
    const name = get(message, 'data.screen') || null

    if (id) {
      params = { id }
    }

    return name ? { name, params } : null
  }

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async (message) => {
      const route = getRoute(message)
      const type = get(message, 'data.type') || null

      if (type === 'call') {
        const name = 'Hello Felican'
        const uid = get(message, 'data.uid') || null
        const phone = get(message, 'data.phone') || null
        const channel = get(message, 'data.channel') || null

        dispatch(setVideo({ uid, phone, channel }))

        const options = {
          callerId: uid,
          ios: {
            phoneNumber: phone,
            name,
            hasVideo: true,
          },
          android: {
            ringtuneSound: true,
            ringtune: 'ringtune',
            duration: 30000,
            vibration: true,
            channel_name: 'Video Call',
            notificationId: 1123,
            notificationTitle: 'Incomming Video Call',
            notificationBody: `${name} is Calling...`,
            answerActionTitle: 'Answer',
            declineActionTitle: 'Decline',
          },
        }

        try {
          await RNVoipCall.displayIncomingCall(options)
        } catch (error) {
          //
        }

        return null
      }

      return navigate(route.name, route.params)
    })

    return unsubscribe
  }, [])

  useEffect(() => {
    messaging().onNotificationOpenedApp((message) => {
      const route = getRoute(message)

      if (route) {
        navigate(route.name, route.params)
        endCallNotification()
      }
    })

    messaging()
      .getInitialNotification()
      .then((message) => {
        if (message) {
          const route = getRoute(message)

          if (route) {
            dispatch(setRoute(route))
            RNVoipCall.endAllCalls()
            RNVoipCall.stopRingtune()
          }
        }

        setLoading(false)
      })

    RNVoipCall.getInitialNotificationActions()
      .then(() => {
        dispatch(setRoute({ name: 'Video' }))
        endCallNotification()
      })
      .catch(() => {})

    RNVoipCall.onCallAnswer(() => {
      dispatch(setRoute({ name: 'Video' }))
      endCallNotification()
    })
  }, [])

  useEffect(() => {
    async function requestPermission() {
      const authStatus = await messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if (!enabled) {
        Toast.show('Missing notification permission')
      }
    }

    RNVoipCall.initializeCall({
      appName: 'Hello Felican'
    }).then(() => {}).catch(e => {});

    requestPermission()
  }, [])

  if (loading) return null

  if (token && user) {
    return user.roles?.includes('admin') ? (
      <DoctorNavigator />
    ) : (
      <AppNavigator showProfile={!user.name} />
    )
  }

  return <AuthNavigator />
}

export default MainNavigator
