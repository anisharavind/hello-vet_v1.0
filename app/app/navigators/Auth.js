import React, { useEffect } from 'react'
import RNBootSplash from 'react-native-bootsplash'
import { createStackNavigator } from '@react-navigation/stack'

import { Welcome, Login, Verify } from '../screens/Auth'

const Stack = createStackNavigator()

const AuthNavigator = () => {
  useEffect(() => RNBootSplash.hide({ duration: 250 }), [])

  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Welcome" component={Welcome} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Verify" component={Verify} />
    </Stack.Navigator>
  )
}

export default AuthNavigator
