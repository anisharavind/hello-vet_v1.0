import React, { useEffect } from 'react'
import { Provider } from 'react-redux'
import { RootToaster } from 'react-native-root-toaster'
import { PersistGate } from 'redux-persist/integration/react'
import { NavigationContainer } from '@react-navigation/native'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import { store, persistor } from './store'
import MainNavigator from './navigators/Main'
import { isMountedRef, navigationRef } from './libs/navigation'

const App = () => {
  useEffect(() => {
    isMountedRef.current = true
  }, [])

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <SafeAreaProvider>
          <NavigationContainer ref={navigationRef}>
            <MainNavigator />
          </NavigationContainer>
          <RootToaster />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  )
}

export default App
