import { createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
  name: 'user',
  initialState: {
    user: null,
    token: null,
    home: 'booking',
  },
  reducers: {
    setUser(state, action) {
      state.user = action.payload
    },
    setToken(state, action) {
      state.token = action.payload
    },
    setHome(state, action) {
      state.home = action.payload
    },
    logOut(state) {
      state.user = null
      state.token = null
      state.home = 'booking'
    },
  },
})

const { actions, reducer } = userSlice
export const { setUser, setToken, setHome, logOut } = actions
export default reducer
