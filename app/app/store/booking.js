import { createSlice } from '@reduxjs/toolkit'

const bookingSlice = createSlice({
  name: 'booking',
  initialState: {
    pet: null,
    date: null,
  },
  reducers: {
    setPet(state, action) {
      state.pet = action.payload
    },
    setDate(state, action) {
      state.date = action.payload
    },
    reset(state) {
      state.pet = null
      state.date = null
    },
  },
})

const { actions, reducer } = bookingSlice
export const { setPet, setDate, reset } = actions
export default reducer
