import { createSlice } from '@reduxjs/toolkit'

const notificationsSlice = createSlice({
  name: 'notifications',
  initialState: {
    route: null,
    video: null,
  },
  reducers: {
    setRoute(state, action) {
      state.route = action.payload
    },
    setVideo(state, action) {
      state.video = action.payload
    },
  },
})

const { actions, reducer } = notificationsSlice
export const { setRoute, setVideo } = actions
export default reducer
