import { combineReducers } from 'redux'
import { configureStore } from '@reduxjs/toolkit'
import { persistReducer, persistStore } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'

import user from './user'
import booking from './booking'
import notifications from './notifications'

const persistConfig = {
  key: 'root',
  whitelist: ['user'],
  storage: AsyncStorage,
}

const rootReducer = combineReducers({ user, booking, notifications })

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
  reducer: persistedReducer,
  middleware: [],
})

export const persistor = persistStore(store)
