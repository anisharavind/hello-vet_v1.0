import { StyleSheet } from 'react-native'

import { colors } from '../config'
import { normalize } from '../libs/utils'

export const sheet = { container: { height: 'auto' } }

const styles = StyleSheet.create({
  absolute: {
    position: 'absolute',
  },
  audio: {
    backgroundColor: colors.textAlt,
  },
  borderLeft: {
    borderLeftColor: colors.shade300,
    borderLeftWidth: 1,
  },
  borderRight: {
    borderRightColor: colors.text200,
    borderRightWidth: 1,
  },
  borderTop: {
    borderTopColor: colors.shade300,
    borderTopWidth: 1,
  },
  buttonWrapper: {
    bottom: 0,
    position: 'absolute',
    zIndex: 100,
  },
  buttons: {
    alignItems: 'center',
    borderRadius: normalize(48),
    height: normalize(48),
    justifyContent: 'center',
    marginHorizontal: normalize(12),
    width: normalize(48),
  },
  camera: {
    backgroundColor: colors.textAlt,
  },
  count: {
    position: 'absolute',
    right: 4,
    top: 4,
  },
  countdown: {
    left: 0,
    position: 'absolute',
    width: 160,
  },
  disconnect: {
    backgroundColor: colors.danger,
  },
  grow: {
    flexGrow: 1,
  },
  localView: {
    height: normalize(150),
    position: 'absolute',
    right: 28,
    top: 28,
    width: normalize(120),
    zIndex: 100,
  },
  noShrink: { flexShrink: 0 },
  overlay: {
    ...StyleSheet.absoluteFill,
    zIndex: 99,
  },
  pb12: {
    paddingBottom: 12,
  },
  px20: {
    paddingHorizontal: 20,
  },
  relative: {
    position: 'relative',
  },
})

export default styles
