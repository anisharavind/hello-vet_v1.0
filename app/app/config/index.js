const config = {
  // API_URL: 'http://192.168.1.4:5000/api',
  // PAYMENT_URL: 'http://192.168.1.4:5000/payments',
  // SUCCESS_URL: 'http://192.168.1.4:5000/success',
  API_URL: 'https://api.felican.in/api',
  PAYMENT_URL: 'https://api.felican.in/payments',
  SUCCESS_URL: 'https://api.felican.in/success',
}

export const colors = {
  primary: 'rgb(127, 55, 196)',
  primary100: 'rgba(127, 55, 196, 0.1)',
  primary200: 'rgba(127, 55, 196, 0.2)',
  primary300: 'rgba(127, 55, 196, 0.3)',
  primary500: 'rgba(127, 55, 196, 0.5)',
  primary800: 'rgba(127, 55, 196, 0.8)',
  secondary: 'rgba(127, 55, 196, 0.8)',
  text: 'rgb(0, 0, 0)',
  text200: 'rgba(0, 0, 0, 0.2)',
  text400: 'rgba(0, 0, 0, 0.4)',
  text700: 'rgba(0, 0, 0, 0.7)',
  textAlt: '#ffffff',
  bg: '#ffffff',
  shade: 'rgb(196, 196, 196)',
  shade100: '#f5f5f5',
  shade150: 'rgba(196, 196, 196, 0.15)',
  shade300: 'rgba(196, 196, 196, 0.3)',
  shade500: 'rgba(196, 196, 196, 0.5)',
  success: 'rgb(11, 161, 62)',
  success100: 'rgba(11, 161, 62, 0.1)',
  success300: 'rgba(11, 161, 62, 0.3)',
  danger: 'rgb(233, 2, 2)',
  danger100: 'rgba(233, 2, 2, 0.1)',
  danger300: 'rgba(233, 2, 2, 0.3)',
  danger500: 'rgba(233, 2, 2, 0.5)',
  info: 'rgb(23, 162, 184)',
  info100: 'rgba(23, 162, 184, 0.1)',
  info300: 'rgba(23, 162, 184, 0.3)',
  warning: 'rgb(255, 193, 7)',
  warning100: 'rgba(255, 193, 7, 0.1)',
  warning300: 'rgba(255, 193, 7, 0.3)',
  transparent: 'transparent',
}

export const images = {
  logo: require('../assets/images/logo.png'),
  hero: require('../assets/images/hero.png'),
  department: require('../assets/images/department.png'),
  success: require('../assets/images/success.png'),
}

export const lottie = {
  error: require('../assets/lottie/error.json'),
}

export const sex = [
  { id: 1, name: 'Female', value: 0 },
  { id: 2, name: 'Male', value: 1 },
]

export default config
