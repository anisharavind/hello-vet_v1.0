import moment from 'moment'
import get from 'lodash/get'
import keys from 'lodash/keys'
import { Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')

export const normalize = (size, round = false) => {
  const factor = 0.5
  const baseline = 375
  const current = height > width ? width : height
  const normalized = size + ((current / baseline) * size - size) * factor

  return round ? Math.round(normalized) : normalized
}

export const firstError = (error) =>
  get(error, `errors[${keys(error.errors)[0]}].0`) || null

export const errorMsg = (errors, touched) => {
  const error = errors && touched ? errors : null

  return Array.isArray(error) ? error[0] : error
}

export const getAge = (dob) => {
  const diff = moment().diff(dob, 'months')
  const years = Math.floor(diff / 12)
  const months = diff % 12

  return { years, months }
}

export const isPending = (item) => !item.order_id || !item.paid_at

export const isCancelled = (item) =>
  item.cancelled_at || (!item.paid_at && moment(item.date).isBefore())

export const numberFormat = (number) => {
  if (!number) return 0

  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

const pad = (num) => `0${num}`.slice(-2)

export const hhmmss = (sec) => {
  let minutes = Math.floor(sec / 60)
  const secs = sec % 60
  const hours = Math.floor(minutes / 60)
  minutes %= 60

  return `${pad(hours)}:${pad(minutes)}:${pad(secs)}`
}
