import useSWR from 'swr'

import request from './request'

const useFetch = (url, { initialData, ...config } = {}) => {
  return useSWR(url, () => request.get(url).then((response) => response.data), {
    ...config,
    initialData: initialData && {
      status: 200,
      statusText: 'InitialData',
      headers: {},
      data: initialData,
    },
  })
}

export default useFetch
