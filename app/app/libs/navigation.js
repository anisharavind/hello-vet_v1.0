import React from 'react'

export const isMountedRef = React.createRef()

export const navigationRef = React.createRef()

export function navigate(name, params) {
  if (isMountedRef.current && navigationRef.current) {
    navigationRef.current.navigate(name, params)
  }
}

export function route() {
  if (isMountedRef.current && navigationRef.current) {
    return navigationRef.current.getCurrentRoute()
  }

  return null
}
