import axios from 'axios'
import { cache } from 'swr'
import get from 'lodash/get'

import config from '../config'
import { store } from '../store'
import { logOut } from '../store/user'

const api = axios.create({
  baseURL: config.API_URL,
})

api.interceptors.request.use(
  (conf) => {
    const {
      user: { token },
    } = store.getState()

    if (token) {
      conf.headers.Authorization = `Bearer ${token}`
    }

    return conf
  },
  (error) => {
    return Promise.reject(error)
  }
)

api.interceptors.response.use(
  (response) => {
    return response
  },
  (error) => {
    const status = get(error, 'response.status')

    if (status === 401) {
      cache.clear()
      store.dispatch(logOut())
    }

    return Promise.reject(error)
  }
)

export default api
