import React from 'react'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { LoginForm } from '../../components/Form'
import { Box, Header, Text } from '../../components'

const Login = () => {
  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Get started" />
          <Text mt={-40} mb={56} mx={28}>
            Register with your active mobile number
          </Text>
          <Box flex px={28} pb={28}>
            <LoginForm />
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Login
