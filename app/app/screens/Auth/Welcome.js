import React from 'react'
import { useWindowDimensions, ImageBackground, StyleSheet } from 'react-native'

import { images } from '../../config'
import { Box, Text, Button } from '../../components'
import { ArrowRight } from '../../components/Svg'

const styles = StyleSheet.create({
  hero: {
    flex: 1,
    height: 800,
    justifyContent: 'center',
    resizeMode: 'cover',
    width: 400,
  },
})

const Welcome = ({ navigation }) => {
  const window = useWindowDimensions()

  const onNext = () => navigation.navigate('Login')

  return (
    <ImageBackground
      source={images.hero}
      style={[styles.hero, { width: window.width, height: window.height }]}
    >
      <Box safe>
        <Box flex px={28} py={48}>
          <Box flex>
            <Text type="h1" weight="bold" color="textAlt">
              Hellovet
            </Text>
          </Box>
          <Box>
            <Text type="h4" weight="medium" color="textAlt" mb={8}>
              A BETTER WAY TO
            </Text>
            <Text type="h1" weight="bold" color="textAlt">
              {`Consult your favorite \nveterinarian.`}
            </Text>
            <Box align="flex-start" mt={16}>
              <Button right={<ArrowRight color="textAlt" />} onPress={onNext}>
                Sign up free
              </Button>
            </Box>
          </Box>
        </Box>
      </Box>
    </ImageBackground>
  )
}

export default Welcome
