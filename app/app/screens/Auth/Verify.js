import React from 'react'
import { ScrollView } from 'react-native'
import { useSelector } from 'react-redux'

import styles from '../../styles'
import { VerifyForm } from '../../components/Form'
import { Box, Header, Text } from '../../components'

const Verify = () => {
  const { user } = useSelector((state) => state.user)

  if (!user.phone) return null

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Get started" />
          <Text mt={-40} mb={56} mx={28}>
            Enter OTP sent to +91 {user.phone}
          </Text>
          <Box flex px={28} pb={28}>
            <VerifyForm user={user} />
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Verify
