/* eslint-disable no-nested-ternary */
import React, { useState, useMemo } from 'react'
import get from 'lodash/get'
import { useSelector } from 'react-redux'
import { ScrollView, StyleSheet } from 'react-native'

import useFetch from '../../libs/useFetch'
import { Check } from '../../components/Svg'
import { AppointmentForm } from '../../components/Form'
import { Box, Text, Header, Loader, Image } from '../../components'

const styles = StyleSheet.create({
  image: {
    height: 48,
    width: 48,
  },
  scroll: {
    flexGrow: 1,
  },
})

const BookingForm = () => {
  const [department, setDepartment] = useState(null)
  const { pet, date } = useSelector((state) => state.booking)
  const { data, isValidating, mutate } = useFetch(
    pet ? `departments?pet=${pet.id}` : null
  )

  const departments = useMemo(() => get(data, 'data') || [], [data])

  const DepartmentItem = ({ item, selected = false }) => (
    <Box
      row
      round
      bg="primary100"
      align="center"
      px={16}
      py={12}
      mb={16}
      onPress={() => setDepartment(item)}
    >
      {selected ? (
        <Box
          circle
          bg="primary200"
          width={40}
          height={40}
          align="center"
          justify="center"
        >
          <Check color="textAlt" width={60} height={60} />
        </Box>
      ) : item.image ? (
        <Box circle width={40} height={40}>
          <Image source={item.image} width={40} height={40} />
        </Box>
      ) : (
        <Box circle bg="shade300" width={40} height={40} />
      )}
      <Box ml={16}>
        <Text type="h3" weight="medium">
          {item.name}
        </Text>
        <Text type="h3" weight="medium" color="text400">
          INR {item.fee}
        </Text>
      </Box>
    </Box>
  )

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header />
          <Box flex px={28} py={28}>
            {!departments ? (
              <Loader loading={isValidating} mutate={mutate} />
            ) : (
              <Box flex>
                <Text type="h3" color="text" mb={24}>
                  Departments
                </Text>
                <Text type="h4" mb={24}>
                  Select department for consultation
                </Text>
                {!department ? (
                  <Box mb={24}>
                    {departments.map((item) => (
                      <DepartmentItem item={item} key={item.id} />
                    ))}
                  </Box>
                ) : (
                  <Box mb={24}>
                    <DepartmentItem selected item={department} />
                  </Box>
                )}
                {department && (
                  <Box>
                    <Text type="h3" color="text" mb={24}>
                      {department.name} Data
                    </Text>
                    <Text type="h4" mb={24}>
                      Kindly share details for better diagnosis
                    </Text>
                    <AppointmentForm
                      pet={pet}
                      date={date}
                      department={department}
                    />
                  </Box>
                )}
              </Box>
            )}
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default BookingForm
