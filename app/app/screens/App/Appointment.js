import React, { useMemo } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { Toast } from 'react-native-root-toaster'
import { ScrollView, Linking } from 'react-native'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Download } from '../../components/Svg'
import { isPending, isCancelled } from '../../libs/utils'
import {
  Box,
  Header,
  AppointmentItem,
  Loader,
  Button,
  Text,
} from '../../components'

const Appointment = ({ route, navigation }) => {
  const { id } = route.params
  const { data, isValidating, mutate } = useFetch(`appointments/${id}`)

  const appointment = useMemo(() => get(data, 'data') || null)
  const status = useMemo(() => {
    if (!appointment) return ''
    if (isCancelled(appointment)) return 'danger'
    if (isPending(appointment)) return 'warning'
    if (moment(appointment.date).isAfter()) return 'info'

    return 'success'
  }, [appointment])

  const onEdit = () => navigation.navigate('AppointmentEdit', { appointment })
  const onPayment = () => navigation.navigate('Payment', { appointment })

  const onCancel = () =>
    navigation.navigate('AppointmentCancel', { appointment })
  const onDownload = async () => {
    try {
      await Linking.openURL(appointment.prescription)
    } catch (error) {
      Toast.show('Download failed')
    }
  }

  return (
    <Box flex safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Appointment" />
          <Box flex px={28} pb={28}>
            {!appointment ? (
              <Loader loading={isValidating} mutate={mutate} />
            ) : (
              <Box flex>
                <AppointmentItem
                  item={appointment}
                  status={status}
                  onPress={() => {}}
                />
                {appointment.prescription && (
                  <Box
                    row
                    round
                    bg="success300"
                    align="center"
                    justify="space-between"
                    onPress={onDownload}
                    mt={24}
                    px={12}
                    py={12}
                  >
                    <Text color="text">Prescription</Text>
                    <Download />
                  </Box>
                )}
                <Box align="flex-start" mt={24}>
                  {status === 'warning' && (
                    <Button small bg="secondary" onPress={onPayment} mb={16}>
                      Make the payment
                    </Button>
                  )}
                </Box>
                {status === 'info' && (
                  <Box row wrap justify="space-between">
                    <Button small bg="secondary" onPress={onEdit} mb={16}>
                      Edit appointment
                    </Button>
                    <Button small bg="secondary" onPress={onCancel}>
                      Cancel appointment
                    </Button>
                  </Box>
                )}
              </Box>
            )}
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Appointment
