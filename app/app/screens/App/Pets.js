import React, { useMemo } from 'react'
import get from 'lodash/get'
import { FlatList } from 'react-native'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Plus } from '../../components/Svg'
import { Box, Header, Button, PetItem, Loader } from '../../components'

const Pets = ({ navigation }) => {
  const { data, isValidating } = useFetch('pets')

  const pets = useMemo(() => get(data, 'data'), [data])

  const onNew = () => navigation.navigate('PetProfile')
  const onAppointment = () => navigation.navigate('Booking')
  const onPet = (item) => navigation.navigate('Pet', { id: item.id })

  const ListHeader = () => <Header title="Pet profiles" />

  const renderItem = ({ item }) => (
    <Box px={28} mb={28}>
      <PetItem item={item} onPress={() => onPet(item)} />
    </Box>
  )

  const ListFooter = () => (
    <Box align="flex-start" mt={12} px={28}>
      {!isValidating && (
        <Button
          small
          bg="secondary"
          left={<Plus color="textAlt" width={20} />}
          onPress={onNew}
        >
          Add new pet
        </Button>
      )}
    </Box>
  )

  return (
    <Box safe bg="bg">
      <FlatList
        data={pets}
        renderItem={renderItem}
        ListEmptyComponent={<Loader loading={isValidating} />}
        keyExtractor={(item) => String(item.id)}
        ListHeaderComponent={<ListHeader />}
        ListFooterComponent={<ListFooter />}
        contentContainerStyle={styles.grow}
      />

      <Box bg="bg" align="flex-end" px={28} pt={16} pb={28}>
        <Button left={<Plus color="textAlt" />} onPress={onAppointment}>
          Appointment
        </Button>
      </Box>
    </Box>
  )
}

export default Pets
