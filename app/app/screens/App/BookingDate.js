import React, { useMemo, useState } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import groupBy from 'lodash/groupBy'
import { ScrollView } from 'react-native'
import { Toast } from 'react-native-root-toaster'
import { useDispatch, useSelector } from 'react-redux'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Check } from '../../components/Svg'
import { setDate as setTimeSlot } from '../../store/booking'
import { Box, Text, Header, PetItem, Loader, Button } from '../../components'

const BookingDate = ({ navigation }) => {
  const dispatch = useDispatch()
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'))
  const [time, setTime] = useState(null)
  const { pet } = useSelector((state) => state.booking)

  const { data, isValidating, mutate } = useFetch('timeslots')

  const slots = useMemo(() => get(data, 'data') || [], [data])
  const dates = useMemo(() => {
    return groupBy(slots, (s) => moment(s.date).format('YYYY-MM-DD'))
  }, [slots])
  const times = useMemo(() => {
    let ts = dates[date] || []
    ts = ts.map((t) => t.date)

    return new Set(ts)
  }, [dates, date])

  const alltimes = useMemo(() => {
    if (!date) return []
    if (!Object.keys(dates).includes(date)) return []

    return [...Array(24 * 2).keys()].map((t) =>
      moment(date)
        .hour(0)
        .startOf('hour')
        .add(t * 30, 'minutes')
    )
  }, [dates, date])

  const getBg = (f) => {
    if (time === f) return 'primary300'
    if (times.has(f)) return 'success'

    return 'danger'
  }

  const chooseDate = (d) => {
    setDate(d)
    setTime(null)
  }
  const onNext = () => {
    dispatch(setTimeSlot(time))
    navigation.push('BookingForm')
  }

  if (!pet) {
    Toast.show('Select a pet')
    return null
  }

  const Date = ({ d }) => {
    const day = moment(d)

    return (
      <Box
        circle
        height={56}
        width={56}
        bg={date === d ? 'primary300' : 'primary100'}
        align="center"
        justify="center"
        mr={16}
        onPress={() => chooseDate(d)}
      >
        <Text type="p3">{day.format('ddd')}</Text>
        <Text type="h3">{day.format('DD')}</Text>
      </Box>
    )
  }
  const Time = ({ t }) => {
    const formatted = t.format('YYYY-MM-DD HH:mm:ss')

    return (
      <Box bg={getBg(formatted)} basis="33.33%" border="textAlt">
        <Box
          align="center"
          justify="center"
          height={48}
          onPress={() => times.has(formatted) && setTime(formatted)}
        >
          <Text color={time === formatted ? 'text' : 'textAlt'}>
            {t.format('hh:mm A')}
          </Text>
        </Box>
      </Box>
    )
  }

  return (
    <Box safe bg="bg" style={styles.relative}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Book an appointment" />
          <Box flex px={28} pb={100}>
            {!dates ? (
              <Loader loading={isValidating} mutate={mutate} />
            ) : (
              <Box flex>
                <Box
                  row
                  largeRound
                  align="center"
                  border="shade500"
                  justify="space-between"
                  mb={32}
                  px={16}
                  py={16}
                >
                  <PetItem item={pet} onPress={() => null} />
                  <Box pr={8}>
                    <Check color="primary500" />
                  </Box>
                </Box>

                <Box row align="center" justify="space-between" mb={24}>
                  <Text type="h3" color="text">
                    Select date and time
                  </Text>
                </Box>
                <Box height={64} mb={28} mx={-28}>
                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <Box row align="center" px={20} pb={8}>
                      {Object.keys(dates).map((d) => (
                        <Date d={d} key={d} />
                      ))}
                    </Box>
                  </ScrollView>
                </Box>

                <Box row wrap align="center">
                  {alltimes.map((t) => (
                    <Time t={t} key={t.valueOf()} />
                  ))}
                </Box>
              </Box>
            )}
          </Box>
        </>
      </ScrollView>

      {date && time && (
        <Box
          bg="bg"
          align="flex-end"
          width="100%"
          px={28}
          py={16}
          style={[styles.buttonWrapper, styles.borderTop]}
        >
          <Box align="flex-start">
            <Button onPress={onNext}>Confirm</Button>
          </Box>
        </Box>
      )}
    </Box>
  )
}

export default BookingDate
