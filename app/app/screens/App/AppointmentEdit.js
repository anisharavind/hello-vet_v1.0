import React, { useMemo, useState } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { mutate } from 'swr'
import groupBy from 'lodash/groupBy'
import { ScrollView } from 'react-native'
import { Toast } from 'react-native-root-toaster'

import styles from '../../styles'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { Box, Text, Header, Loader, Button } from '../../components'

const AppointmentEdit = ({ navigation, route }) => {
  const [date, setDate] = useState(moment().format('YYYY-MM-DD'))
  const [time, setTime] = useState(null)
  const appointment = get(route, 'params.appointment')

  const { data, isValidating } = useFetch('timeslots')

  const slots = useMemo(() => get(data, 'data') || [], [data])
  const dates = useMemo(() => {
    return groupBy(slots, (s) => moment(s.date).format('YYYY-MM-DD'))
  }, [slots])
  const times = useMemo(() => {
    let ts = dates[date] || []
    ts = ts.map((t) => t.date)

    return new Set(ts)
  }, [dates, date])

  const alltimes = useMemo(() => {
    if (!date) return []
    if (!Object.keys(dates).includes(date)) return []

    return [...Array(24 * 2).keys()].map((t) =>
      moment(date)
        .hour(0)
        .startOf('hour')
        .add(t * 30, 'minutes')
    )
  }, [dates, date])

  const getBg = (f) => {
    if (time === f) return 'primary300'
    if (times.has(f)) return 'success'

    return 'danger'
  }

  const chooseDate = (d) => {
    setDate(d)
    setTime(null)
  }

  const onUpdate = async () => {
    try {
      await request.patch(`appointments/${appointment.id}`, { date: time })

      mutate(`appointments/${appointment.id}`)
      Toast.show('Appointment updated')
      navigation.goBack()
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      Toast.show(message || error.message)
    }
  }

  const Date = ({ d }) => {
    const day = moment(d)

    return (
      <Box
        circle
        height={56}
        width={56}
        bg={date === d ? 'primary300' : 'primary100'}
        align="center"
        justify="center"
        mx={8}
        onPress={() => chooseDate(d)}
      >
        <Text type="p3">{day.format('ddd')}</Text>
        <Text type="h3">{day.format('DD')}</Text>
      </Box>
    )
  }
  const Time = ({ t }) => {
    const formatted = t.format('YYYY-MM-DD HH:mm:ss')

    return (
      <Box bg={getBg(formatted)} basis="33.33%" border="textAlt">
        <Box
          align="center"
          justify="center"
          height={48}
          onPress={() => times.has(formatted) && setTime(formatted)}
        >
          <Text color={time === formatted ? 'text' : 'textAlt'}>
            {t.format('hh:mm A')}
          </Text>
        </Box>
      </Box>
    )
  }

  if (!appointment) {
    return <Loader error />
  }

  return (
    <Box safe bg="bg" style={styles.relative}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Edit appointment" />
          <Box flex px={28} pb={100}>
            {!dates ? (
              <Loader loading={isValidating} />
            ) : (
              <Box flex>
                <Box row align="center" justify="space-between" mb={24}>
                  <Text type="h3" color="text">
                    Select date and time
                  </Text>
                </Box>
                <Box height={64} mb={28} mx={-28}>
                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <Box row align="center" px={20} pb={8}>
                      {Object.keys(dates).map((d) => (
                        <Date d={d} key={d} />
                      ))}
                    </Box>
                  </ScrollView>
                </Box>

                <Box row wrap align="center">
                  {alltimes.map((t) => (
                    <Time t={t} key={t.valueOf()} />
                  ))}
                </Box>
              </Box>
            )}
          </Box>
        </>
      </ScrollView>

      {date && time && (
        <Box
          bg="bg"
          align="flex-end"
          width="100%"
          px={28}
          py={16}
          style={[styles.buttonWrapper, styles.borderTop]}
        >
          <Box align="flex-start">
            <Button onPress={onUpdate}>Update</Button>
          </Box>
        </Box>
      )}
    </Box>
  )
}

export default AppointmentEdit
