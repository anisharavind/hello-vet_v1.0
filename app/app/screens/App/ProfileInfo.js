import React from 'react'
import { useSelector } from 'react-redux'
import { ScrollView, StyleSheet } from 'react-native'

import { Box, Header, Text, Image } from '../../components'
import { User, Help, Phone, Mail } from '../../components/Svg'

const styles = StyleSheet.create({
  scroll: {
    flexGrow: 1,
  },
})

const ProfileInfo = ({ navigation }) => {
  const { user } = useSelector((state) => state.user)

  const onEdit = () => navigation.navigate('Profile')

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header right={<Text color="danger">Edit</Text>} onRight={onEdit} />
          <Box flex px={28} py={28}>
            <Box flex>
              <Box align="flex-start">
                <Box
                  circle
                  bg="shade300"
                  align="center"
                  justify="center"
                  width={100}
                  height={100}
                >
                  {user.image ? (
                    <Image source={user.image} width={100} height={100} />
                  ) : (
                    <Text uppercase size={36} height={100} weight="medium">
                      {String(user.name).charAt(0)}
                    </Text>
                  )}
                </Box>
              </Box>
              <Box border="shade300" mt={56} mb={40} ml={-28} mr={-28} />
              <Box row py={16}>
                <User color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  {user.name}
                </Text>
              </Box>
              <Box row py={16}>
                <Phone color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  +91 {user.phone}
                </Text>
              </Box>
              {user.email && (
                <Box row py={16}>
                  <Mail color="primary" width={24} />
                  <Text type="h3" color="text" ml={20}>
                    {user.email}
                  </Text>
                </Box>
              )}
              <Box border="shade300" my={40} ml={-28} mr={-28} />
              <Box row py={16}>
                <Help color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Help
                </Text>
              </Box>
            </Box>
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default ProfileInfo
