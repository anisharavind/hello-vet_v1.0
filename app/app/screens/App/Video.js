import React from 'react'

import { Video as VideoView } from '../../components/Video'

const Video = () => <VideoView screen="Home" />

export default Video
