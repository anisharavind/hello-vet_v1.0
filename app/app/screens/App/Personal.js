import React from 'react'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { Box, Header } from '../../components'
import { PersonalForm } from '../../components/Form'

const Personal = () => {
  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header hideBack title="Registration" />
          <Box flex px={28} pb={28}>
            <PersonalForm />
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Personal
