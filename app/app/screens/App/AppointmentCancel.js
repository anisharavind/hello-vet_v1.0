import React, { useRef, useState } from 'react'
import get from 'lodash/get'
import { mutate } from 'swr'
import { ScrollView } from 'react-native'
import { Toast } from 'react-native-root-toaster'
import RBSheet from 'react-native-raw-bottom-sheet'

import styles from '../../styles'
import request from '../../libs/request'
import { normalize } from '../../libs/utils'
import { ArrowDown } from '../../components/Svg'
import { Box, Text, Loader, Button, Header, Select } from '../../components'

const reasons = [
  { id: 1, name: 'I am busy' },
  { id: 2, name: 'Forgot about the appointment' },
  { id: 3, name: 'Changed my mind' },
  { id: 4, name: 'Visited another veterinarian' },
]

const AppointmentCancel = ({ route, navigation }) => {
  const warning = useRef(null)
  const [loading, setLoading] = useState(false)
  const [reason, setReason] = useState(null)
  const appointment = get(route, 'params.appointment')

  const onSubmit = async () => {
    if (!reason) {
      return Toast.show('Please choose a reason')
    }

    setLoading(true)

    try {
      await request.post(`appointments/${appointment.id}/cancel`, {
        reason: reason.name,
      })
      Toast.show('Appointment cancelled')
      navigation.navigate('Home')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      Toast.show(message || error.message)
    }

    return setLoading(false)
  }

  const onDelete = () => warning.current && warning.current.open()
  const onCancel = () => warning.current && warning.current.close()

  if (!appointment) {
    return <Loader error />
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Cancel appointment" />
          <Box flex px={28} pb={28}>
            <Box row align="center" justify="space-between" mb={24}>
              <Text type="h3" color="text">
                Choose reason
              </Text>
            </Box>
            <Select
              data={reasons}
              selected={reason}
              onSelect={(item) => setReason(item)}
              right={<ArrowDown color="shade500" width={20} />}
            />
            <Box align="flex-start" mt={44}>
              <Button loading={loading} onPress={onDelete}>
                Cancel & request refund
              </Button>
            </Box>
          </Box>
        </>
      </ScrollView>

      <RBSheet ref={warning} height={normalize(180)}>
        <Box px={28} py={28}>
          <Text type="h3" color="text" weight="bold" align="center" mb={12}>
            Are you sure you want to cancel the appointment?
          </Text>
          <Box row align="center" pt={40}>
            <Box flex mr={16}>
              <Button ghost type="danger" loading={loading} onPress={onSubmit}>
                CONFIRM
              </Button>
            </Box>
            <Box height="60%" style={styles.borderRight} />
            <Box flex ml={16}>
              <Button ghost onPress={onCancel}>
                CANCEL
              </Button>
            </Box>
          </Box>
        </Box>
      </RBSheet>
    </Box>
  )
}

export default AppointmentCancel
