import React from 'react'
import { useSelector } from 'react-redux'

import { Box, Loader } from '../../components'
import { Booking, Appointments } from '../../components/Home'

const Home = () => {
  const { home } = useSelector((state) => state.user)

  if (!home)
    return (
      <Box flex safe bg="bg">
        <Loader />
      </Box>
    )

  return (
    <Box flex safe bg="bg">
      {home === 'booking' && <Booking />}
      {home === 'appointments' && <Appointments />}
    </Box>
  )
}

export default Home
