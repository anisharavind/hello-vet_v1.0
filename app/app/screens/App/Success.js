import React from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { ScrollView, Image } from 'react-native'

import styles from '../../styles'
import { images } from '../../config'
import { Box, Button, Text } from '../../components'

const Success = ({ navigation, route }) => {
  const pet = get(route, 'params.pet')
  const redirect = get(route, 'params.redirect')
  const appointment = get(route, 'params.appointment')

  const onNext = () => {
    navigation.reset({
      index: 0,
      routes: [{ name: redirect || 'Home', params: { id: pet?.id } }],
    })
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Box flex align="center" justify="center" px={28} py={28}>
          <Box align="center">
            <Image
              source={images.success}
              style={{ width: 96, height: 96, marginBottom: 24 }}
            />
            {appointment && (
              <>
                <Text type="h4" height={24} mb={16} align="center">
                  {`Your appointment is confirmed on \n`}
                  <Text type="h4" height={24} weight="bold">
                    {moment(appointment.date).format('DD-MM-YYYY')}
                  </Text>{' '}
                  at{' '}
                  <Text type="h4" height={24} weight="bold">
                    {moment(appointment.date).format('hh:mm A')}
                  </Text>
                </Text>
                <Text type="h4" height={24} mb={16} align="center">
                  Please be ready and logged in. You will receive video call from
                  Felican at the confirmed time.
                </Text>
              </>
            )}
          </Box>
        </Box>
      </ScrollView>
      <Box align="flex-end" mt={16} px={28} mb={28}>
        <Button type="primary" onPress={onNext}>
          Confirm
        </Button>
      </Box>
    </Box>
  )
}

export default Success
