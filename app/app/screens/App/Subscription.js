import React, { useMemo } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { ScrollView } from 'react-native'
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'

import styles from '../../styles'
import { colors } from '../../config'
import useFetch from '../../libs/useFetch'
import { Box, Loader, Text, Button } from '../../components'

const Subscription = ({ route, navigation }) => {
  const { id } = route.params
  const { data, isValidating } = useFetch(`pets/${id}`)

  const pet = useMemo(() => get(data, 'data') || null, [data])
  const days = useMemo(() => {
    if (!pet) return 0

    const diff = moment(pet.subscription_ends_at).diff(moment(), 'days')

    return diff > 0 ? diff : 0
  }, [pet])

  const onConfirm = () =>
    navigation.reset({
      index: 0,
      routes: [{ name: 'Home' }],
    })

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Box flex px={28} pb={28}>
          {!pet ? (
            <Loader loading={isValidating} />
          ) : (
            <Box flex align="center" justify="center">
              <CountdownCircleTimer
                isPlaying={false}
                // size={44}
                duration={14}
                initialRemainingTime={days}
                strokeWidth={8}
                colors={[[colors.primary800]]}
                trailColor={colors.shade300}
                onComplete={() => {}}
              >
                {({ remainingTime }) => (
                  <Text type="h1" style={{ fontSize: 48, lineHeight: 64 }}>
                    {remainingTime}
                  </Text>
                )}
              </CountdownCircleTimer>
              <Text type="h3" align="center" mt={24} mx={28}>
                You’ve {days} more days remaining in your free consultation
              </Text>
            </Box>
          )}
        </Box>
      </ScrollView>
      <Box align="flex-end" mt={16} px={28} mb={28}>
        <Button type="primary" onPress={onConfirm}>
          Confirm
        </Button>
      </Box>
    </Box>
  )
}

export default Subscription
