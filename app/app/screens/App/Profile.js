import React, { useRef, useState, useMemo } from 'react'
import { cache } from 'swr'
import get from 'lodash/get'
import { useDispatch } from 'react-redux'
import { Toast } from 'react-native-root-toaster'
import RBSheet from 'react-native-raw-bottom-sheet'
import ImagePicker from 'react-native-image-crop-picker'
import { ScrollView, ActivityIndicator } from 'react-native'

import { colors } from '../../config'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { normalize } from '../../libs/utils'
import styles, { sheet } from '../../styles'
import { setUser, logOut } from '../../store/user'
import { ProfileForm } from '../../components/Form'
import { Image as ImageIcon, Camera } from '../../components/Svg'
import { Box, Text, Loader, Button, Image, Header } from '../../components'

const options = {
  width: 360,
  height: 360,
  cropping: true,
}

const Profile = () => {
  const menu = useRef(null)
  const warning = useRef(null)
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [deleting, setDeleting] = useState(false)
  const { data, isValidating, mutate } = useFetch('user')

  const user = useMemo(() => get(data, 'data') || null)

  const uploadImage = async (image) => {
    setLoading(true)
    const values = new FormData()

    values.append('image', {
      uri: image.path,
      type: image.mime,
      name: 'image.jpg',
    })

    try {
      let userData = await request.post('user/image', values)
      userData = get(userData, 'data.data')

      mutate()
      dispatch(setUser(userData))
      Toast.show('Image Uploaded')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      Toast.show(message || error.message)
    }
    setLoading(false)
  }

  const deleteProfile = async () => {
    setDeleting(true)

    try {
      await request.delete('user')
      cache.clear()
      dispatch(logOut())
      Toast.show('Profile deleted')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      setDeleting(false)
      Toast.show(message || error.message)
    }
  }

  const onGallery = () => {
    if (menu.current) menu.current.close()

    ImagePicker.openPicker(options)
      .then((image) => {
        uploadImage(image)
      })
      .catch(() => {})
  }

  const onCamera = () => {
    if (menu.current) menu.current.close()

    ImagePicker.openCamera(options)
      .then((image) => {
        uploadImage(image)
      })
      .catch(() => {})
  }

  const onPicture = () => menu.current && menu.current.open()
  const onDelete = () => warning.current && warning.current.open()
  const onCancel = () => warning.current && warning.current.close()

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header />
          <Box flex px={28} pb={28}>
            {!user ? (
              <Loader loading={isValidating} mutate={mutate} />
            ) : (
              <Box flex pt={56}>
                <Box align="flex-start">
                  <Box
                    circle
                    bg="shade300"
                    align="center"
                    justify="center"
                    width={100}
                    height={100}
                    onPress={onPicture}
                  >
                    {user.image && (
                      <Image source={user.image} width={100} height={100} />
                    )}
                    <Box
                      circle
                      align="center"
                      justify="center"
                      height={44}
                      width={44}
                      bg="primary"
                      style={styles.absolute}
                    >
                      {loading ? (
                        <ActivityIndicator color={colors.textAlt} />
                      ) : (
                        <Camera width={24} height={24} color="textAlt" />
                      )}
                    </Box>
                  </Box>
                </Box>
                <Box border="shade300" mt={56} mb={56} ml={-28} mr={-28} />
                <ProfileForm user={user} />
                <Box border="shade300" mt={56} mb={40} ml={-28} mr={-28} />
                <Box align="flex-start">
                  <Button ghost type="danger" onPress={onDelete}>
                    Delete profile
                  </Button>
                </Box>
              </Box>
            )}
          </Box>
        </>
      </ScrollView>

      <RBSheet ref={menu} height={normalize(180)}>
        <Box px={28} py={28}>
          <Box row py={16} onPress={onCamera}>
            <Camera color="primary" width={24} />
            <Text type="h3" color="text" ml={20}>
              Take photo
            </Text>
          </Box>
          <Box row py={16} onPress={onGallery}>
            <ImageIcon color="primary" width={24} />
            <Text type="h3" color="text" ml={20}>
              Choose image
            </Text>
          </Box>
        </Box>
      </RBSheet>
      <RBSheet ref={warning} customStyles={sheet}>
        <Box px={28} py={28}>
          <Text type="h3" color="text" weight="bold" align="center" mb={12}>
            Are you sure?
          </Text>
          <Text color="text" height={22} align="center">
            {`You are going to delete everything, including pets, appointments, your profile... everything. \nThis cannot be undone, so be very sure.`}
          </Text>
          <Box row align="center" pt={40}>
            <Box flex mr={16}>
              <Button
                ghost
                type="danger"
                loading={deleting}
                onPress={deleteProfile}
              >
                YES, DELETE
              </Button>
            </Box>
            <Box height="60%" style={styles.borderRight} />
            <Box flex ml={16}>
              <Button ghost onPress={onCancel}>
                CANCEL
              </Button>
            </Box>
          </Box>
        </Box>
      </RBSheet>
    </Box>
  )
}

export default Profile
