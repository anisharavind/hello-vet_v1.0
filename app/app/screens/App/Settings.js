import React, { useCallback, useState } from 'react'
import { cache } from 'swr'
import { useDispatch, useSelector } from 'react-redux'
import { ScrollView, ActivityIndicator } from 'react-native'

import styles from '../../styles'
import { colors } from '../../config'
import request from '../../libs/request'
import { logOut } from '../../store/user'
import { Box, Header, Text, Image } from '../../components'
import { Logout, User, Heart, Help } from '../../components/Svg'

const Settings = ({ navigation }) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const { user } = useSelector((state) => state.user)

  const onPets = () => navigation.navigate('Pets')
  const onProfile = () => navigation.navigate('ProfileInfo')

  const onLogout = useCallback(async () => {
    setLoading(true)

    try {
      await request.post('logout')
    } catch (error) {
      //
    }

    cache.clear()
    dispatch(logOut())
  }, [])

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header />
          <Box flex px={28} py={28}>
            <Box flex>
              <Box align="flex-start">
                <Box
                  circle
                  bg="shade300"
                  align="center"
                  justify="center"
                  width={100}
                  height={100}
                  mb={28}
                >
                  {user.image ? (
                    <Image source={user.image} width={100} height={100} />
                  ) : (
                    <Text uppercase size={36} height={100} weight="medium">
                      {String(user.name).charAt(0)}
                    </Text>
                  )}
                </Box>
                <Text type="h2">{user.name}</Text>
              </Box>
              <Box border="shade300" mt={56} mb={40} ml={-28} mr={-28} />
              <Box row py={16} onPress={onProfile}>
                <User color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Registered profile
                </Text>
              </Box>
              <Box row py={16} onPress={onPets}>
                <Heart color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Pet profiles
                </Text>
              </Box>
              <Box row py={16} onPress={onLogout}>
                <Box align="center" justify="center" width={24}>
                  {loading ? (
                    <ActivityIndicator color={colors.primary} />
                  ) : (
                    <Logout color="primary" width={24} />
                  )}
                </Box>
                <Text type="h3" color="text" ml={20}>
                  Log out
                </Text>
              </Box>
              <Box border="shade300" my={40} ml={-28} mr={-28} />
              <Box row py={16}>
                <Help color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Help
                </Text>
              </Box>
            </Box>
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Settings
