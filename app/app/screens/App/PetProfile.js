import React from 'react'
import get from 'lodash/get'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { Box, Header } from '../../components'
import { PetCreateForm } from '../../components/Form'

const PetProfile = ({ route }) => {
  const redirect = get(route, 'params.redirect')

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Create your pet profile" />
          <Box flex px={28} pb={28}>
            <PetCreateForm redirect={redirect} />
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default PetProfile
