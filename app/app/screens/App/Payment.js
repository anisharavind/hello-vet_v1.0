import React, { useEffect, useState, useMemo } from 'react'
import get from 'lodash/get'
import { mutate } from 'swr'
import { useDispatch } from 'react-redux'
import { WebView } from 'react-native-webview'
import { Toast } from 'react-native-root-toaster'

import styles from '../../styles'
import config from '../../config'
import { reset } from '../../store/booking'
import { Box, Loader, Header } from '../../components'

const INJECTED_JAVASCRIPT = `(function() {
  if(window.location.href.startsWith('${config.SUCCESS_URL}')) {
    window.ReactNativeWebView.postMessage("SUCCESS");
  }
})();`

const Payment = ({ route, navigation }) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(true)
  const appointment = get(route, 'params.appointment') || null

  const uri = useMemo(
    () => `${config.PAYMENT_URL}/${appointment.id}?token=${appointment.token}`,
    [appointment]
  )

  useEffect(() => {
    dispatch(reset())
  }, [])

  const onBack = () => navigation.navigate('Home')
  const onError = () => Toast.show('Something went wrong')

  const onMessage = async ({ nativeEvent: { data } }) => {
    if (!data) {
      return
    }

    if (data === 'SUCCESS') {
      mutate('appointments')
      navigation.reset({
        index: 0,
        routes: [
          { name: 'Success', params: { appointment, redirect: 'Home' } },
        ],
      })
    }
  }

  if (!appointment) {
    return <Loader error />
  }

  return (
    <Box safe bg="bg">
      <Header onBack={onBack} />
      <Box flex style={styles.relative}>
        {loading && (
          <Box bg="textAlt" style={styles.overlay}>
            <Loader loading />
          </Box>
        )}
        <WebView
          androidHardwareAccelerationDisabled
          onError={onError}
          onMessage={onMessage}
          source={{ uri }}
          onLoad={() => setLoading(false)}
          injectedJavaScript={INJECTED_JAVASCRIPT}
        />
      </Box>
    </Box>
  )
}

export default Payment
