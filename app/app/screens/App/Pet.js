import React, { useRef, useMemo, useState } from 'react'
import get from 'lodash/get'
import { mutate } from 'swr'
import { ScrollView } from 'react-native'
import { Toast } from 'react-native-root-toaster'
import RBSheet from 'react-native-raw-bottom-sheet'

import { sex } from '../../config'
import request from '../../libs/request'
import { getAge } from '../../libs/utils'
import useFetch from '../../libs/useFetch'
import styles, { sheet } from '../../styles'
import { PetUpdateForm } from '../../components/Form'
import { Box, Header, Loader, Text, Button } from '../../components'

const Personal = ({ route, navigation }) => {
  const { id } = route.params

  const warning = useRef(null)
  const [deleting, setDeleting] = useState(null)

  const { data, isValidating } = useFetch(`pets/${id}`)

  const pet = useMemo(() => {
    const petData = get(data, 'data') || null

    if (!petData) return null

    const d = { ...petData }
    d.sex = sex.find((s) => s.value === d.sex) || sex[0]

    if (d.age) {
      const age = getAge(d.age)
      d.years = String(age.years)
      d.months = String(age.months)
    }

    return d
  }, [data])

  const onDelete = () => warning.current && warning.current.open()
  const onCancel = () => warning.current && warning.current.close()
  const deleteProfile = async () => {
    setDeleting(true)

    try {
      await request.delete(`pets/${pet.id}`)
      mutate('pets')
      Toast.show('Pet Profile deleted')
      navigation.navigate('Pets')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      setDeleting(false)
      Toast.show(message || error.message)
    }
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header title="Edit your pet profile" />
          <Box flex px={28} pb={28}>
            {!pet ? (
              <Loader loading={isValidating} />
            ) : (
              <>
                <PetUpdateForm pet={pet} />
                <Box align="flex-start" mt={24}>
                  <Button ghost type="danger" onPress={onDelete}>
                    Delete pet profile
                  </Button>
                </Box>
              </>
            )}
          </Box>

          <RBSheet ref={warning} animationType="none" customStyles={sheet}>
            <Box px={28} py={28}>
              <Text type="h3" color="text" weight="bold" align="center" mb={12}>
                Are you sure?
              </Text>
              <Text color="text" height={22} align="center">
                {`You are going to delete everything related to your pet, including pet profile, appointments, prescriptions... \nThis cannot be undone, so be very sure.`}
              </Text>
              <Box row align="center" pt={40}>
                <Box flex mr={16}>
                  <Button
                    ghost
                    type="danger"
                    loading={deleting}
                    onPress={deleteProfile}
                  >
                    YES, DELETE
                  </Button>
                </Box>
                <Box height="60%" style={styles.borderRight} />
                <Box flex ml={16}>
                  <Button ghost onPress={onCancel}>
                    CANCEL
                  </Button>
                </Box>
              </Box>
            </Box>
          </RBSheet>
        </>
      </ScrollView>
    </Box>
  )
}

export default Personal
