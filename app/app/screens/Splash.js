import React from 'react'
import { StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'

import { images } from '../config'
import { Box } from '../components'

const styles = StyleSheet.create({
  logo: {
    height: 200,
    width: 200,
  },
})

const SplashScreen = () => (
  <Box flex bg="primary" align="center" justify="center" style={styles.box}>
    <FastImage source={images.logo} style={styles.logo} />
  </Box>
)

export default SplashScreen
