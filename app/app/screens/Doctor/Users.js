import React, { useMemo, useState } from 'react'
import get from 'lodash/get'
import { useDispatch } from 'react-redux'
import { Toast } from 'react-native-root-toaster'
import { FlatList, ActivityIndicator } from 'react-native'

import styles from '../../styles'
import { colors } from '../../config'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { Call } from '../../components/Svg'
import { setVideo } from '../../store/notifications'
import { Box, Header, Loader, Text } from '../../components'

const Users = ({ navigation }) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const { data, isValidating } = useFetch('doctor/users')

  const users = useMemo(() => get(data, 'data'), [data])

  const onCall = async (item) => {
    setLoading(true)

    try {
      const resp = await request.get(`doctor/users/${item.id}`)
      const channel = get(resp, 'data.channel') || null

      if (channel) {
        dispatch(setVideo({ channel }))
        navigation.navigate('Video')
      }
    } catch (error) {
      Toast.show('Unable to call right now')
    }

    setLoading(false)
  }

  const ListHeader = () => (
    <Box flex px={28} pt={28}>
      <Header title="Users" />
    </Box>
  )

  const renderItem = ({ item }) => (
    <Box px={28} mb={28}>
      <Box
        row
        align="center"
        justify="space-between"
        onPress={() => onCall(item)}
      >
        <Box>
          <Text color="text" weight="medium">
            {item.name}
          </Text>
        </Box>
        <Box
          circle
          bg="primary100"
          align="center"
          justify="center"
          height={44}
          width={44}
          mr={16}
        >
          {loading ? <ActivityIndicator color={colors.primary} /> : <Call />}
        </Box>
      </Box>
    </Box>
  )

  return (
    <Box safe bg="bg">
      <FlatList
        data={users}
        renderItem={renderItem}
        ListEmptyComponent={<Loader loading={isValidating} />}
        keyExtractor={(item) => String(item.id)}
        ListHeaderComponent={<ListHeader />}
        contentContainerStyle={styles.grow}
      />
    </Box>
  )
}

export default Users
