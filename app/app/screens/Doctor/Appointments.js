import React from 'react'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { Menu } from '../../components/Svg'
import { Box, Header, Text } from '../../components'

const Appointments = ({ navigation }) => {
  const onDrawer = () => navigation.openDrawer()
  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header hideBack right={<Menu />} onRight={onDrawer} />
        <Box flex px={28} py={28}>
          <Box row align="center" justify="space-between" mb={24}>
            <Text type="h3" color="text">
              Overview
            </Text>
          </Box>
        </Box>
      </ScrollView>
    </Box>
  )
}

export default Appointments
