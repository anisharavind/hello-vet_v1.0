import React, { useState, useMemo, useEffect } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { ScrollView } from 'react-native'
import { Toast } from 'react-native-root-toaster'

import styles from '../../styles'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { Box, Header, Text, Select, Button } from '../../components'
import { Menu, ArrowDown, ArrowLeft, ArrowRight } from '../../components/Svg'

const options = [...Array(24).keys()].map((t) => ({
  id: t,
  name: moment().hour(t).startOf('hour').format('hh:mm A'),
  value: moment().hour(t).startOf('hour'),
}))

const Timeslots = ({ navigation }) => {
  const [end, setEnd] = useState(null)
  const [start, setStart] = useState(null)
  const [date, setDate] = useState(moment())
  const [slots, setSlots] = useState(new Set())
  const [loading, setLoading] = useState(false)
  const { data, mutate } = useFetch(
    `doctor/time-slots?date=${date.format('YYYY-MM-DD')}`
  )

  useEffect(() => {
    if (data) {
      let timeSlots = get(data, 'data') || []
      timeSlots = timeSlots.map((ts) => ts.date)
      const set = new Set(timeSlots)

      setSlots(set)
    }
  }, [data])

  const times = useMemo(() => {
    if (!start || !end) return []

    const diff = end.value.diff(start.value, 'hours')

    if (diff <= 0) return []

    const count = diff * 2

    return [...Array(count).keys()].map((t) =>
      date
        .clone()
        .hour(start.value.hour())
        .startOf('hour')
        .add(t * 30, 'minutes')
    )
  }, [date, start, end])

  const onDrawer = () => navigation.openDrawer()
  const onNext = () => setDate((d) => d.clone().add(1, 'day'))

  const onPrev = () =>
    setDate((d) => {
      const prev = d.clone().subtract(1, 'day')

      return !prev.isBefore(moment(), 'day') ? prev : d
    })

  const onSelect = (t) => {
    const set = new Set(slots)
    const formated = t.format('YYYY-MM-DD HH:mm:ss')

    if (set.has(formated)) {
      set.delete(formated)
    } else {
      set.add(formated)
    }

    setSlots(set)
  }

  const onSubmit = async () => {
    setLoading(true)

    try {
      await request.post('doctor/time-slots', {
        date: date.format('YYYY-MM-DD'),
        time_slots: [...slots],
      })
      mutate()
      Toast.show('Time slots updated')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      Toast.show(message || error.message)
    }

    setLoading(false)
  }

  const Time = ({ t }) => {
    return (
      <Box
        bg={
          slots.has(t.format('YYYY-MM-DD HH:mm:ss'))
            ? 'primary300'
            : 'primary100'
        }
        basis="33.33%"
        border="textAlt"
      >
        <Box
          align="center"
          justify="center"
          height={48}
          onPress={() => onSelect(t)}
        >
          <Text color="text">{t.format('hh:mm A')}</Text>
        </Box>
      </Box>
    )
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <>
          <Header right={<Menu />} onRight={onDrawer} />
          <Box flex px={28} py={28}>
            <Text type="h3" weight="bold" color="text" mb={28}>
              Manage time slots
            </Text>
            <Text type="h3" mb={24}>
              Set consultation start & end time
            </Text>
            <Box row justify="space-between" mb={28}>
              <Box basis="40%">
                <Select
                  label="STARTS AT"
                  data={options}
                  selected={start}
                  onSelect={(item) => setStart(item)}
                  right={<ArrowDown color="shade500" width={20} />}
                />
              </Box>
              <Box basis="40%">
                <Select
                  label="ENDS AT"
                  data={options}
                  selected={end}
                  onSelect={(item) => setEnd(item)}
                  right={<ArrowDown color="shade500" width={20} />}
                />
              </Box>
            </Box>
            {start && end && (
              <>
                <Text type="h3" mb={28}>
                  Choose available slots
                </Text>
                <Box row align="center" justify="space-between" mb={28}>
                  <Text type="h4">{date.format('ddd, DD MMM YYYY')}</Text>
                  <Box row>
                    <Box onPress={onPrev} mr={16}>
                      <ArrowLeft color="text700" />
                    </Box>
                    <Box onPress={onNext}>
                      <ArrowRight color="text700" />
                    </Box>
                  </Box>
                </Box>
                <Box row wrap align="center">
                  {times.map((t) => (
                    <Time t={t} key={t.valueOf()} />
                  ))}
                </Box>
                <Box align="flex-start" mt={28}>
                  <Button
                    onPress={onSubmit}
                    loading={loading}
                    disabled={loading}
                  >
                    Save changes
                  </Button>
                </Box>
              </>
            )}
          </Box>
        </>
      </ScrollView>
    </Box>
  )
}

export default Timeslots
