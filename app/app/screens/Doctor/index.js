import Video from './Video'
import History from './History'
import Calendar from './Calendar'
import Dashboard from './Dashboard'
import Diagnosis from './Diagnosis'
import Timeslots from './Timeslots'
import Appointment from './Appointment'
import Appointments from './Appointments'
import Prescription from './Prescription'

export {
  Video,
  History,
  Calendar,
  Dashboard,
  Diagnosis,
  Timeslots,
  Appointment,
  Appointments,
  Prescription,
}
