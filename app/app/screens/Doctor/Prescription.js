import React from 'react'
import get from 'lodash/get'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { Menu } from '../../components/Svg'
import { PrescriptionForm } from '../../components/Form'
import { Box, Header, Text, Loader, PetTag } from '../../components'

const Prescription = ({ route, navigation }) => {
  const appointment = get(route, 'params.appointment')

  const onDrawer = () => navigation.openDrawer()

  if (!appointment) {
    return <Loader />
  }

  return (
    <Box safe bg="bg" pb={28}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header right={<Menu />} onRight={onDrawer} />
        <Box flex px={28} py={28}>
          <Text type="h3" weight="bold" color="text" mb={12}>
            Add prescription
          </Text>
          <PetTag item={appointment.pet} pb={28} />
          <PrescriptionForm appointment={appointment} />
        </Box>
      </ScrollView>
    </Box>
  )
}

export default Prescription
