import React, { useRef, useState, useMemo } from 'react'
import get from 'lodash/get'
import { useDispatch } from 'react-redux'
import { ScrollView, View } from 'react-native'
import { Toast } from 'react-native-root-toaster'
import ImageView from 'react-native-image-viewing'
import RBSheet from 'react-native-raw-bottom-sheet'

import styles from '../../styles'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { Menu } from '../../components/Svg'
import { normalize } from '../../libs/utils'
import { setVideo } from '../../store/notifications'
import {
  Box,
  Header,
  Text,
  Loader,
  Button,
  PetTag,
  Image,
} from '../../components'

const Appointment = ({ route, navigation }) => {
  const menu = useRef(null)
  const dispatch = useDispatch()
  const id = get(route, 'params.id')
  const [loading, setLoading] = useState(false)
  const [visible, setVisible] = useState(false)
  const { data, isValidating } = useFetch(`doctor/appointments/${id}`)

  const appointment = useMemo(() => get(data, 'data') || null, [data])

  const onDrawer = () => navigation.openDrawer()
  const onCall = async () => {
    setLoading(true)

    try {
      const resp = await request.get(`doctor/users/${appointment.user.id}`)
      const channel = get(resp, 'data.channel') || null

      if (channel) {
        dispatch(setVideo({ channel }))
        navigation.navigate('Video')
      }
    } catch (error) {
      Toast.show('Unable to call right now')
    }

    setLoading(false)
  }

  const onAction = () => menu.current && menu.current.open()
  const onClose = () => menu.current && menu.current.close()
  const onDiagnosis = () => {
    onClose()
    navigation.navigate('Diagnosis', { appointment })
  }
  const onPrescription = () => {
    onClose()
    navigation.navigate('Prescription', { appointment })
  }
  const onHistory = () => {
    onClose()
    navigation.navigate('History', { id: appointment.pet.id })
  }

  if (!appointment) {
    return <Loader loading={isValidating} />
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header right={<Menu />} onRight={onDrawer} />
        <Box flex px={28} py={28}>
          <PetTag
            item={appointment.pet}
            textSize="h3"
            borderBottom="shade300"
            pb={28}
            mb={20}
          />
          <Box mb={20}>
            <Text type="h3" mb={4}>{appointment.user?.name}</Text>
            <Text type="h3">{appointment.user?.phone}</Text>
          </Box>
          {appointment.questions.map((question) =>
            question.question && (question.answer || question.file) ? (
              <Box key={question.id} mb={24}>
                <Text mb={12}>Q: {question.question.title}</Text>
                {!!question.answer && (
                  <Text color="text" weight="medium">
                    A: {question.answer}
                  </Text>
                )}
                {question.file && (
                  <>
                    <Box onPress={() => setVisible(true)}>
                      <Image source={question.file} width={100} height={100} />
                    </Box>
                    <ImageView
                      images={[{ uri: question.file }]}
                      imageIndex={0}
                      visible={visible}
                      onRequestClose={() => setVisible(false)}
                    />
                  </>
                )}
              </Box>
            ) : null
          )}
          {appointment.diagnosis && (
            <Box round bg="shade100" mx={-12} px={12} pt={16} mb={16}>
              <Text type="h4" color="text" weight="medium" mb={12}>
                Diagnosis
              </Text>
              <Text mb={24}>{appointment.diagnosis.content}</Text>
            </Box>
          )}
          {appointment.prescriptions.length > 0 && (
            <Box round bg="shade100" mx={-12} px={12} pt={16}>
              <Text type="h4" color="text" weight="medium" mb={12}>
                Prescriptions
              </Text>
              <Box>
                <Box
                  row
                  justify="space-between"
                  borderBottom="shade300"
                  mb={16}
                >
                  <Box basis="33%">
                    <Text mb={12}>Medicine</Text>
                  </Box>
                  <Box basis="33%">
                    <Text mb={12}>Usage</Text>
                  </Box>
                  <Box basis="33%">
                    <Text mb={12}>Directions</Text>
                  </Box>
                </Box>
                {appointment.prescriptions.map((prescription) => (
                  <Box key={prescription.id} borderBottom="shade300" mb={16}>
                    <Box row>
                      <Text
                        color="text"
                        weight="medium"
                        mb={20}
                        style={{ flex: 1, flexWrap: 'wrap' }}
                      >
                        {prescription.item}
                      </Text>
                      <Text
                        color="text"
                        weight="medium"
                        mb={20}
                        style={{ flex: 1, flexWrap: 'wrap' }}
                      >
                        {prescription.usage}
                      </Text>
                      <Text
                        color="text"
                        weight="medium"
                        mb={20}
                        style={{ flex: 1, flexWrap: 'wrap' }}
                      >
                        {prescription.directions}
                      </Text>
                    </Box>
                  </Box>
                ))}
              </Box>
            </Box>
          )}
        </Box>
      </ScrollView>
      <View>
        <Box
          row
          justify="space-between"
          px={28}
          py={16}
          style={styles.borderTop}
        >
          <Button loading={loading} onPress={onCall}>
            Call now
          </Button>
          <Button onPress={onAction}>Actions</Button>
        </Box>
      </View>

      <RBSheet ref={menu} height={normalize(192)}>
        <Box px={28} py={12}>
          <Box row py={12} onPress={onDiagnosis}>
            <Text type="h3" color="text">
              Add diagnosis
            </Text>
          </Box>
          <Box row py={12} onPress={onPrescription}>
            <Text type="h3" color="text">
              Add prescription
            </Text>
          </Box>
          <Box row py={12} onPress={onHistory}>
            <Text type="h3" color="text">
              Pet history
            </Text>
          </Box>
        </Box>
      </RBSheet>
    </Box>
  )
}

export default Appointment
