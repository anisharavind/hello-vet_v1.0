import React, { useState } from 'react'
import moment from 'moment'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import { Day, Month } from '../../components/Calendar'
import { Menu, ArrowDown } from '../../components/Svg'
import { Box, Header, Text, Select } from '../../components'

const options = [
  { id: 1, name: 'Day' },
  { id: 3, name: 'Month' },
]

const Calendar = ({ navigation }) => {
  const [date, setDate] = useState(moment())
  const [view, setView] = useState(options[0])

  const onDrawer = () => navigation.openDrawer()

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header right={<Menu />} onRight={onDrawer} />
        <Box flex px={28} py={28}>
          <Box row align="center" justify="space-between">
            <Text type="h3" weight="bold" color="text">
              Calendar
            </Text>
            <Box width={140}>
              <Select
                small
                data={options}
                selected={view}
                onSelect={(item) => setView(item)}
                right={<ArrowDown color="shade500" width={20} />}
              />
            </Box>
          </Box>
          {view.name === 'Day' && <Day date={date} setDate={setDate} />}
          {view.name === 'Month' && (
            <Month date={date} setDate={setDate} setView={setView} />
          )}
        </Box>
      </ScrollView>
    </Box>
  )
}

export default Calendar
