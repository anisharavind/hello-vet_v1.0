import React, { useMemo, useCallback } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { ScrollView } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Menu } from '../../components/Svg'
import { numberFormat, hhmmss } from '../../libs/utils'
import { Box, Header, Text, Loader, PetTag } from '../../components'

const Dashboard = ({ navigation }) => {
  const { data, isValidating, mutate } = useFetch(`doctor/dashboard`)

  useFocusEffect(
    useCallback(() => {
      mutate()
    }, [mutate])
  )

  const dashboard = useMemo(() => get(data, 'appointments') || null, [data])

  const onDrawer = () => navigation.openDrawer()
  const onCalendar = () => navigation.navigate('Calendar')
  const onAppointment = (item) =>
    navigation.navigate('Appointment', { id: item.id })

  const Percentage = ({ current, previous }) => {
    const percentage = parseFloat(
      previous ? ((current - previous) / previous) * 100 : 100
    ).toFixed()

    const getColor = (p) => {
      if (p < 0) return 'danger'
      if (p > 0) return 'success'

      return 'info'
    }

    return (
      <Text type="p2" color={getColor(percentage)}>
        {percentage > 0 && '+'}
        {percentage}%
      </Text>
    )
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header hideBack right={<Menu />} onRight={onDrawer} />
        {!dashboard ? (
          <Loader loading={isValidating} />
        ) : (
          <Box flex px={28} py={28}>
            <Text type="h3" weight="bold" color="text" mb={28}>
              Overview
            </Text>
            <Box row wrap mx={-6} mb={24}>
              <Box basis="50%" px={6} mb={12}>
                <Box round bg="primary200" px={12} py={12} onPress={onCalendar}>
                  <Box row justify="space-between">
                    <Text type="h1" color="text">
                      {dashboard.day.count}
                    </Text>
                    <Percentage
                      current={dashboard.day.count}
                      previous={dashboard.previous.count}
                    />
                  </Box>
                  <Text type="p2">{`New appointments \ntoday`}</Text>
                </Box>
              </Box>
              <Box basis="50%" px={6} mb={12}>
                <Box round bg="danger300" px={12} py={12}>
                  <Box row justify="space-between">
                    <Text type="h1" color="text">
                      {dashboard.all.count}
                    </Text>
                  </Box>
                  <Text type="p2">{`Appointments \ntill date`}</Text>
                </Box>
              </Box>
              <Box basis="50%" px={6} mb={12}>
                <Box round bg="success300" px={12} py={12}>
                  <Box row justify="space-between">
                    <Text type="h1" color="text">
                      {numberFormat(dashboard.day.revenue)}
                    </Text>
                    <Percentage
                      current={dashboard.day.revenue}
                      previous={dashboard.previous.revenue}
                    />
                  </Box>
                  <Text type="p2">{`In revenue \ntoday`}</Text>
                </Box>
              </Box>
              <Box basis="50%" px={6} mb={12}>
                <Box round bg="warning300" px={12} py={12}>
                  <Box row justify="space-between">
                    <Text type="h1" color="text">
                      {numberFormat(dashboard.all.revenue)}
                    </Text>
                  </Box>
                  <Text type="p2">{`In revenue \ntill date`}</Text>
                </Box>
              </Box>
            </Box>
            <Box row justify="space-between" align="flex-end" mb={28}>
              <Text type="h3" weight="bold" color="text">
                Next appointment
              </Text>
              <Text color="primary" onPress={onCalendar}>
                View calendar
              </Text>
            </Box>
            {dashboard.next ? (
              <Box
                row
                round
                align="center"
                justify="space-between"
                bg="danger500"
                px={16}
                py={20}
              >
                <Box style={styles.relative}>
                  <CountdownCircleTimer
                    isPlaying
                    size={44}
                    duration={moment(dashboard.next.date).diff(
                      moment(),
                      'seconds'
                    )}
                    strokeWidth={0}
                    colors={[['#fff']]}
                  >
                    {({ remainingTime }) => (
                      <Text type="h3" color="textAlt" style={styles.countdown}>
                        In {hhmmss(remainingTime)}
                      </Text>
                    )}
                  </CountdownCircleTimer>
                  <PetTag item={dashboard.next.pet} textColor="textAlt" />
                </Box>
                <Box
                  round
                  bg="danger500"
                  px={16}
                  py={8}
                  onPress={() => onAppointment(dashboard.next)}
                >
                  <Text color="textAlt">View case</Text>
                </Box>
              </Box>
            ) : (
              <Text>No upcoming appointments</Text>
            )}
          </Box>
        )}
      </ScrollView>
    </Box>
  )
}

export default Dashboard
