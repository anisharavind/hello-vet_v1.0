import React, { useMemo } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { ScrollView } from 'react-native'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Menu } from '../../components/Svg'
import { Box, Header, Text, Loader, PetTag } from '../../components'

const History = ({ route, navigation }) => {
  const id = get(route, 'params.id')
  const { data, isValidating } = useFetch(`doctor/pets/${id}/history`)

  const pet = useMemo(() => get(data, 'data') || null, [data])

  const onDrawer = () => navigation.openDrawer()

  if (!pet) {
    return <Loader loading={isValidating} />
  }

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header right={<Menu />} onRight={onDrawer} />
        <Box flex px={28} pt={28}>
          <PetTag
            item={pet}
            textSize="h3"
            borderBottom="shade300"
            pb={28}
            mb={24}
          />
          {pet.appointments.map((appointment, index) => (
            <Box key={appointment.id} mb={24}>
              <Box row align="center" mb={16}>
                <Box
                  circle
                  bg="primary100"
                  align="center"
                  justify="center"
                  height={44}
                  width={44}
                  ml={-8}
                  mr={16}
                >
                  <Text uppercase type="h3" height={44} weight="medium">
                    {index + 1}
                  </Text>
                </Box>
                <Box>
                  <Text color="text" weight="medium">
                    {moment(appointment.date).format('ddd, MMM D, YYYY')}
                  </Text>
                  <Text color="text" weight="medium">
                    {moment(appointment.date).format('hh:mm A')}
                  </Text>
                </Box>
              </Box>
              {appointment.questions.map((question) =>
                question.question && question.answer ? (
                  <Box key={question.id} mb={24}>
                    <Text mb={12}>Q: {question.question.title}</Text>
                    <Text color="text" weight="medium">
                      A: {question.answer}
                    </Text>
                  </Box>
                ) : null
              )}
              {appointment.diagnosis && (
                <Box round bg="shade100" mx={-12} px={12} pt={16} mb={16}>
                  <Text type="h4" color="text" weight="medium" mb={12}>
                    Diagnosis
                  </Text>
                  <Text mb={24}>{appointment.diagnosis.content}</Text>
                </Box>
              )}
              {appointment.prescriptions.length > 0 && (
                <Box round bg="shade100" mx={-12} px={12} pt={16}>
                  <Text type="h4" color="text" weight="medium" mb={12}>
                    Prescriptions
                  </Text>
                  <Box>
                    <Box
                      row
                      justify="space-between"
                      borderBottom="shade300"
                      mb={16}
                    >
                      <Box basis="33%">
                        <Text mb={12}>Medicine</Text>
                      </Box>
                      <Box basis="33%">
                        <Text mb={12}>Usage</Text>
                      </Box>
                      <Box basis="33%">
                        <Text mb={12}>Directions</Text>
                      </Box>
                    </Box>
                    {appointment.prescriptions.map((prescription) => (
                      <Box
                        key={prescription.id}
                        borderBottom="shade300"
                        mb={16}
                      >
                        <Box row>
                          <Text
                            color="text"
                            weight="medium"
                            mb={20}
                            style={{ flex: 1, flexWrap: 'wrap' }}
                          >
                            {prescription.item}
                          </Text>
                          <Text
                            color="text"
                            weight="medium"
                            mb={20}
                            style={{ flex: 1, flexWrap: 'wrap' }}
                          >
                            {prescription.usage}
                          </Text>
                          <Text
                            color="text"
                            weight="medium"
                            mb={20}
                            style={{ flex: 1, flexWrap: 'wrap' }}
                          >
                            {prescription.directions}
                          </Text>
                        </Box>
                      </Box>
                    ))}
                  </Box>
                </Box>
              )}
            </Box>
          ))}
        </Box>
      </ScrollView>
    </Box>
  )
}

export default History
