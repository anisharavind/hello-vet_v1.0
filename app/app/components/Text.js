import React from 'react'
import { StyleSheet, Text as RnText } from 'react-native'

import { colors } from '../config'
import { normalize } from '../libs/utils'

export const textStyles = StyleSheet.create({
  button: {
    fontSize: normalize(18),
    lineHeight: normalize(32),
  },
  h1: {
    fontSize: normalize(30),
    lineHeight: normalize(32),
  },
  h2: {
    fontFamily: 'ApercuPro-Medium',
    fontSize: normalize(26),
    lineHeight: normalize(30),
  },
  h3: {
    fontSize: normalize(18),
    lineHeight: normalize(22),
  },
  h4: {
    fontSize: normalize(16),
    lineHeight: normalize(20),
  },
  input: {
    fontFamily: 'ApercuPro-Regular',
    fontSize: normalize(18),
    lineHeight: normalize(22),
  },
  label: {
    fontFamily: 'ApercuPro-Bold',
    fontSize: normalize(10),
    lineHeight: normalize(16),
  },
  p1: {
    fontSize: normalize(14),
    lineHeight: normalize(18),
  },
  p2: {
    fontSize: normalize(12),
    lineHeight: normalize(16),
  },
  p3: {
    fontSize: normalize(10),
    lineHeight: normalize(16),
  },
})

const fontFamily = (weight) => {
  switch (weight) {
    case 'medium':
      return 'ApercuPro-Medium'

    case 'bold':
      return 'ApercuPro-Bold'

    default:
      return 'ApercuPro-Regular'
  }
}

const Text = ({
  type = 'p1',
  color = 'text700',
  align,
  size,
  weight,
  height,
  space,
  uppercase,
  mt,
  mr,
  mb,
  ml,
  mx,
  my,
  children,
  style,
  ...props
}) => {
  const textStyle = [
    { fontFamily: fontFamily(weight) },
    type && textStyles[type],
    color && { color: colors[color] },
    align && { textAlign: align },
    size && { fontSize: normalize(size) },
    height && { lineHeight: normalize(height) },
    space && { letterSpacing: space },
    uppercase && { textTransform: 'uppercase' },
    mt && { marginTop: normalize(mt) },
    mr && { marginRight: normalize(mr) },
    mb && { marginBottom: normalize(mb) },
    ml && { marginLeft: normalize(ml) },
    mx && { marginHorizontal: normalize(mx) },
    my && { marginVertical: normalize(my) },
    style,
  ]

  return (
    <RnText style={textStyle} {...props}>
      {children}
    </RnText>
  )
}

export default Text
