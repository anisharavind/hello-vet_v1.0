import React from 'react'
import SafeAreaView from 'react-native-safe-area-view'
import { StyleSheet, View, TouchableOpacity } from 'react-native'

import { colors } from '../config'
import { normalize } from '../libs/utils'

const styles = StyleSheet.create({
  base: {
    flexShrink: 1,
  },
  border: {
    borderWidth: 1,
  },
  borderBottom: {
    borderBottomWidth: 1,
  },
  borderRight: {
    borderRightWidth: 1,
  },
  grow: {
    flexGrow: 1,
  },
  largeRound: {
    borderRadius: normalize(12),
  },
  round: {
    borderRadius: normalize(6),
  },
  row: {
    flexDirection: 'row',
  },
  safe: {
    flex: 1,
  },
})

const Box = ({
  safe,
  bg,
  flex,
  row,
  grow,
  justify,
  align,
  wrap,
  basis,
  height,
  width,
  mt,
  mr,
  mb,
  ml,
  mx,
  my,
  pt,
  pr,
  pb,
  pl,
  px,
  py,
  circle,
  round,
  largeRound,
  border,
  borderBottom,
  borderRight,
  children,
  style,
  ...props
}) => {
  const boxStyle = [
    styles.base,
    bg && { backgroundColor: colors[bg] },
    flex && { flex: flex === true ? 1 : flex },
    grow && styles.grow,
    row && styles.row,
    justify && { justifyContent: justify },
    align && { alignItems: align },
    wrap && { flexWrap: 'wrap' },
    basis && { flexBasis: basis },
    height && { height },
    width && { width },
    mt && { marginTop: normalize(mt) },
    mr && { marginRight: normalize(mr) },
    mb && { marginBottom: normalize(mb) },
    ml && { marginLeft: normalize(ml) },
    mx && { marginHorizontal: normalize(mx) },
    my && { marginVertical: normalize(my) },
    pt && { paddingTop: normalize(pt) },
    pr && { paddingRight: normalize(pr) },
    pb && { paddingBottom: normalize(pb) },
    pl && { paddingLeft: normalize(pl) },
    px && { paddingHorizontal: normalize(px) },
    py && { paddingVertical: normalize(py) },
    circle && height && { borderRadius: normalize(height), overflow: 'hidden' },
    round && styles.round,
    largeRound && styles.largeRound,
    border && { ...styles.border, borderColor: colors[border] },
    borderBottom && {
      ...styles.borderBottom,
      borderBottomColor: colors[borderBottom],
    },
    borderRight && {
      ...styles.borderRight,
      borderRightColor: colors[borderRight],
    },
    style,
  ]

  const shouldWrap =
    props.onPress || props.onLongPress || props.onPressIn || props.onPressOut

  const view = (
    <View style={boxStyle} {...props}>
      {children}
    </View>
  )

  if (safe) {
    return (
      <SafeAreaView style={[boxStyle, styles.safe]}>{children}</SafeAreaView>
    )
  }

  if (shouldWrap) {
    return (
      <TouchableOpacity activeOpacity={0.8} {...props}>
        {view}
      </TouchableOpacity>
    )
  }

  return view
}

export default Box
