import React, { useMemo } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { useNavigation } from '@react-navigation/native'

import Box from '../Box'
import Text from '../Text'
import Loader from '../Loader'
import PetTag from '../PetTag'
import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { normalize } from '../../libs/utils'
import { ArrowLeft, ArrowRight } from '../Svg'

const Day = ({ date, setDate }) => {
  const navigation = useNavigation()
  const { data, isValidating } = useFetch(
    `doctor/calendar?view=day&date=${date.format('YYYY-MM-DD')}`
  )

  const appointments = useMemo(() => get(data, 'appointments') || [], [data])

  const onPrev = () => setDate((d) => d.clone().subtract(1, 'day'))
  const onNext = () => setDate((d) => d.clone().add(1, 'day'))
  const onAppointment = (item) =>
    navigation.navigate('Appointment', { id: item.id })

  return (
    <Box flex>
      <Box
        row
        align="center"
        justify="space-between"
        borderBottom="shade300"
        py={28}
      >
        <Text type="h4">{date.format('DD MMM YYYY')}</Text>
        <Box row>
          <Box onPress={onPrev} mr={16}>
            <ArrowLeft color="text700" />
          </Box>
          <Box onPress={onNext}>
            <ArrowRight color="text700" />
          </Box>
        </Box>
      </Box>
      {appointments.map((appointment) => (
        <Box
          row
          align="center"
          justify="space-between"
          borderBottom="shade300"
          key={appointment.id}
        >
          <Box
            pr={12}
            py={16}
            style={styles.noShrink}
            width={normalize(80)}
            justify="space-around"
            borderRight="shade300"
          >
            <Text mb={28}>{moment(appointment.date).format('hh:mm A')}</Text>
            <Text>
              {moment(appointment.date).add(15, 'minutes').format('hh:mm A')}
            </Text>
          </Box>
          <Box grow pl={16}>
            <PetTag
              item={appointment.pet}
              round
              bg="primary300"
              align="center"
              px={12}
              py={20}
              onPress={() => onAppointment(appointment)}
            />
          </Box>
        </Box>
      ))}
      {appointments.length === 0 && (
        <Loader loading={isValidating} dataText="No appointments" />
      )}
    </Box>
  )
}

export default Day
