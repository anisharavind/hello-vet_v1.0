import React, { useMemo } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import { batch } from 'react-redux'
import calendarize from 'calendarize'

import Box from '../Box'
import Text from '../Text'
import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { normalize } from '../../libs/utils'
import { ArrowLeft, ArrowRight } from '../Svg'

const days = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT']

const Month = ({ date, setDate, setView }) => {
  const { data } = useFetch(
    `doctor/calendar?view=month&date=${date
      .startOf('month')
      .format('YYYY-MM-DD')}`
  )

  const dates = calendarize(date.startOf('month').format('YYYY-MM-DD'))
  const appointments = useMemo(() => get(data, 'appointments') || [], [data])

  const onPrev = () =>
    setDate((d) => d.clone().startOf('month').subtract(1, 'month'))
  const onNext = () => setDate((d) => d.clone().endOf('month').add(1, 'day'))

  const onDay = (d) => {
    batch(() => {
      setDate(d)
      setView({ id: 1, name: 'Day' })
    })
  }

  const Count = ({ day }) => {
    const d = moment().date(day).format('YYYY-MM-DD')
    const count = appointments.find((appointment) => appointment.day === d)

    if (!count) {
      return null
    }

    return (
      <Box
        circle
        bg="primary300"
        align="center"
        justify="center"
        width={normalize(20)}
        height={normalize(20)}
        style={styles.count}
      >
        <Text type="p3">{count.count}</Text>
      </Box>
    )
  }

  return (
    <Box flex>
      <Box
        row
        align="center"
        justify="space-between"
        borderBottom="shade300"
        py={28}
      >
        <Text type="h4">{date.format('MMM YYYY')}</Text>
        <Box row>
          <Box onPress={onPrev} mr={16}>
            <ArrowLeft color="text700" />
          </Box>
          <Box onPress={onNext}>
            <ArrowRight color="text700" />
          </Box>
        </Box>
      </Box>

      <Box row style={styles.borderLeft}>
        {days.map((day) => (
          <Box
            align="center"
            justify="center"
            basis="14.28%"
            height={normalize(68)}
            borderBottom="shade300"
            borderRight="shade300"
            key={day}
          >
            <Text type="p2">{day}</Text>
          </Box>
        ))}
      </Box>
      {dates.map((row) => (
        <Box row key={row} style={styles.borderLeft}>
          {row.map((col) => (
            <Box basis="14.28%" key={col}>
              <Box
                align="center"
                justify="center"
                height={normalize(68)}
                borderBottom="shade300"
                borderRight="shade300"
                onPress={() => onDay(moment().date(col))}
              >
                <Count day={col} />
                <Text type="p2">{col === 0 ? '' : col}</Text>
              </Box>
            </Box>
          ))}
        </Box>
      ))}
    </Box>
  )
}

export default Month
