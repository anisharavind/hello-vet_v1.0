import React, { useMemo } from 'react'

import { getAge } from '../libs/utils'

import Box from './Box'
import Text from './Text'

const PetTag = ({ item, textSize, textColor, ...props }) => {
  const pet = useMemo(() => {
    const dob = item.dob || item.age
    const age = getAge(dob)

    return {
      name: item.name,
      image: item.image,
      age: `${age.years}.${age.months} yrs`,
      sex: item.sex === 0 ? 'Female' : 'Male',
      category: item.category.name,
    }
  }, [item])

  return (
    <Box {...props}>
      <Text type={textSize || 'p1'} color={textColor || 'text'}>
        {pet.name} {pet.age} / {pet.sex} / {pet.category}
      </Text>
    </Box>
  )
}

export default PetTag
