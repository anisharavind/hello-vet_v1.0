import React from 'react'
import { useNavigation } from '@react-navigation/native'

import { images } from '../config'
import { normalize } from '../libs/utils'

import Box from './Box'
import Text from './Text'
import Image from './Image'
import { ArrowLeft } from './Svg'

const Header = ({ title, onBack, hideBack, right, onRight, children }) => {
  const navigation = useNavigation()

  const goBack = () => navigation.goBack()

  return (
    <Box>
      <Box
        row
        align="center"
        justify="space-between"
        height={normalize(60)}
        mb={!(children || title) ? 0 : 36}
        px={28}
        borderBottom="shade500"
      >
        <Box row align="center">
          {!hideBack && (
            <Box onPress={onBack || goBack} ml={-8} px={8} py={4} mr={12}>
              <ArrowLeft />
            </Box>
          )}
          <Box circle bg="primary" height={normalize(36)} width={normalize(36)}>
            <Image
              image={images.logo}
              width={normalize(36)}
              height={normalize(36)}
            />
          </Box>
        </Box>
        {right && (
          <Box onPress={onRight} mr={-8} px={8} py={4}>
            {right}
          </Box>
        )}
      </Box>
      {(children || title) && (
        <Box px={28} pb={56}>
          <Text type="h2" color="text">
            {children || title}
          </Text>
        </Box>
      )}
    </Box>
  )
}

export default Header
