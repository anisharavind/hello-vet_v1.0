import React, { useMemo } from 'react'

import { getAge } from '../libs/utils'

import Box from './Box'
import Text from './Text'
import Image from './Image'

const Pet = ({ item, onPress }) => {
  const pet = useMemo(() => {
    const dob = item.dob || item.age
    const age = getAge(dob)

    return {
      name: item.name,
      image: item.image,
      age: `${age.years}.${age.months} yrs`,
      sex: item.sex === 0 ? 'Female' : 'Male',
      category: item.category.name,
    }
  }, [item])

  return (
    <Box row align="center" onPress={() => onPress(item)}>
      <Box
        circle
        bg="primary100"
        align="center"
        justify="center"
        height={44}
        width={44}
        mr={16}
      >
        {pet.image ? (
          <Image source={pet.image} width={44} height={44} />
        ) : (
          <Text uppercase type="h3" height={44} weight="medium">
            {String(pet.name).charAt(0)}
          </Text>
        )}
      </Box>
      <Box>
        <Text color="text" weight="medium">
          {pet.name}
        </Text>
        <Text weight="medium">
          {pet.age}, {pet.sex}, {pet.category}
        </Text>
      </Box>
    </Box>
  )
}

export default Pet
