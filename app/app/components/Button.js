import React from 'react'
import isString from 'lodash/isString'
import { StyleSheet, ActivityIndicator } from 'react-native'

import { colors } from '../config'

import Box from './Box'
import Text from './Text'

const styles = StyleSheet.create({
  loader: {
    position: 'absolute',
    zIndex: 1,
  },
})

const Button = ({
  onPress,
  bg,
  ghost,
  small,
  type,
  left,
  right,
  loading,
  disabled,
  children,
  ...props
}) => (
  <Box
    disabled={disabled || loading}
    onPress={onPress}
    style={{ opacity: disabled ? 0.8 : 1 }}
    {...props}
  >
    {loading && (
      <Box
        circle
        bg={ghost ? 'bg' : bg || 'primary'}
        align="center"
        justify="center"
        width="100%"
        height={ghost || small ? 44 : 56}
        style={styles.loader}
      >
        <ActivityIndicator color={ghost ? colors[type] : colors.textAlt} />
      </Box>
    )}
    <Box
      row
      circle
      align="center"
      justify="center"
      height={ghost || small ? 44 : 56}
      bg={ghost ? 'transparent' : bg || 'primary'}
      px={ghost ? 0 : 24}
    >
      {left && (
        <Box mr={12} ml={-4}>
          {left}
        </Box>
      )}
      {isString(children) ? (
        <Text
          type={ghost || small ? 'p1' : 'button'}
          color={ghost ? type : 'textAlt'}
          weight={ghost ? 'medium' : 'bold'}
        >
          {children}
        </Text>
      ) : (
        children
      )}
      {right && (
        <Box ml={12} mr={-4}>
          {right}
        </Box>
      )}
    </Box>
  </Box>
)

export default Button
