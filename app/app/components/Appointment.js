import React from 'react'
import moment from 'moment'

import Box from './Box'
import Text from './Text'
import Image from './Image'

const Appointment = ({ item, status, onPress }) => (
  <Box round bg={`${status}300`} px={8} py={12} onPress={() => onPress(item)}>
    <Box row align="center">
      {item.pet?.image ? (
        <Box
          circle
          align="center"
          justify="center"
          width={48}
          height={48}
          ml={8}
          mr={12}
        >
          <Image source={item.pet?.image} width={48} height={48} />
        </Box>
      ) : (
        <Box
          circle
          bg={`${status}300`}
          align="center"
          justify="center"
          height={48}
          width={48}
          ml={8}
          mr={12}
        >
          <Text uppercase type="h3" height={48} weight="medium">
            {String(item.pet?.name).charAt(0)}
          </Text>
        </Box>
      )}
      <Box>
        <Box align="flex-start">
          <Box
            circle
            bg={`${status}300`}
            align="center"
            justify="center"
            height={24}
            px={12}
            mb={8}
          >
            <Text uppercase type="p2" height={24} weight="medium">
              {item.pet?.name}
            </Text>
          </Box>
        </Box>
        <Box row>
          <Text weight="medium" color="text" mr={8}>
            {moment(item.date).format('ddd, MMM D')}
          </Text>
          <Text weight="medium">{moment(item.date).format('hh:mm A')}</Text>
        </Box>
      </Box>
    </Box>
  </Box>
)

export default Appointment
