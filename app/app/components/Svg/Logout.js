import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Logout = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M378 108a191.41 191.41 0 0170 148c0 106-86 192-192 192S64 362 64 256a192 192 0 0169-148M256 64v192"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
    </Base>
  )
}

export default Logout
