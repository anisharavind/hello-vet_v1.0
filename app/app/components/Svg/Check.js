import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Check = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z"
        strokeMiterlimit={10}
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M352 176L217.6 336 160 272"
      />
    </Base>
  )
}

export default Check
