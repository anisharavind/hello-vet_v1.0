import React from 'react'
import { Path, Rect } from 'react-native-svg'

import Base from './Base'

const Phone = (props) => {
  return (
    <Base {...props}>
      <Rect
        x={128}
        y={16}
        width={256}
        height={480}
        rx={48}
        ry={48}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
      <Path
        d="M176 16h24a8 8 0 018 8h0a16 16 0 0016 16h64a16 16 0 0016-16h0a8 8 0 018-8h24"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
    </Base>
  )
}

export default Phone
