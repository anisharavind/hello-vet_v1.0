import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Search = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M221.09 64a157.09 157.09 0 10157.09 157.09A157.1 157.1 0 00221.09 64z"
        strokeMiterlimit={10}
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeMiterlimit={10}
        strokeWidth={32}
        d="M338.29 338.29L448 448"
      />
    </Base>
  )
}

export default Search
