import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Plus = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeWidth={32}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M256 112v288M400 256H112"
      />
    </Base>
  )
}

export default Plus
