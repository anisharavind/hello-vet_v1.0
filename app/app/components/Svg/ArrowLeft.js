import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const ArrowLeft = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={48}
        d="M244 400L100 256l144-144M120 256h292"
      />
    </Base>
  )
}

export default ArrowLeft
