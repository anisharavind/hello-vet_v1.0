import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Menu = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeLinecap="round"
        strokeMiterlimit={10}
        strokeWidth={32}
        d="M80 160h352M80 256h352M80 352h352"
      />
    </Base>
  )
}

export default Menu
