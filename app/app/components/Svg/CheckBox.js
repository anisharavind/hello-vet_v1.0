import React from 'react'
import { Path, Rect } from 'react-native-svg'

import Base from './Base'

export const UnChecked = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M416 448H96a32.09 32.09 0 01-32-32V96a32.09 32.09 0 0132-32h320a32.09 32.09 0 0132 32v320a32.09 32.09 0 01-32 32z"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
    </Base>
  )
}

export const Checked = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M352 176L217.6 336 160 272"
      />
      <Rect
        x={64}
        y={64}
        width={384}
        height={384}
        rx={48}
        ry={48}
        strokeLinejoin="round"
        strokeWidth={32}
      />
    </Base>
  )
}
