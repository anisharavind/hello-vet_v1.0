import React from 'react'
import Svg, { Path, Circle } from 'react-native-svg'

import { colors } from '../../config'

const DotArrow = ({
  width = 24,
  height = 24,
  color = 'text',
  children,
  ...props
}) => {
  return (
    <Svg
      viewBox="0 0 24 24"
      width={width}
      height={height}
      fill={colors[color]}
      {...props}
    >
      <Path fill="none" d="M0 0h24v24H0z" />
      <Path
        d="M19.172 11l-5.364-5.364 1.414-1.414L23 12l-7.778 7.778-1.414-1.414L19.172 13H7v-2h12.172z"
        fill="#7D39C4"
      />
      <Circle cx={2} cy={12} r={1} fill="#7D39C4" />
    </Svg>
  )
}

export default DotArrow
