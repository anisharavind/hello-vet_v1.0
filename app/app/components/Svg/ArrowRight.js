import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const ArrowRight = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={48}
        d="M268 112l144 144-144 144M392 256H100"
      />
    </Base>
  )
}

export default ArrowRight
