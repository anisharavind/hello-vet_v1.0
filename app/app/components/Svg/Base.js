import React from 'react'
import Svg from 'react-native-svg'

import { colors } from '../../config'

const Base = ({
  width = 24,
  height = 24,
  color = 'text',
  children,
  ...props
}) => {
  return (
    <Svg
      viewBox="0 0 512 512"
      width={width}
      height={height}
      fill="none"
      stroke={colors[color]}
      {...props}
    >
      {children}
    </Svg>
  )
}

export default React.memo(Base)
