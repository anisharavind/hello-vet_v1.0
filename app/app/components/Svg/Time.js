import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Time = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M256 64C150 64 64 150 64 256s86 192 192 192 192-86 192-192S362 64 256 64z"
        strokeMiterlimit={10}
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M256 128v144h96"
      />
    </Base>
  )
}

export default Time
