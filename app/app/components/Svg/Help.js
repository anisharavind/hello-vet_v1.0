import React from 'react'
import { Path, Circle } from 'react-native-svg'

import Base from './Base'

const Help = (props) => {
  return (
    <Base {...props}>
      <Circle
        cx={256}
        cy={256}
        r={208}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
      <Circle
        cx={256}
        cy={256}
        r={80}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M208 54l8 132M296 186l8-132M208 458l8-132M296 326l8 132M458 208l-132 8M326 296l132 8M54 208l132 8M186 296l-132 8"
      />
    </Base>
  )
}

export default Help
