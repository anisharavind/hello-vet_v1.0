import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const ArrowDown = (props) => {
  return (
    <Base {...props}>
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={48}
        d="M112 184l144 144 144-144"
      />
    </Base>
  )
}

export default ArrowDown
