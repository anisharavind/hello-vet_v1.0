import React from 'react'
import { Path } from 'react-native-svg'

import Base from './Base'

const Download = (props) => {
  return (
    <Base {...props}>
      <Path
        d="M336 176h40a40 40 0 0140 40v208a40 40 0 01-40 40H136a40 40 0 01-40-40V216a40 40 0 0140-40h40"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M176 272l80 80 80-80M256 48v288"
      />
    </Base>
  )
}

export default Download
