import React from 'react'
import { Path, Rect } from 'react-native-svg'

import Base from './Base'

const Mail = (props) => {
  return (
    <Base {...props}>
      <Rect
        x={48}
        y={96}
        width={416}
        height={320}
        rx={40}
        ry={40}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
      />
      <Path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={32}
        d="M112 160l144 112 144-112"
      />
    </Base>
  )
}

export default Mail
