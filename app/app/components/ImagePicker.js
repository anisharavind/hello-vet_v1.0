import React, { useRef, useMemo } from 'react'
import get from 'lodash/get'
import RBSheet from 'react-native-raw-bottom-sheet'
import ImagePicker from 'react-native-image-crop-picker'

import { normalize } from '../libs/utils'

import Box from './Box'
import Text from './Text'
import InputWrapper from './InputWrapper'
import { Cloud, Camera, Image } from './Svg'

const options = {
  width: 720,
  height: 720,
  cropping: true,
}

const ImagesPicker = ({ value, error, onSelect }) => {
  const menu = useRef(null)
  const path = useMemo(() => get(value, 'name') || null, [value])

  const onGallery = () => {
    ImagePicker.openPicker(options)
      .then((image) => {
        onSelect({
          uri: image.path,
          type: image.mime,
          name: 'image.jpg',
        })

        if (menu.current) menu.current.close()
      })
      .catch(() => {})
  }

  const onCamera = () => {
    ImagePicker.openCamera(options)
      .then((image) => {
        onSelect({
          uri: image.path,
          type: image.mime,
          name: 'image.jpg',
        })

        if (menu.current) menu.current.close()
      })
      .catch(() => {})
  }

  const onPicture = () => menu.current && menu.current.open()

  return (
    <InputWrapper error={error} onPress={onPicture}>
      <Box row align="center" height={60} px={16}>
        <Cloud width={20} color="text700" />
        <Text numberOfLines={1} ml={16}>
          {path || 'Upload image'}
        </Text>
      </Box>
      <RBSheet ref={menu} height={normalize(180)}>
        <Box px={28} py={28}>
          <Box row py={16} onPress={onCamera}>
            <Camera color="primary" width={24} />
            <Text type="h3" color="text" ml={20}>
              Take photo
            </Text>
          </Box>
          <Box row py={16} onPress={onGallery}>
            <Image color="primary" width={24} />
            <Text type="h3" color="text" ml={20}>
              Choose image
            </Text>
          </Box>
        </Box>
      </RBSheet>
    </InputWrapper>
  )
}

export default ImagesPicker
