import React from 'react'
import LottieView from 'lottie-react-native'
import { Placeholder, PlaceholderLine, Fade } from 'rn-placeholder'

import { lottie } from '../config'
import { normalize } from '../libs/utils'

import Box from './Box'
import Button from './Button'

const Loader = ({ loading, mutate }) => (
  <Box height={normalize(320)} px={28} py={28}>
    {loading ? (
      <Placeholder Animation={Fade}>
        <PlaceholderLine width={80} />
        <PlaceholderLine />
        <PlaceholderLine width={30} />
      </Placeholder>
    ) : (
      <Box flex>
        <LottieView source={lottie.error} autoPlay loop />
        {typeof mutate === 'function' && (
          <Button ghost type="primary" onPress={() => mutate()}>
            Retry
          </Button>
        )}
      </Box>
    )}
  </Box>
)

export default Loader
