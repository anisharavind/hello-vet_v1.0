import React from 'react'

import Box from './Box'
import Text from './Text'
import { Checked, UnChecked } from './Svg/CheckBox'

const Checkbox = ({ value, onChange, label }) => {
  return (
    <Box row onPress={onChange}>
      {value ? <Checked color="primary" /> : <UnChecked color="primary" />}
      <Text ml={8}>{label}</Text>
    </Box>
  )
}

export default Checkbox
