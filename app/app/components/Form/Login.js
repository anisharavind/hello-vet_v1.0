import React, { useState, useCallback } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { Formik } from 'formik'
import { Toast } from 'react-native-root-toaster'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'

import request from '../../libs/request'
import { setUser } from '../../store/user'
import { errorMsg } from '../../libs/utils'
import { Box, Text, Input, Button } from '../'
import Checkbox from '../Checkbox'

const initialValues = { phone: '' }
const validationSchema = Yup.object().shape({
  phone: Yup.string().length(10).required(),
})

const LoginForm = () => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const [terms, setTerms] = useState(false)
  const { user } = useSelector((state) => state.user)

  const onChange = () => setTerms((t) => !t)

  const onSubmit = useCallback(async (values, { setErrors }) => {
    try {
      await request.post('login', values)
      dispatch(setUser(values))
      navigation.navigate('Verify')
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  return (
    <Formik
      onSubmit={onSubmit}
      validateOnChange={false}
      validationSchema={validationSchema}
      initialValues={user || initialValues}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <Box flex>
          <Box mb={8}>
            <Input
              mask="custom"
              options={{
                mask: '9999999999',
              }}
              label="Phone number"
              left={
                <Text type="h3" color="text" weight="bold">
                  +91
                </Text>
              }
              value={values.phone}
              error={errorMsg(errors.phone, touched.phone)}
              onChangeText={handleChange('phone')}
              onBlur={handleBlur('phone')}
              autoFocus
              keyboardType="phone-pad"
              autoCompleteType="tel"
              importantForAutofill="yes"
              textContentType="telephoneNumber"
            />
          </Box>
          <Text type="p1" mb={16}>
            {`We’ll send an SMS to verify the mobile number. \nCarrier charges may apply.`}
          </Text>
          <Box round bg="shade100" px={16} py={16} mx={-8}>
            <Text type="p1" mb={12}>
              This preliminary consultation is not a final diagnosis, although
              our advice/prescription shall be in goog faith and at its
              professional best, we shall not be held liable for any unexpected
              reactions or legal proceedings initiated against our advice.
            </Text>
            <Checkbox
              value={terms}
              onChange={onChange}
              label="We agree to the above terms and conditions."
            />
          </Box>
          <Box align="flex-start" mt={24}>
            <Button
              disabled={!terms}
              loading={isSubmitting}
              onPress={handleSubmit}
            >
              Continue
            </Button>
          </Box>
        </Box>
      )}
    </Formik>
  )
}

export default LoginForm
