import React, { useCallback, useState, useEffect } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { useFormik } from 'formik'
import isString from 'lodash/isString'
import { batch, useDispatch } from 'react-redux'
import { Toast } from 'react-native-root-toaster'
import { Platform, Keyboard } from 'react-native'
import SmsRetriever from 'react-native-sms-retriever'
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'

import request from '../../libs/request'
import { errorMsg } from '../../libs/utils'
import { Box, Text, Input, Button } from '../'
import { setUser, setToken, setHome } from '../../store/user'

const validationSchema = Yup.object().shape({
  phone: Yup.string().length(10).required(),
  code: Yup.string().length(6).required(),
})

const VerifyForm = ({ user }) => {
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const [isPlaying, setIsPlaying] = useState(false)
  const [showResend, setShowResend] = useState(false)

  const onSubmit = useCallback(async (values, { setErrors }) => {
    try {
      Keyboard.dismiss()

      const data = await request.post('verify', values)
      const users = get(data, 'data.data')
      const token = get(data, 'data.meta.token')
      const home = get(data, 'data.meta.home')

      batch(() => {
        dispatch(setUser(users || null))
        dispatch(setToken(token || null))
        dispatch(setHome(home || 'booking'))
      })
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  const {
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
    values,
    errors,
    touched,
    isSubmitting,
    submitForm,
  } = useFormik({
    initialValues: { code: '', phone: user.phone },
    validationSchema,
    onSubmit,
  })

  useEffect(() => {
    const retrieveSms = async () => {
      try {
        const registered = await SmsRetriever.startSmsRetriever()
        if (registered) {
          SmsRetriever.addSmsListener(async (event) => {
            const { message } = event

            if (message && isString(message)) {
              const regex = /\d+/g
              const otp = message.match(regex)

              setFieldValue('code', otp[0])
              await submitForm()
            }
          })
        }
      } catch (error) {
        //
      }
    }

    if (Platform.OS === 'android') {
      retrieveSms()
    }

    setIsPlaying(true)

    return () => {
      if (Platform.OS === 'android') {
        SmsRetriever.removeSmsListener()
      }
    }
  }, [])

  const onComplete = () => setShowResend(true)

  const onResend = useCallback(async () => {
    setLoading(true)

    try {
      await request.post('verify/resend', user)
      setShowResend(false)
      Toast.show('OTP sent successfully')
    } catch (error) {
      const message = get(error, 'response.data.message') || null

      Toast.show(message || error.message)
    }

    setLoading(false)
  }, [])

  return (
    <Box flex>
      <Box mb={8}>
        <Input
          mask="custom"
          options={{
            mask: '999999',
          }}
          label="Enter OTP"
          value={values.code}
          error={errorMsg(errors.code, touched.code)}
          onChangeText={handleChange('code')}
          onBlur={handleBlur('code')}
          onSubmitEditing={handleSubmit}
          keyboardType="phone-pad"
        />
      </Box>
      {showResend ? (
        <Box align="flex-start">
          <Button ghost type="primary" loading={loading} onPress={onResend}>
            Resend
          </Button>
        </Box>
      ) : (
        <Box row align="center" height={44}>
          <Text>Request new OTP in </Text>
          <CountdownCircleTimer
            isPlaying={isPlaying}
            size={44}
            duration={60}
            strokeWidth={0}
            colors={[['#fff']]}
            onComplete={onComplete}
          >
            {({ remainingTime }) => (
              <Text color="text" weight="bold">
                {remainingTime}
              </Text>
            )}
          </CountdownCircleTimer>
        </Box>
      )}
      <Box align="flex-start" mt={44}>
        <Button loading={isSubmitting} onPress={handleSubmit}>
          Continue
        </Button>
      </Box>
    </Box>
  )
}

export default VerifyForm
