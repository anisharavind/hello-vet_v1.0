/* eslint-disable no-nested-ternary */
import React, { useCallback, useRef, useState, useMemo } from 'react'
import * as Yup from 'yup'
import moment from 'moment'
import get from 'lodash/get'
import { mutate } from 'swr'
import { Formik } from 'formik'
import isEqual from 'lodash/isEqual'
import { Toast } from 'react-native-root-toaster'
import RBSheet from 'react-native-raw-bottom-sheet'
import ImagePicker from 'react-native-image-crop-picker'

import styles from '../../styles'
import { sex } from '../../config'
// import DatePicker from '../DatePicker'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { errorMsg, normalize } from '../../libs/utils'
import { Box, Input, Button, Select, Image, Text } from '../'
import { ArrowDown, Camera, Image as ImageIcon } from '../Svg'

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  category: Yup.object().required(),
  sex: Yup.object().required(),
  years: Yup.number(),
  months: Yup.number().when('years', {
    is: (years) => years === undefined,
    then: Yup.number().required(),
  }),
  // dob: Yup.string()
  //   .when(['years', 'months'], {
  //     is: (years, months) => years === undefined || months === undefined,
  //     then: Yup.string().required(),
  //   })
  //   .nullable(),
  breed: Yup.object().required(),
})
const options = {
  width: 360,
  height: 360,
  cropping: true,
}

const PetUpdateForm = ({ pet }) => {
  const menu = useRef(null)
  const months = useRef(null)
  const [pic, setPic] = useState(null)
  const [category, setCategory] = useState(pet.category)

  const { data } = useFetch('categories')
  const { data: breedData } = useFetch(
    category ? `categories/${category.id}/breeds` : null
  )

  const categories = useMemo(() => get(data, 'data') || [], [data])
  const breeds = useMemo(() => get(breedData, 'data') || [], [breedData])

  const focusMonths = () => months.current && months.current.focus()
  const onCategorySelect = useCallback(
    (item, setFieldValue) => {
      setCategory(item)
      setFieldValue('category', item)

      if (!isEqual(category, item)) setFieldValue('breed', '')
    },
    [category]
  )

  const onSubmit = useCallback(
    async (values, { setErrors }) => {
      try {
        const body = new FormData()

        body.append('_method', 'patch')
        body.append('name', values.name)
        body.append('category_id', values.category.id)
        body.append('breed_id', values.breed.id)
        body.append('sex', values.sex.value)
        body.append(
          'age',
          moment(1, 'DD')
            .subtract(values.years || 0, 'years')
            .subtract(values.months || 0, 'months')
            .format('YYYY-MM-DD')
        )

        // if (values.dob) {
        //   body.append('dob', moment(values.dob).format('YYYY-MM-DD'))
        // }

        if (pic) {
          body.append('image', pic)
        }

        await request.post(`pets/${pet.id}`, body)

        mutate('pets')
        mutate(`pets/${pet.id}`)
        Toast.show('Pet profile updated')
      } catch (error) {
        const errors = get(error, 'response.data.errors') || {}
        const message = get(error, 'response.data.message') || null

        setErrors(errors)
        Toast.show(message || error.message)
      }
    },
    [pic]
  )

  const onGallery = () => {
    ImagePicker.openPicker(options)
      .then((image) => {
        setPic({
          uri: image.path,
          type: image.mime,
          name: 'image.jpg',
        })

        if (menu.current) menu.current.close()
      })
      .catch(() => {})
  }

  const onCamera = () => {
    ImagePicker.openCamera(options)
      .then((image) => {
        setPic({
          uri: image.path,
          type: image.mime,
          name: 'image.jpg',
        })

        if (menu.current) menu.current.close()
      })
      .catch(() => {})
  }

  const onPicture = () => menu.current && menu.current.open()

  return (
    <Formik
      enableReinitialize
      onSubmit={onSubmit}
      initialValues={pet}
      validateOnChange={false}
      validationSchema={validationSchema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <>
          <Box align="flex-start" mb={24}>
            <Box
              circle
              bg="shade300"
              align="center"
              justify="center"
              width={80}
              height={80}
              onPress={onPicture}
            >
              {pic && pic.uri ? (
                <Image source={pic.uri} width={80} height={80} />
              ) : values.image ? (
                <Image source={values.image} width={80} height={80} />
              ) : null}
              <Box
                circle
                align="center"
                justify="center"
                height={32}
                width={32}
                bg="primary"
                style={styles.absolute}
              >
                <Camera width={20} height={20} color="textAlt" />
              </Box>
            </Box>
          </Box>
          <Box mb={24}>
            <Input
              label="Name of pet"
              value={values.name}
              error={errorMsg(errors.name, touched.name)}
              onChangeText={handleChange('name')}
              onBlur={handleBlur('name')}
            />
          </Box>
          <Box row mb={24}>
            <Box flex mr={8}>
              <Select
                label="Pet type"
                data={categories}
                selected={values.category}
                onSelect={(item) => onCategorySelect(item, setFieldValue)}
                right={<ArrowDown color="shade500" width={20} />}
                error={errorMsg(errors.category, touched.category)}
              />
            </Box>
            <Box flex ml={8}>
              <Select
                label="Sex"
                data={sex}
                selected={values.sex}
                onSelect={(item) => setFieldValue('sex', item)}
                right={<ArrowDown color="shade500" width={20} />}
                error={errorMsg(errors.sex, touched.sex)}
              />
            </Box>
          </Box>
          <Box row mb={24}>
            <Box flex mr={8}>
              <Input
                label="Age (years)"
                value={values.years}
                error={errorMsg(errors.years, touched.years)}
                onChangeText={handleChange('years')}
                onBlur={handleBlur('years')}
                onSubmitEditing={focusMonths}
              />
            </Box>
            <Box flex ml={8}>
              <Input
                ref={months}
                label="Months"
                value={values.months}
                error={errorMsg(errors.months, touched.months)}
                onChangeText={handleChange('months')}
                onBlur={handleBlur('months')}
              />
            </Box>
          </Box>
          {/* <Box mb={24}>
            <DatePicker
              label="Date of Birth"
              selected={values.dob}
              onSelect={(item) => setFieldValue('dob', item)}
              right={<ArrowDown color="shade500" width={20} />}
              error={errorMsg(errors.dob, touched.dob)}
            />
          </Box> */}
          <Box>
            <Select
              searchable
              label="Breed"
              data={breeds}
              selected={values.breed}
              onSelect={(item) => setFieldValue('breed', item)}
              right={<ArrowDown color="shade500" width={20} />}
              error={errorMsg(errors.breed, touched.breed)}
            />
          </Box>
          <Box align="flex-start" mt={56}>
            <Button loading={isSubmitting} onPress={handleSubmit}>
              Save profile
            </Button>
          </Box>

          <RBSheet ref={menu} height={normalize(180)}>
            <Box px={28} py={28}>
              <Box row py={16} onPress={onCamera}>
                <Camera color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Take photo
                </Text>
              </Box>
              <Box row py={16} onPress={onGallery}>
                <ImageIcon color="primary" width={24} />
                <Text type="h3" color="text" ml={20}>
                  Choose image
                </Text>
              </Box>
            </Box>
          </RBSheet>
        </>
      )}
    </Formik>
  )
}

export default PetUpdateForm
