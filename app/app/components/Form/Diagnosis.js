import React, { useCallback } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { mutate } from 'swr'
import { Formik } from 'formik'
import { Toast } from 'react-native-root-toaster'
import { useNavigation } from '@react-navigation/native'

import { Box, Input, Button } from '../'
import request from '../../libs/request'
import { errorMsg } from '../../libs/utils'

const initialValues = {
  content: undefined,
}
const validationSchema = Yup.object().shape({
  content: Yup.string().required('Required'),
})

const DiagnosisForm = ({ appointment }) => {
  const navigation = useNavigation()

  const onSubmit = useCallback(async (values, { setErrors, resetForm }) => {
    try {
      await request.post(
        `doctor/appointments/${appointment.id}/diagnosis`,
        values
      )

      mutate(`doctor/appointments/${appointment.id}`)
      mutate(`doctor/pets/${appointment.pet.id}/history`)
      resetForm()
      Toast.show('Diagnosis added')
      navigation.navigate('Appointment', { id: appointment.id })
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  return (
    <Formik
      onSubmit={onSubmit}
      validateOnChange={false}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <Box flex>
          <Box mt={24}>
            <Input
              multiline
              numberOfLines={6}
              label="Diagnosis"
              value={values.content}
              error={errorMsg(errors.content, touched.content)}
              onChangeText={handleChange('content')}
              onBlur={handleBlur('content')}
            />
          </Box>
          <Box align="flex-start" mt={56}>
            <Button loading={isSubmitting} onPress={handleSubmit}>
              Submit
            </Button>
          </Box>
        </Box>
      )}
    </Formik>
  )
}

export default DiagnosisForm
