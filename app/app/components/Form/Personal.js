import React, { useCallback, useRef } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { Formik } from 'formik'
import { useDispatch } from 'react-redux'
import { Toast } from 'react-native-root-toaster'
import { useNavigation } from '@react-navigation/native'

import { Box, Input, Button } from '../'
import request from '../../libs/request'
import { setUser } from '../../store/user'
import { errorMsg } from '../../libs/utils'

const initialValues = { name: '', email: '' }
const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string().email().required(),
})

const PersonalForm = () => {
  const email = useRef(null)
  const dispatch = useDispatch()
  const navigation = useNavigation()

  const focusEmail = () => email.current && email.current.focus()

  const onSubmit = useCallback(async (values, { setErrors }) => {
    try {
      const data = await request.post('user', values)
      const user = get(data, 'data.data')

      dispatch(setUser(user))
      navigation.navigate('Home')
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  return (
    <Formik
      onSubmit={onSubmit}
      validateOnChange={false}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <Box flex>
          <Box mb={24}>
            <Input
              label="Your full name"
              value={values.name}
              error={errorMsg(errors.name, touched.name)}
              onChangeText={handleChange('name')}
              onBlur={handleBlur('name')}
              onSubmitEditing={focusEmail}
              autoFocus
              autoCompleteType="name"
              textContentType="name"
            />
          </Box>
          <Box>
            <Input
              ref={email}
              label="Your email ID"
              value={values.email}
              error={errorMsg(errors.email, touched.email)}
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              onSubmitEditing={handleSubmit}
              autoCompleteType="email"
              keyboardType="email-address"
              textContentType="emailAddress"
            />
          </Box>
          <Box align="flex-start" mt={56}>
            <Button loading={isSubmitting} onPress={handleSubmit}>
              Create your profile
            </Button>
          </Box>
        </Box>
      )}
    </Formik>
  )
}

export default PersonalForm
