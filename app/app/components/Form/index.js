import LoginForm from './Login'
import VerifyForm from './Verify'
import ProfileForm from './Profile'
import PersonalForm from './Personal'
import DiagnosisForm from './Diagnosis'
import PetCreateForm from './PetCreate'
import PetUpdateForm from './PetUpdate'
import AppointmentForm from './Appointment'
import PrescriptionForm from './Prescription'

export {
  LoginForm,
  VerifyForm,
  ProfileForm,
  PersonalForm,
  DiagnosisForm,
  PetCreateForm,
  PetUpdateForm,
  AppointmentForm,
  PrescriptionForm,
}
