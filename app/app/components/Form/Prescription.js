import React, { useCallback, useMemo } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { mutate } from 'swr'
import find from 'lodash/find'
import { Formik, FieldArray } from 'formik'
import { Toast } from 'react-native-root-toaster'
import { useNavigation } from '@react-navigation/native'

import { ArrowDown } from '../Svg'
import request from '../../libs/request'
import useFetch from '../../libs/useFetch'
import { errorMsg } from '../../libs/utils'
import { Box, Input, Button, Select } from '../'

const usages = ['1-1-1', '1-1-0', '1-0-1', '0-1-1', '1-0-0', '0-1-0', '0-0-1']

const initInput = () => ({
  item: '',
  usage: '',
  directions: '',
})
const initialValues = {
  prescriptions: [initInput()],
}
const validationSchema = Yup.object().shape({
  prescriptions: Yup.array()
    .of(
      Yup.object().shape({
        item: Yup.string().required('Required'),
        usage: Yup.string().required('Required'),
        directions: Yup.string().required('Required'),
      })
    )
    .required()
    .min(1),
})

const PrescriptionForm = ({ appointment }) => {
  const navigation = useNavigation()

  const { data } = useFetch(`doctor/medicines`)

  const medicines = useMemo(() => get(data, 'data') || [], [data])

  const onSubmit = useCallback(async (values, { setErrors, resetForm }) => {
    try {
      await request.post(
        `doctor/appointments/${appointment.id}/prescription`,
        values
      )

      mutate(`doctor/appointments/${appointment.id}`)
      mutate(`doctor/pets/${appointment.pet.id}/history`)
      resetForm()
      Toast.show('Prescription added')
      navigation.navigate('Appointment', { id: appointment.id })
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  const seletItem = (value) => {
    const item = find(medicines, ['name', value])

    return item || { label: value, value }
  }

  return (
    <Formik
      onSubmit={onSubmit}
      validateOnChange={false}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <>
          <FieldArray
            name="prescriptions"
            render={(arrayHelpers) => (
              <>
                {values.prescriptions.map((prescription, index) => {
                  const item = `prescriptions[${index}].item`
                  const itemErrors = get(errors, item) || false
                  const itemTouched = get(touched, item) || false
                  const usage = `prescriptions[${index}].usage`
                  const usageErrors = get(errors, usage) || false
                  const usageTouched = get(touched, usage) || false
                  const directions = `prescriptions[${index}].directions`
                  const directionsErrors = get(errors, directions) || false
                  const directionsTouched = get(touched, directions) || false

                  return (
                    <Box key={index} mt={28}>
                      <Box mb={24}>
                        <Select
                          searchable
                          label="Medicine name"
                          data={medicines}
                          selectRender={() => (
                            <Box row align="center" justify="space-between">
                              <Box grow mb={-4}>
                                <Input
                                  noborder
                                  width="100%"
                                  value={prescription.item}
                                  onChangeText={handleChange(item)}
                                  onBlur={handleBlur(item)}
                                />
                              </Box>
                              <Box px={16}>
                                <ArrowDown color="shade" />
                              </Box>
                            </Box>
                          )}
                          selected={seletItem(prescription.item)}
                          onSelect={(val) => setFieldValue(item, val.name)}
                          right={<ArrowDown color="shade500" width={20} />}
                          error={errorMsg(itemErrors, itemTouched)}
                        />
                      </Box>
                      <Box mb={24}>
                        <Select
                          label="Usage"
                          data={usages}
                          selected={prescription.usage}
                          onSelect={(val) => setFieldValue(usage, val)}
                          right={<ArrowDown color="shade500" width={20} />}
                          error={errorMsg(usageErrors, usageTouched)}
                        />
                      </Box>
                      <Box mb={24}>
                        <Input
                          multiline
                          numberOfLines={6}
                          label="Directions of use"
                          value={prescription.directions}
                          error={errorMsg(directionsErrors, directionsTouched)}
                          onChangeText={handleChange(directions)}
                          onBlur={handleBlur(directions)}
                        />
                      </Box>
                      {values.prescriptions.length > 1 && (
                        <Box align="flex-end" pr={16} mt={-12}>
                          <Button
                            ghost
                            type="danger"
                            onPress={() => arrayHelpers.remove(index)}
                          >
                            Delete
                          </Button>
                        </Box>
                      )}
                    </Box>
                  )
                })}
                <Box align="flex-start">
                  <Button onPress={() => arrayHelpers.push(initInput())}>
                    Add more
                  </Button>
                </Box>
              </>
            )}
          />
          <Box align="flex-start" mt={56}>
            <Button loading={isSubmitting} onPress={handleSubmit}>
              Submit
            </Button>
          </Box>
        </>
      )}
    </Formik>
  )
}

export default PrescriptionForm
