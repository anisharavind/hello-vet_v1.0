import React, { useCallback, useMemo } from 'react'
import * as Yup from 'yup'
import get from 'lodash/get'
import { mutate } from 'swr'
import { Formik } from 'formik'
import { useDispatch } from 'react-redux'
import { Toast } from 'react-native-root-toaster'
import { useNavigation } from '@react-navigation/native'

import Box from '../Box'
import Text from '../Text'
import Input from '../Input'
import Button from '../Button'
import Select from '../Select'
import ImagePicker from '../ImagePicker'
import { ArrowDown } from '../Svg'
import request from '../../libs/request'
import { setHome } from '../../store/user'
import { errorMsg } from '../../libs/utils'

const AppointmentForm = ({ pet, date, department }) => {
  const dispatch = useDispatch()
  const navigation = useNavigation()

  const questions = useMemo(() => get(department, 'questions') || [], [
    department,
  ])
  const form = useMemo(() => {
    const values = {}
    const schema = {}

    questions.forEach((q) => {
      values[q.id] = ''
      schema[q.id] = Yup.string()
    })

    return { values, schema: Yup.object(schema) }
  }, [questions])

  const onSubmit = useCallback(async (values, { setErrors }) => {
    const data = new FormData()

    data.append('pet_id', pet.id)
    data.append('department_id', department.id)
    data.append('date', date)

    Object.keys(values).forEach((q, i) => {
      data.append(`questions[${i}][question_id]`, q)
      data.append(`questions[${i}][answer]`, values[q])
    })

    // data.append('questions', qs)

    // const data = {
    //   pet_id: pet.id,
    //   department_id: department.id,
    //   date,
    //   questions: qs,
    // }

    try {
      const resp = await request.post('appointments', data)
      const appointment = get(resp, 'data.data')

      dispatch(setHome('appointments'))

      if (appointment.paid_at) {
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'Success',
              params: { appointment, pet, redirect: 'Subscription' },
            },
          ],
        })
      } else {
        navigation.reset({
          index: 0,
          routes: [{ name: 'Payment', params: { appointment } }],
        })
      }
    } catch (error) {
      const errors = get(error, 'response.data.errors') || {}
      const message = get(error, 'response.data.message') || null

      setErrors(errors)
      Toast.show(message || error.message)
    }
  }, [])

  if ((!form.values, !form.schema)) return null

  return (
    <Formik
      enableReinitialize
      onSubmit={onSubmit}
      validateOnChange={false}
      initialValues={form.values}
      // validationSchema={form.schema}
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        setFieldValue,
        values,
        errors,
        touched,
        isSubmitting,
      }) => (
        <Box flex>
          {questions.map((q) => (
            <Box mb={24} key={q.id}>
              <Text mb={8}>{q.title}</Text>
              {q.type.value === 'text' && (
                <Input
                  value={values[q.id]}
                  error={errorMsg(errors[q.id], touched[q.id])}
                  onChangeText={handleChange(String(q.id))}
                  onBlur={handleBlur(String(q.id))}
                />
              )}
              {q.type.value === 'textarea' && (
                <Input
                  value={values[q.id]}
                  error={errorMsg(errors[q.id], touched[q.id])}
                  onChangeText={handleChange(String(q.id))}
                  onBlur={handleBlur(String(q.id))}
                  multiline
                  numberOfLines={6}
                />
              )}
              {q.type.value === 'select' && (
                <Select
                  data={q.options}
                  selected={values[q.id]}
                  onSelect={(item) => setFieldValue(String(q.id), item)}
                  right={<ArrowDown color="shade500" width={20} />}
                  error={errorMsg(errors[q.id], touched[q.id])}
                />
              )}
              {q.type.value === 'image' && (
                <ImagePicker
                  value={values[q.id]}
                  onSelect={(item) => setFieldValue(String(q.id), item)}
                  error={errorMsg(errors[q.id], touched[q.id])}
                />
              )}
            </Box>
          ))}
          <Box align="flex-start" mt={32}>
            <Button loading={isSubmitting} onPress={handleSubmit}>
              Checkout
            </Button>
          </Box>
        </Box>
      )}
    </Formik>
  )
}

export default AppointmentForm
