import React, { useMemo, useCallback } from 'react'
import moment from 'moment'
import get from 'lodash/get'
import groupBy from 'lodash/groupBy'
import isEmpty from 'lodash/isEmpty'
import { ScrollView } from 'react-native'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import styles from '../../styles'
import useFetch from '../../libs/useFetch'
import { Plus, UserFilled } from '../../components/Svg'
import { isPending, isCancelled } from '../../libs/utils'
import { Box, Header, Button, Text, AppointmentItem } from '../../components'

const Appointments = () => {
  const navigation = useNavigation()
  const { data, mutate } = useFetch('appointments')
  const { data: dataUpcoming, mutate: dataMutate } = useFetch(
    'appointments/upcoming'
  )

  useFocusEffect(
    useCallback(() => {
      mutate()
      dataMutate()
    }, [mutate, dataMutate])
  )

  const appointments = useMemo(() => get(data, 'data') || [], [data])
  const dataUpcomings = useMemo(() => get(dataUpcoming, 'data') || [], [
    dataUpcoming,
  ])

  const pendings = useMemo(() => {
    return groupBy(dataUpcomings, (appointment) => isPending(appointment))
  }, [dataUpcomings])

  const status = (item) => {
    if (!item) return ''
    if (isCancelled(item)) return 'danger'
    if (isPending(item)) return 'warning'
    if (moment(item.date).isAfter()) return 'info'

    return 'success'
  }

  const onSettings = () => navigation.navigate('Settings')
  const onNew = () => navigation.navigate('Booking')
  const onAppointment = (item) =>
    navigation.navigate('Appointment', { id: item.id })

  return (
    <Box safe bg="bg">
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={styles.grow}
      >
        <Header
          hideBack
          title="Appointments"
          right={<UserFilled width={28} height={28} />}
          onRight={onSettings}
        />
        <Box flex px={28} pb={28}>
          {!isEmpty(pendings.true) && (
            <Box mb={16}>
              <Text mb={20}>PAYMENT PENDING</Text>
              {pendings.true.map((pending) => (
                <Box key={pending.id} mb={16}>
                  <AppointmentItem
                    item={pending}
                    onPress={onAppointment}
                    status={status(pending)}
                  />
                </Box>
              ))}
            </Box>
          )}
          {!isEmpty(pendings.false) && (
            <Box mb={16}>
              <Text mb={20}>UPCOMING</Text>
              {pendings.false.map((upcoming) => (
                <Box key={upcoming.id} mb={16}>
                  <AppointmentItem
                    item={upcoming}
                    onPress={onAppointment}
                    status={status(upcoming)}
                  />
                </Box>
              ))}
            </Box>
          )}
          {!isEmpty(appointments) && <Text mb={20}>HISTORY</Text>}
          {appointments.map((upcoming) => (
            <Box key={upcoming.id} mb={16}>
              <AppointmentItem
                item={upcoming}
                onPress={onAppointment}
                status={status(upcoming)}
              />
            </Box>
          ))}
        </Box>
      </ScrollView>
      <Box
        align="flex-end"
        // width="100%"
        px={28}
        pt={16}
        pb={28}
        style={[styles.buttonWrapper, { right: 0 }]}
      >
        <Button left={<Plus color="textAlt" />} onPress={onNew}>
          New
        </Button>
      </Box>
    </Box>
  )
}

export default Appointments
