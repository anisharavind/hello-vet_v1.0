import React, { useMemo } from 'react'
import get from 'lodash/get'
import { FlatList } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'

import styles from '../../styles'
import { Plus, UserFilled } from '../Svg'
import useFetch from '../../libs/useFetch'
import { setPet } from '../../store/booking'
import { Box, Header, PetItem, Loader, Text, Button } from '../'

const Pets = () => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const { home } = useSelector((state) => state.user)
  const { data, isValidating } = useFetch('pets')

  const pets = useMemo(() => get(data, 'data') || [], [data])

  const onPet = (item) => {
    dispatch(setPet(item))
    navigation.navigate('BookingDate')
  }
  const onNew = () => navigation.navigate('PetProfile', { redirect: 'Booking' })
  const onSettings = () => navigation.navigate('Settings')

  const ListHeader = () => (
    <>
      <Header
        hideBack={home === 'booking'}
        title={pets.length > 0 ? 'Book an appointment' : 'Pet profiles'}
        right={<UserFilled width={28} height={28} />}
        onRight={onSettings}
      />
      {pets.length > 0 && (
        <Text mt={-40} mb={56} mx={28}>
          Select the pet for consultation
        </Text>
      )}
    </>
  )
  const ListFooter = () => (
    <Box align="flex-start" mt={28} px={28}>
      {!isValidating && (
        <Button
          small
          bg="secondary"
          left={<Plus color="textAlt" width={20} />}
          onPress={onNew}
        >
          {pets.length > 0 ? 'Add new pet' : 'Create your pet profile'}
        </Button>
      )}
    </Box>
  )
  const Empty = () =>
    isValidating ? (
      <Loader loading={isValidating} />
    ) : (
      <Box px={28}>
        <Text>
          Create your pet profiles for easy appointments and medical records
        </Text>
      </Box>
    )

  const renderItem = ({ item }) => (
    <Box px={28} mb={28}>
      <PetItem item={item} onPress={onPet} />
    </Box>
  )

  return (
    <Box safe bg="bg">
      <FlatList
        data={pets}
        renderItem={renderItem}
        ListEmptyComponent={<Empty />}
        keyExtractor={(item) => String(item.id)}
        ListHeaderComponent={<ListHeader />}
        ListFooterComponent={<ListFooter />}
        contentContainerStyle={styles.grow}
      />
    </Box>
  )
}

export default Pets
