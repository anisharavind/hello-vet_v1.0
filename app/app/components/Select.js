import React, { useRef, useState, useMemo } from 'react'
import get from 'lodash/get'
import isEqual from 'lodash/isEqual'
import includes from 'lodash/includes'
import isString from 'lodash/isString'
import { FlatList, View } from 'react-native'
import RBSheet from 'react-native-raw-bottom-sheet'

import { normalize } from '../libs/utils'

import Box from './Box'
import Text from './Text'
import Input from './Input'
import { Check } from './Svg'
import InputWrapper from './InputWrapper'

const Select = ({
  data,
  selected,
  onSelect,
  label,
  placeholder,
  error,
  left,
  right,
  selectRender,
  itemRender,
  small,
  large = false,
  searchable = false,
}) => {
  const menu = useRef(null)
  const [query, setQuery] = useState('')

  const value = useMemo(
    () => (isString(selected) ? selected : get(selected, 'name')),
    [selected]
  )

  const results = useMemo(() => {
    if (query.length > 1) {
      return data.filter((d) =>
        includes(d.name.toLowerCase(), query.toLocaleLowerCase())
      )
    }

    return data
  }, [data, query])

  const height = useMemo(() => {
    if (selectRender) {
      return searchable ? normalize(380) : normalize(320)
    }

    return searchable ? normalize(320) : normalize(260)
  }, [searchable, selectRender])

  const onOpen = () => menu.current && menu.current.open()
  const onSelection = (item) => {
    onSelect(item)

    if (menu.current) menu.current.close()
  }

  const Left = () => <Box pl={16}>{left}</Box>
  const Right = () => <Box pr={16}>{right}</Box>
  const ListHeader = () => (
    <Box
      justify="center"
      px={28}
      height={normalize(62)}
      borderBottom="shade500"
    >
      <Text type="h3" color="primary" weight="medium">
        {label}
      </Text>
    </Box>
  )
  const renderSelect = () => (
    <Box row align="center" height={small ? 40 : 60}>
      {left && <Left />}
      <Box flex grow>
        <Box justify="center" height="100%" px={16}>
          <Text
            type="input"
            color={value ? 'text700' : 'shade'}
            numberOfLines={1}
          >
            {value || placeholder}
          </Text>
        </Box>
      </Box>
      {right && <Right />}
    </Box>
  )
  const renderItem = ({ item }) => (
    <Box
      row
      align="center"
      justify="space-between"
      px={28}
      py={16}
      onPress={() => onSelection(item)}
    >
      <Box>
        {itemRender ? (
          itemRender(item)
        ) : (
          <Text type="h4" color={isEqual(item, selected) ? 'text' : 'text700'}>
            {isString(item) ? item : item.name}
          </Text>
        )}
      </Box>
      <Box pl={16}>
        <Check
          width={20}
          height={20}
          color={isEqual(item, selected) ? 'primary' : 'shade500'}
        />
      </Box>
    </Box>
  )

  return (
    <InputWrapper label={label} large={large} error={error} onPress={onOpen}>
      {selectRender ? selectRender(selected) : renderSelect()}
      <RBSheet ref={menu} height={height}>
        {label && <ListHeader />}
        <FlatList
          data={results}
          renderItem={renderItem}
          keyExtractor={(item) => (isString(item) ? item : String(item.id))}
          ItemSeparatorComponent={() => <Box borderBottom="shade500" />}
          keyboardShouldPersistTaps="handled"
        />
        {searchable && (
          <View>
            <Box borderBottom="shade500" />
            <Box justify="center" px={28} pt={12} pb={8} height={68}>
              <Input
                small
                autoFocus
                value={query}
                onChangeText={(text) => setQuery(text)}
              />
            </Box>
          </View>
        )}
      </RBSheet>
    </InputWrapper>
  )
}

export default Select
