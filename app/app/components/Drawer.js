import React, { useState, useCallback } from 'react'
import { cache } from 'swr'
import { ActivityIndicator } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { DrawerContentScrollView } from '@react-navigation/drawer'

import { colors } from '../config'
import request from '../libs/request'
import { logOut } from '../store/user'
import { navigate, route } from '../libs/navigation'

import Box from './Box'
import Text from './Text'
import Image from './Image'

const Drawer = (props) => {
  const dispatch = useDispatch()
  const currentRoute = route()
  const [loading, setLoading] = useState(false)
  const { user } = useSelector((state) => state.user)

  const onNavigate = (screen) => navigate(screen)

  const getColor = (screen) => {
    if (!currentRoute) return 'text'

    return currentRoute.name === screen ? 'primary' : 'text'
  }

  const onLogout = useCallback(async () => {
    setLoading(true)

    try {
      await request.post('logout')
    } catch (error) {
      //
    }

    cache.clear()
    dispatch(logOut())
  }, [])

  return (
    <DrawerContentScrollView {...props}>
      <Box align="flex-start" pt={16} pl={16}>
        <Box
          circle
          bg="shade300"
          align="center"
          justify="center"
          width={100}
          height={100}
          mb={28}
        >
          {user.image ? (
            <Image source={user.image} width={100} height={100} />
          ) : (
            <Text uppercase size={36} height={100} weight="medium">
              {String(user.name).charAt(0)}
            </Text>
          )}
        </Box>
        <Box onPress={() => onNavigate('Dashboard')} py={12}>
          <Text type="h4" color={getColor('Dashboard')}>
            Dashboard
          </Text>
        </Box>
        <Box onPress={() => onNavigate('Calendar')} py={12}>
          <Text type="h4" color={getColor('Calendar')}>
            Calendar
          </Text>
        </Box>
        <Box onPress={() => onNavigate('Timeslots')} py={12}>
          <Text type="h4" color={getColor('Timeslots')}>
            Manage Time Slots
          </Text>
        </Box>
        {/* <Box onPress={() => onNavigate('Appointments')} py={12}>
          <Text type="h4" color={getColor('Appointments')}>
            Appointments
          </Text>
        </Box> */}
        <Box onPress={onLogout} py={12}>
          {loading ? (
            <ActivityIndicator color={colors.primary} />
          ) : (
            <Text type="h4" color="text">
              Logout
            </Text>
          )}
        </Box>
      </Box>
    </DrawerContentScrollView>
  )
}

export default Drawer
