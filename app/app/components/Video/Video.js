import React, { useEffect, useState } from 'react'
import { Toast } from 'react-native-root-toaster'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { PermissionsAndroid, Platform } from 'react-native'
import RtcEngine, { RtcLocalView, RtcRemoteView } from 'react-native-agora'

import styles from '../../styles'
import { Box, Button } from '../../components'
import { setVideo } from '../../store/notifications'
import { User, Call, ToggleCamera } from '../../components/Svg'

const LocalView = RtcLocalView.SurfaceView
const RemoteView = RtcRemoteView.SurfaceView
const appid = 'ceca74fbca90431ea24cb947e04cdd48'

const Video = ({ screen }) => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const [peer, setPeer] = useState(null)
  // const [audio, setAudio] = useState(true)
  const [engine, setEngine] = useState(null)
  const [connected, setConnected] = useState(false)
  const { video } = useSelector((state) => state.notifications)

  useEffect(() => {
    if (engine && video) {
      setConnected(true)
      engine.joinChannel(null, video.channel, null, 0)
      dispatch(setVideo(null))
    }
  }, [engine, video])

  const onEnd = () => {
    if (engine) engine.leaveChannel()

    setPeer(null)
    setConnected(false)
    Toast.show('Call ended')
    navigation.navigate(screen)
  }
  const onCamera = () => {
    if (engine) engine.switchCamera()
  }
  // const onAudio = () => {
  //   setAudio((a) => {
  //     if (engine) {
  //       engine.muteLocalAudioStream(!a)
  //       return !a
  //     }

  //     return a
  //   })
  // }

  useEffect(() => {
    const init = async () => {
      if (Platform.OS === 'android') {
        try {
          const granted = await PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
          ])
          if (
            granted['android.permission.RECORD_AUDIO'] !==
              PermissionsAndroid.RESULTS.GRANTED ||
            granted['android.permission.CAMERA'] !==
              PermissionsAndroid.RESULTS.GRANTED
          ) {
            Toast.show('No camera or audio permission')
          }
        } catch (err) {
          Toast.show('No camera or audio permission')
        }
      }

      const eng = await RtcEngine.create(appid)

      await eng.enableVideo()

      eng.addListener('UserJoined', (data) => {
        setPeer(data)
      })
      eng.addListener('UserOffline', () => {
        Toast.show('User disconnected')
        // onEnd()
      })
      eng.addListener('JoinChannelSuccess', () => {
        setConnected(true)
      })

      setEngine(eng)
    }

    setImmediate(() => init())
  }, [])

  return (
    <Box safe bg="bg">
      <Box flex>
        {peer ? (
          <Box flex bg="shade300">
            <RemoteView style={styles.grow} uid={peer} renderMode={1} />
          </Box>
        ) : (
          <Box flex bg="shade300" align="center" justify="center">
            <User width="50%" height="50%" />
          </Box>
        )}
        {connected && (
          <>
            <LocalView
              style={styles.localView}
              renderMode={1}
              zOrderMediaOverlay
            />
            <Box
              row
              justify="center"
              align="center"
              width="100%"
              pb={28}
              style={styles.buttonWrapper}
            >
              {/* <Button
                ghost
                onPress={onAudio}
                style={[styles.buttons, styles.camera]}
              >
                {audio ? <Mic /> : <MicOff />}
              </Button> */}
              <Button
                bg="textAlt"
                ghost
                onPress={onCamera}
                style={[styles.buttons, styles.camera]}
              >
                <ToggleCamera />
              </Button>
              <Button
                ghost
                onPress={onEnd}
                style={[styles.buttons, styles.disconnect]}
              >
                <Call color="textAlt" />
              </Button>
            </Box>
          </>
        )}
      </Box>
    </Box>
  )
}

export default Video
