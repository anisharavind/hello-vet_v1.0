/* eslint-disable no-nested-ternary */
import React from 'react'
import { StyleSheet } from 'react-native'

import Box from './Box'
import Text from './Text'

const styles = StyleSheet.create({
  label: {
    position: 'absolute',
    top: 0,
  },
})

const InputWrapper = ({
  label,
  focus,
  error,
  children,
  large = false,
  noborder = false,
  ...props
}) => {
  const Label = () => (
    <Box bg="bg" px={8} ml={8} mt={-8} style={styles.label}>
      <Text type="label" color="primary">
        {label}
      </Text>
    </Box>
  )

  const Error = () => (
    <Box px={8}>
      <Text type="p3" color="danger">
        {error}
      </Text>
    </Box>
  )

  return (
    <>
      <Box
        round
        largeRound={large}
        justify="center"
        border={
          noborder ? false : error ? 'danger' : focus ? 'primary' : 'shade500'
        }
        mb={4}
        {...props}
      >
        {label && <Label />}
        {children}
      </Box>
      {error && <Error />}
    </>
  )
}

export default InputWrapper
