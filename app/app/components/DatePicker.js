import React, { useRef } from 'react'
import moment from 'moment'
import RBSheet from 'react-native-raw-bottom-sheet'
import CalendarPicker from 'react-native-calendar-picker'

import { colors } from '../config'
import { normalize } from '../libs/utils'

import Box from './Box'
import Text, { textStyles } from './Text'
import InputWrapper from './InputWrapper'

const DatePicker = ({
  selected,
  onSelect,
  label,
  placeholder,
  error,
  left,
  right,
}) => {
  const menu = useRef(null)
  const date = moment(selected)
  const value = date.isValid() ? date.format('DD-MM-YYYY') : ''

  const onOpen = () => menu.current && menu.current.open()
  const onSelection = (item) => {
    onSelect(item)

    if (menu.current) menu.current.close()
  }

  const Left = () => <Box pl={16}>{left}</Box>
  const Right = () => <Box pr={16}>{right}</Box>

  return (
    <InputWrapper label={label} error={error}>
      <Box row align="center" height={52}>
        {left && <Left />}
        <Box flex grow>
          <Box justify="center" height="100%" onPress={onOpen} px={16}>
            <Text
              type="input"
              color={value ? 'text700' : 'shade'}
              numberOfLines={1}
            >
              {value || placeholder}
            </Text>
          </Box>
        </Box>
        {right && <Right />}
      </Box>
      <RBSheet ref={menu} height={normalize(404)}>
        <Box pt={12}>
          <CalendarPicker
            onDateChange={onSelection}
            selectedDayColor={colors.primary}
            selectedDayTextColor={colors.textAlt}
            todayBackgroundColor={colors.shade500}
            textStyle={{ ...textStyles.p1, color: colors.text700 }}
            todayTextStyle={{ color: colors.text700 }}
            dayLabelsWrapper={{
              borderBottomColor: colors.shade500,
              borderTopColor: colors.shade500,
            }}
          />
        </Box>
      </RBSheet>
    </InputWrapper>
  )
}

export default DatePicker
