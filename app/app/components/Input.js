import React, { useState, useCallback } from 'react'
import { TextInput, StyleSheet } from 'react-native'
import { TextInputMask } from 'react-native-masked-text'

import { colors } from '../config'

import Box from './Box'
import { textStyles } from './Text'
import InputWrapper from './InputWrapper'

const styles = StyleSheet.create({
  input: {
    color: colors.text700,
    padding: 0,
    paddingBottom: 1,
    borderWidth: 0,
    textAlignVertical: 'center',
    ...textStyles.input,
  },
})

const Input = (
  {
    label,
    error,
    left,
    right,
    mask,
    options,
    multiline = false,
    noborder = false,
    ...props
  },
  ref
) => {
  const [focused, setFocused] = useState(false)

  const Left = () => <Box pl={16}>{left}</Box>
  const Right = () => <Box pr={16}>{right}</Box>

  const onFocus = useCallback(() => setFocused(true), [])
  const onBlur = useCallback(() => setFocused(false), [])

  return (
    <InputWrapper
      label={label}
      focus={focused}
      error={error}
      noborder={noborder}
    >
      <Box row align="center" height={multiline ? 120 : 60}>
        {left && <Left />}
        <Box flex justify="center" px={16}>
          {mask ? (
            <TextInputMask
              type={mask}
              options={options}
              multiline={multiline}
              style={styles.input}
              placeholderTextColor={colors.text400}
              underlineColorAndroid={colors.transparent}
              onFocus={onFocus}
              onBlur={onBlur}
              onEndEditing={onBlur}
              {...props}
            />
          ) : (
            <TextInput
              ref={ref}
              multiline={multiline}
              style={styles.input}
              placeholderTextColor={colors.text400}
              underlineColorAndroid={colors.transparent}
              onFocus={onFocus}
              onBlur={onBlur}
              onEndEditing={onBlur}
              {...props}
            />
          )}
        </Box>
        {right && <Right />}
      </Box>
    </InputWrapper>
  )
}

export default React.forwardRef(Input)
