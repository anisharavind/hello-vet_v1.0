import React from 'react'
import FastImage from 'react-native-fast-image'

const Image = ({ image, source, width, height, ...props }) => (
  <FastImage
    source={image || { uri: source }}
    style={{ width, height }}
    {...props}
  />
)

export default Image
